import React from "react";
import {Router, Route} from "react-router-dom";
import { Container, Row, Col } from "shards-react";

import routes from "./routes";
import withTracker from "./withTracker";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/shards-dashboards.1.1.0.min.css";
import 'antd/dist/antd.css';
import './App.css'
import {Provider} from 'react-redux';
import store from './store';
import Global from './components/layout/Global/Global';
export default() => (
    <Provider store={store}>
        <Router basename={process.env.REACT_APP_BASENAME || ""} history={Global.history}>
        <Container className="container-fluid">
        
        
                {routes.map((route, index) => {
                    let authFlag = route.authNeeded;
                    if(authFlag===undefined) {
                        authFlag = true;
                    }
                    return (
                        <Route
                            key={index}
                            path={route.path}
                            exact={route.exact}
                            component={withTracker(props => {
                            return (
                              <route.layout {...props} authNeeded={authFlag}>
                                    <route.component {...props}/>
                                </route.layout>
                            );
                        })}/>
                    );
                })}
        </Container>
        </Router>
    </Provider>
);