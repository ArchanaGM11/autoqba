import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Container, Navbar } from "shards-react";

import NavbarSearch from "./NavbarSearch";
import NavbarNav from "./NavbarNav/NavbarNav";
import NavbarToggle from "./NavbarToggle";
import NavbarMenu from "./NavbarMenu";

const MainNavbar = ({ layout, stickyTop }) => {
  const classes = classNames(
    "main-navbar",
    //"bg-white",
    stickyTop && "header-sticky"
  );

  return (
    <header className={classes}>
        <Navbar className="row">
          <div className="col-md-8 nav-left">
            <div className="autoqba-logo">
              <img
                  id="main-logo"
                  className="logo"
                  src={require("../../../assets/images/autoqba-logo.png")}
                  alt="Auto Q - BA Dashboard"
                />
            </div>
            <NavbarMenu/>
          </div>
          <div className="col-md-4 nav-right">
              <div className="search-box col-md-7">
                <NavbarSearch />
              </div>
              <NavbarNav />
              <NavbarToggle />
          </div>
        </Navbar>
    </header>
  );
};

MainNavbar.propTypes = {
  /**
   * The layout type where the MainNavbar is used.
   */
  layout: PropTypes.string,
  /**
   * Whether the main navbar is sticky to the top, or not.
   */
  stickyTop: PropTypes.bool
};

MainNavbar.defaultProps = {
  stickyTop: true
};

export default MainNavbar;
