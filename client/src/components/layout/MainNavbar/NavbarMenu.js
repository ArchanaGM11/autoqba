import React, { Component } from "react";
import { Menu, Icon } from "antd";
import Global from '../Global/Global';
import {connect} from 'react-redux';
const { SubMenu } = Menu;

class NavbarMenu extends Component {
  render() {
    return (
        <Menu
          mode="horizontal"
          defaultSelectedKeys={["0"]}
          className="navHeaderMenu"
        >
          <SubMenu
            title={
              <span className="submenu-title-wrapper">
                {" "}
                <Icon type="laptop" />
                Application{" "}
              </span>
            }
          >
            <Menu.ItemGroup>
              <Menu.Item key="setting:2">
                <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/viewapplication");}}><Icon type="eye" />View Application</a>
              </Menu.Item>
              <Menu.Item key="setting:1">
                <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/addapplication");}}><Icon type="appstore" />Add Application</a>
              </Menu.Item>
            </Menu.ItemGroup>
          </SubMenu>
          <SubMenu
            title={
              <span className="submenu-title-wrapper">
                {" "}
                <Icon type="project" />
                Project{" "}
              </span>
            }
          >
            <Menu.ItemGroup>
              <Menu.Item key="setting:1">
                <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/viewproject");}}><Icon type="eye" />View Project</a>
              </Menu.Item>
              <Menu.Item key="setting:2">
                <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/addproject");}}><Icon type="appstore" />Add Project</a>
              </Menu.Item>
            </Menu.ItemGroup>
          </SubMenu>
          <SubMenu
            title={
              <span className="submenu-title-wrapper">
                {" "}
                <Icon type="tool" />
                Tools{" "}
              </span>
            }
          >
            <Menu.ItemGroup>
              <Menu.Item key="setting:1">
                <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/viewtool");}}><Icon type="eye" />View Tool</a>
              </Menu.Item>

              <Menu.Item key="setting:2">
                <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/addtool");}}><Icon type="file-add" />Add Tool</a>
              </Menu.Item>
            </Menu.ItemGroup>
          </SubMenu>
          <SubMenu
            title={
              <span className="submenu-title-wrapper">
                {" "}
                <Icon type="export" />
                OR{" "}
              </span>
            }
          >
             <Menu.ItemGroup>
              <Menu.Item key="setting:1">
                <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/addrepository");}}><Icon type="cloud" />New Repository</a>
              </Menu.Item>
              <Menu.Item key="setting:2">
                <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/viewrepository");}}><Icon type="eye" />View Repository</a>
              </Menu.Item>
            </Menu.ItemGroup>
          </SubMenu>
        <SubMenu
          key="sub1"
          title={
            <span className="submenu-title-wrapper">
              {" "}
              <Icon type="setting" />
              <span>Settings{" "}</span>
            </span>
          }
        >
          <SubMenu key="sub1-2" title= {
            <span>
            <Icon type="user" />
            <span>User</span>
          </span>
          }>
            <Menu.Item key="1">
              <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/adduser");}}><Icon type="appstore" />New User</a>
            </Menu.Item>
            <Menu.Item key="2">
              <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/viewuser");}}><Icon type="eye" />View User</a>
              </Menu.Item>
          </SubMenu>
          <SubMenu key="sub1-3" title={
            <span>
            <Icon type="crown" />
            <span>Role</span>
          </span>
          }>
            <Menu.Item key="3">
              <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/addrole");}}><Icon type="appstore" />New Role</a>
            </Menu.Item>
            <Menu.Item key="4">
              <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/viewrole");}}><Icon type="eye" />View Role</a>
            </Menu.Item>
          </SubMenu>
          <SubMenu key="sub1-4" title={
            <span>
            <Icon type="deployment-unit" />
            <span>Privilege</span>
          </span>
          }>
            <Menu.Item key="5">
              <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/addprivilege");}}><Icon type="appstore" />New Privilege</a>
            </Menu.Item>
            <Menu.Item key="6">
              <a href="" onClick={(e)=>{e.preventDefault();Global.history.push("/viewprivilege");}}><Icon type="eye" />View Privilege</a>
            </Menu.Item>
          </SubMenu>
        </SubMenu>
        </Menu>
    );
  }
}
const mapStateToProps = state => ({loginData: state.loginData});
export default connect(mapStateToProps)(NavbarMenu);
