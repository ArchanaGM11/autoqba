import React from "react";
import PropTypes from "prop-types";
import { Navbar, NavbarBrand } from "shards-react";

import { Dispatcher, Constants } from "../../../flux";

let navbarStyle;
class SidebarMainNavbar extends React.Component {
  constructor(props) {
    super(props);

    this.handleToggleSidebar = this.handleToggleSidebar.bind(this);
  }

  handleToggleSidebar() {
    Dispatcher.dispatch({
      actionType: Constants.TOGGLE_SIDEBAR
    });
  }

  componentWillMount() {
    if(this.props.leftNav) {
      navbarStyle = "align-items-stretch bg-white flex-md-nowrap border-bottom p-0";
    } else {
      navbarStyle = "align-items-stretch bg-white flex-md-nowrap p-0";
    }
  }

  render() {
    const { hideLogoText } = this.props;
    return (
      <div className="main-navbar">
        <Navbar
          className={navbarStyle}
          type="light"
        >
          <NavbarBrand
            className="w-100 mr-0 autoqba-logo"
            href="#"
          >
              <img
                id="main-logo"
                className="logo"
                src={require("../../../assets/images/autoqba-logo.png")}
                alt="Auto Q - BA Dashboard"
              />
              {/* {!hideLogoText && (
                <span className="d-none d-md-inline ml-1 autoqba">
                  Auto Q - BA
                </span>
              )} */}
          </NavbarBrand>
          {/* eslint-disable-next-line */}
          <a
            className="toggle-sidebar d-sm-inline d-md-none d-lg-none"
            onClick={this.handleToggleSidebar}
          >
            <i className="material-icons">&#xE5C4;</i>
          </a>
        </Navbar>
      </div>
    );
  }
}

SidebarMainNavbar.propTypes = {
  /**
   * Whether to hide the logo text, or not.
   */
  hideLogoText: PropTypes.bool
};

SidebarMainNavbar.defaultProps = {
  hideLogoText: false
};

export default SidebarMainNavbar;
