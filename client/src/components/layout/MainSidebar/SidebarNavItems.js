import React from "react";
import { Nav } from "shards-react";

import SidebarNavItem from "./SidebarNavItem";

class SidebarNavItems extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      navItems: [
        {
          htmlBefore: '<i class="material-icons">note_add</i>',
          title: "Get Quote",
          to: "/home",
        },
        {
          htmlBefore: '<i class="material-icons">table_chart</i>',
          title: "Find Policies",
          to: "/tables",
        }
      ]
    };
  }


  render() {
    const { navItems: items } = this.state;
    return (
      <div className="nav-wrapper">
        <Nav className="nav--no-borders flex-column">
          {items.map((item, idx) => (
            <SidebarNavItem key={idx} item={item} />
          ))}
        </Nav>
      </div>
    )
  }
}

export default SidebarNavItems;
