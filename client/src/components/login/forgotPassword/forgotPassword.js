import React from 'react'
import '../login.css';
import { Row, Col, Card, Input, Form, Icon, Checkbox, Button, Layout,Typography,Alert } from 'antd';
import logo from '../../../assets/images/logo.jpg';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NavLink as RouteNavLink } from "react-router-dom";
import { password } from '../../../redux/actions/forgotPasswordAction';

import Global from '../../layout/Global/Global'

const {Header,Content,Footer,Sider}=Layout;

class ForgotPassword extends React.Component {
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                
                this.props.password(values);
              
            }
        });
     };

    redirectlogin = e => {
      e.preventDefault();
      Global.history.push("/login");
    }
    
    componentWillMount(){
      console.log(101, Global.alertContent)
      Global.clearAlert();
    }
    componentWillUpdate(){
      console.log(102, Global.alertContent)
    }

    render() {
      const { getFieldDecorator } = this.props.form;
      
        return (
          <div>

            <Row className="App-logo">
                <img src={logo} />
            </Row>

            <Row>
              <Row type="flex" justify="center">
                <Card className="Forgot-password">
                <Row>
                    <Row>
                        <h4 className="left-align forgot-text" style={{ marginBottom: '1px' }}>Forgot Password?</h4>
                        <hr></hr>
                        <h6 className="muted-text text ">Enter the email address associated with your account</h6>
                    </Row>
                  </Row>
                 
                  <br />

                  <Form onSubmit={this.handleSubmit}>
                  <Form.Item>
                      {getFieldDecorator('Email', {
                        rules: [
                            {
                              message: 'Please enter Email-ID',
                                required: true
                            },
                            {
                                max: 35,
                                message: "Email should be accept maximum 35 characters"
                            },
                            {
                              message: "Email should be minimum 3 characters",
                                min: 3,
                            }
                        ],
                    })( <Row>
                        <h5 className="text">Email-ID</h5>
                        
                        <Input  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Enter your Email-ID " size="default" allowClear/>
                       </Row>)}
                      </Form.Item><br/>
                       <Row>
                       {
                        Global.alertContent.message?
                        <Alert
                          message={Global.alertContent.status.toUpperCase()}
                          description={Global.alertContent.message}
                          type={Global.alertContent.status}
                          closable={Global.alertContent.closable}
                        />:null
                      }<br/>
                       </Row> 
                      <Row type="flex" justify="center">
                        <Button className="login-btn" htmlType="submit" >
                          Generate Password
                        </Button>
                      </Row>
                  </Form>
                </Card>
              </Row>
              <Row style={{textAlign:"center"}}>
              <p className="muted-text">** If you've been unable to reset your password please contact your admin.
              <Button type="link" onClick={this.redirectlogin}>Back to Login </Button>
              </p>
             </Row>
             </Row><br />
           

            <Footer className="footer">

              <Row type="flex" justify="center">
                <Col>
                  <Button type="link">Help</Button>
                </Col>
                <Col>
                  <Button type="link">Terms</Button>
                </Col>
              </Row>

              <Row type="flex" justify="center">
                <Col>
                  <h5 className="text-footer">
                    © Expleo Groups 2019</h5>
                </Col>
              </Row>

              </Footer>


          </div>
         
        )
    }
}

ForgotPassword.propTypes = {
  message: PropTypes.object.isRequired,
  password: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  // alertContent: {
  //   closable: state.alertContent.closable,
  //   message : state.alertContent.message,
  //   status: state.alertContent.status
  // }
});

const ForgotpasswordForm = Form.create({ name: 'Submit' })(ForgotPassword);

export default connect(mapStateToProps, {password})(ForgotpasswordForm);

