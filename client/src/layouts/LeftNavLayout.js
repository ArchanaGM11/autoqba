import React from "react";
import PropTypes from "prop-types";
import { Container, Row, Col } from "shards-react";

import MainNavbar from "../components/layout/MainNavbar/MainNavbar";
import MainSidebar from "../components/layout/MainSidebar/MainSidebar";
import AuthRouter from '../components/router/AuthRouter'

const LeftNavLayout = ({ children, noNavbar, authNeeded }) => (
  <Row className="leftlayout">
     <AuthRouter authNeeded={authNeeded}/>
    {!noNavbar && <MainNavbar />}
    <Container fluid className="wrapper-container">
      <MainSidebar leftNav={true} />
      <Col
        className="main-content p-0"
        lg={{ size: 10, offset: 2 }}
        md={{ size: 9, offset: 3 }}
        sm="12"
        tag="main"
      >
        {children}
      </Col>
    </Container>
  </Row>
);

LeftNavLayout.propTypes = {
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool,
  /**
   * Whether to display the navbar, or not.
   */
  noNavbar: PropTypes.bool
};

LeftNavLayout.defaultProps = {
  noFooter: false,
  noNavbar: false
};

export default LeftNavLayout;
