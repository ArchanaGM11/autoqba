import {
  FETCH_APPLICATION,
  FILTER_APPLICATION,
  NEW_APPLICATION,
  EDIT_APPLICATION,
  UPDATE_APPLICATION,
  DELETE_APPLICATION,
  FILTER1_APPLICATION,
  UPDATE_APPLICATIONSTATUS
} from "./types";

import fetch from '../../utils/leoFetch'
import axios from '../../utils/leoAxios'

export const createApplication = projectData => dispatch => {
  console.log("action called");
  fetch("http://192.168.22.39:4000/api/applications", {
    body: JSON.stringify(projectData),
    headers: {
      "content-type": "application/json"
    },
    method: "POST"
  })
    .then((res) => {
      if (res.status === 201) { alert('Application Succesfully Added'); }
      else if (res.status === 400) { alert('Error In Application Creation'); }
      else if (res.status === 409) { alert('Duplicate Application'); }
    })
    .then(res => res.json())
    .then(obj =>
      dispatch({
        payload: obj,
        type: NEW_APPLICATION
      })).catch(err => console.log(err));
  console.log('Successfully added....')
  console.log(projectData);
};


export const fetchApplication = () => dispatch => {
  fetch("http://192.168.22.39:4000/api/applications")
    .then(res => res.json())
    .then(applications =>
      dispatch({
        payload: applications,
        type: FETCH_APPLICATION
      })
    );
};

export const editApplication = ApplicationId => dispatch => {
  console.log("Edit Application --> params _id " + ApplicationId);
  fetch("http://192.168.22.39:4000/api/applications/" + ApplicationId, {
    headers: {
      "content-type": "application/json"
    },
    method: "GET"
  })
    .then(res => res.json())
    .then(element => {
      console.log("Edit Element", element);
      dispatch({
        payload: element,
        type: EDIT_APPLICATION
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const updateApplication = (ApplicationId, objData) => dispatch => {
  console.log("update Post --> params _id" + " " + ApplicationId);
  fetch("http://192.168.22.39:4000/api/applications/" + ApplicationId, {
    body: JSON.stringify(objData),
    headers: {
      "content-type": "application/json"
    },
    method: "PUT"
  })
    .then((res) => {
      if (res.status === 204) { alert('Application Succesfully Updated'); }
      else if (res.status === 500) { alert('Error In Application Creation'); }
      else if (res.status === 409) { alert('Duplicate Application'); }
    })
    .then(obj => {
      console.log(obj);
      dispatch({
        payload: obj,
        type: UPDATE_APPLICATION
      });
      console.log(obj);
    });
};

export const deleteApplication = id => dispatch => {
  console.log(2, "Inside Delete action");
  fetch("http://192.168.22.39:4000/api/applications/" + id, {
    headers: {
      "content-type": "application/json"
    },
    method: "DELETE"
  });
  dispatch({
    id: id,
    type: "DELETE_APPLICATION"
  });
};

export const filterApplication = (appName, appVersion) => dispatch => {
  fetch('http://localhost:4000/api/applications/filter?appName=' + appName + '&appVersion=' + appVersion)
    .then(res => {
      res.json();
    }).then(applications => {
      console.log("apps : ", applications);
      dispatch({
        payload: applications,
        type: FILTER_APPLICATION
      })
    });
}
export const viewApplication = id => dispatch => {
  console.log("Inside view Project");
  fetch("http://192.168.22.39:4000/api/applications/user/" + id)
  .then(res => res.json())
  .then(applications =>
    dispatch({
      payload: applications,
      type: FILTER1_APPLICATION
    })
  );
}

export const updateApplicationstatus = id => dispatch=>{
  console.log(2, "Inside Deactive action");
  fetch("http://192.168.22.39:4000/api/applications/status/" + id, {
    headers: {
      "content-type": "application/json"
    },
    method: "PUT"
  })
    .then(obj => {
      console.log(obj);
      dispatch({
        payload: obj,
        type: UPDATE_APPLICATIONSTATUS
      });
      console.log(obj);
    });
};