import { LOGIN, LOGOUT } from './types';
import fetch from '../../utils/leoFetch'
import axios from '../../utils/leoAxios'
import Global from '../../components/layout/Global/Global';
import API_ROOT from '../../utils/api-config'

export const logoutAction = () => dispatch => {
    let loginData = {
        isLoggedIn: false,
        redirect: false,
        token: null
    };
    localStorage.clear();
    dispatch({
        payload: loginData,
        type: LOGOUT
    })

}

export const loginAction = (values) => dispatch => {

    let loginData = {
        Email: null,
        FirstName: null,
        LastName: null,
        isLoggedIn: false,
        redirect: false,
        token: null
    };

    axios.post(API_ROOT+'/users/authenticate', values)
        .then(res => {

            var token = res.data.token;
            var UserName = res.data.username;
            if (token !== null && token !== undefined) {
                Global.role= res.data.role;
                console.log("Set Roles: ",Global.role );
                localStorage.setItem('token', token);
                localStorage.setItem('UserName', res.data.userName);
                localStorage.setItem('loginData',JSON.stringify(res.data));

                dispatch({
                    payload: {
                        Email: res.data.Email,
                        FirstName:res.data.firstName,
                        LastName:res.data.lastName,
                        isLoggedIn: true,
                        redirect: true,
                        ...res.data
                    },
                    type: LOGIN
                })
            }
            else {
                localStorage.removeItem("token");
                Global.alertContent = {
                    closable: true,
                    message: res.data.userMessage,
                    status: "error"
                }
                dispatch({
                    payload: {
                        ...loginData,
                        message: res.data.userMessage
                    },
                    type: LOGIN
                })
            }
        })
}

