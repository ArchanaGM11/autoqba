import { FETCH_PRIVILEGE, NEW_PRIVILEGE, EDIT_PRIVILEGE, UPDATE_PRIVILEGE, DELETE_PRIVILEGE } from './types';
import fetch from '../../utils/leoFetch'
import axios from '../../utils/leoAxios'
import API_ROOT from '../../utils/api-config'

export const createPrivilege = (object) => dispatch => {
  console.log('New Privilege');
  console.log("JWt Token : " +localStorage.getItem("token"));

  fetch(API_ROOT+'/users/newprivilege', {

    body: JSON.stringify(object),
    headers: {
      'Authorization':'Bearer ' +localStorage.getItem("token"),
      'content-type': 'application/json'
    },
    method: 'POST'
  })
    .then(res => res.json())
    .then(privilege => dispatch({
      payload: privilege,
      type: NEW_PRIVILEGE
    }));
  console.log('Successfully added....')
  console.log(object);
};

export const fetchPrivilege = () => dispatch => {
  fetch(API_ROOT+'/users/privilege/privilegelist')
    .then(res => res.json())
    .then(privileges =>
      dispatch({
        payload: privileges,
        type: FETCH_PRIVILEGE
      })
    );
}

export const editPrivilege = PrivilegeId => dispatch => {
  console.log("Edit Privilege --> params _id " + PrivilegeId);
  fetch(API_ROOT+"/users/privilege/" + PrivilegeId, {
    headers: {
      "content-type": "privileges/json"
    },
    method: "GET"
  })
    .then(res => res.json())
    .then(element => {
      console.log("Edit Element", element);
      dispatch({
        payload: element,
        type: EDIT_PRIVILEGE
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};
export const updatePrivilege = (PrivilegeId, objData) => dispatch => {
  console.log("update Post --> params _id" + " " + PrivilegeId);
  fetch(API_ROOT+"/users/privilege/" + PrivilegeId, {
    body: JSON.stringify(objData),
    headers: {
      "content-type": "application/json"
    },
    method: "PUT"
  })
    .then((res) => {
      if (res.status === 204) { alert('Privilege Succesfully Updated'); }
      else if (res.status === 500) { alert('Error In Privilege Creation'); }
      else if (res.status === 409) { alert('Duplicate Privilege'); }
    })
    .then(obj => {
      console.log(obj);
      dispatch({
        payload: obj,
        type: UPDATE_PRIVILEGE
      });
      console.log(obj);
    });
};
export const deletePrivilege = id => dispatch => {
  console.log(2, "Inside Delete action");
  fetch(API_ROOT+"/users/privilege/" + id, {
    headers: {
      "content-type": "application/json"
    },
    method: "DELETE"
  });
  dispatch({
    id: id,
    type: "DELETE_PRIVILEGE"
  });
};



