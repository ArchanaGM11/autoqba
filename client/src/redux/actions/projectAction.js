import { 
  FETCH_PROJECT, 
  NEW_PROJECT, 
  EDIT_PROJECT,
  UPDATE_PROJECT,
  DELETE_PROJECT, 
  INMEMORY_ADD_USER, 
  INMEMORY_ADD_USER1, 
  FILTER_PROJECT,
  UPDATE_PROJECTSTATUS 
} from "./types";
import fetch from '../../utils/leoFetch'

export const createProject = projectData => dispatch => {
  console.log("action called");
  fetch("http://192.168.22.39:4000/api/projects", {
    body: JSON.stringify(projectData),
    headers: {
      "content-type": "application/json"
    },
    method: "POST"
  })
  .then((res) => {
    if (res.status === 201) { alert('Project Succesfully Added'); }
    else if (res.status === 400) { alert('Error In Project Creation'); }
  })
    .then(res => res.json())
    .then(obj =>
      dispatch({
        payload: obj,
        type: NEW_PROJECT
      })).catch(err => console.log(err));
    ///  Object.assign(this.props.data3,[]);
    ///  delete this.props.data3;
      console.log('Successfully added....')
      console.log(projectData);
    };

export const fetchProject = () => dispatch => {
  fetch("http://192.168.22.39:4000/api/projects")
    .then(res => res.json())
    .then(projects =>
      dispatch({
        payload: projects,
        type: FETCH_PROJECT
      })
    );
};
export const editProject = ProjectId => dispatch => {
  console.log("Edit Application --> params _id " + ProjectId);
  fetch("http://192.168.22.39:4000/api/projects/" + ProjectId, {
    headers: {
      "content-type": "application/json"
    },
    method: "GET"
  })
    .then(res => res.json())
    .then(element => {
      console.log("Edit Element", element);
      dispatch({
        payload: element,
        type: EDIT_PROJECT
      });
    })
    .catch(function(error) {
      console.log(error);
    });
};
export const updateProject = (ProjectId, objData) => dispatch => {
  console.log("update Post --> params _id" + " " + ProjectId);
  fetch("http://192.168.22.39:4000/api/projects/" + ProjectId, {
    body: JSON.stringify(objData),
    headers: {
      "content-type": "application/json"
    },
    method: "PUT"
  })
    .then((res) => {
      if (res.status === 204) { alert('Project Succesfully Updated'); }
      else if (res.status === 500) { alert('Error In Project Creation'); }
      else if (res.status === 409) { alert('Duplicate Project'); }
    })
    .then(obj => {
      console.log(obj);
      dispatch({
        payload: obj,
        type: UPDATE_PROJECT
      });
      console.log(obj);
    });
};
export const userupdateProject = (objData) => dispatch => {
  console.log(111,objData);
  dispatch({
    payload: objData.user,
    type: INMEMORY_ADD_USER1
  });
};

export const emptyuserupdate = () => dispatch => {
  console.log("Inside EMPTY USER................")
  dispatch({
    payload: [],
    type: INMEMORY_ADD_USER1
  });
};
export const emptyeditproject = () => dispatch => {
  console.log("Inside EMPTY Edit Project Reducer................")
  dispatch({
    payload: [],
    type: EDIT_PROJECT
  });
};

export const inmemoryAddUser = (ProjectId, objData) => dispatch => {
      console.log(111,objData,ProjectId);
      dispatch({
        payload: objData.user,
        type: INMEMORY_ADD_USER
      });
};
export const deleteProject = id => dispatch => {
  console.log(2, "Inside Delete action");
  fetch("http://192.168.22.39:4000/api/projects/" + id, {
    headers: {
      "content-type": "application/json"
    },
    method: "DELETE"
  });
  dispatch({
    id: id,
    type: DELETE_PROJECT
  });
};
export const viewProject = id => dispatch => {
  console.log("Inside view Project");
  fetch("http://192.168.22.39:4000/api/projects/user/" + id)
  .then(res => res.json())
  .then(projects =>
    dispatch({
      payload: projects,
      type: FILTER_PROJECT
    })
  );
}

export const updateProjectstatus = id => dispatch=>{
  console.log(2, "Inside Deactive action");
  fetch("http://192.168.22.39:4000/api/projects/status/" + id, {
    headers: {
      "content-type": "application/json"
    },
    method: "PUT"
  })
    .then(obj => {
      console.log(obj);
      dispatch({
        payload: obj,
        type: UPDATE_PROJECTSTATUS
      });
      console.log(obj);
    });
};