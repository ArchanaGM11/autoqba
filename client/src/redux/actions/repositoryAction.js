import { FETCH_REPOSITORY, NEW_REPOSITORY,EDIT_REPOSITORY, UPDATE_REPOSITORY, DELETE_REPOSITORY, UPDATE_REPOSITORY_STATUS } from './types';
import fetch from '../../utils/leoFetch'
import axios from '../../utils/leoAxios'

export const createRepository = (repositoryData) => dispatch => {
  console.log('action called');
  fetch('http://localhost:4000/object/repository/add', {
    body: JSON.stringify(repositoryData),
    headers: {
      'content-type': 'application/json'
    },
    method: 'POST'
  }).then((res) => {
    if (res.status === 201) { alert('Repository Succesfully Added'); }
    else if (res.status === 400) { alert('Error In Repository Creation'); }
    else if (res.status === 409) { alert('Duplicate Repository'); }
    })
    .then(res => res.json())
    .then(obj => dispatch({
      payload: obj,
      type: NEW_REPOSITORY
    })).catch(err => console.log(err));
  console.log('Successfully added....')
  console.log(repositoryData);
};

export const fetchRepository = () => dispatch => {
  fetch('http://localhost:4000/object/repository')
    .then(res => res.json())
    .then(repository =>
      dispatch({
        payload: repository,
        type: FETCH_REPOSITORY
      })
    );
}
/* GET ONE REPOSITORY*/
export const editRepository = repositoryId => dispatch => {
  console.log("Edit Post archana --> params _id " + repositoryId);
  fetch("http://localhost:4000/object/repository/" + repositoryId, {
    headers: {
      "content-type": "application/json"
    },
    method: "GET"
  })
    .then(res => res.json())
    .then(element => {
      console.log("Edit Element", element);
      dispatch({
        payload: element,
        type: EDIT_REPOSITORY
      });
    })
    .catch(function(error) {
      console.log(error);
    });
};


export const updateRepository = (repositoryId, objData) => dispatch => {
  console.log("update Post --> params _id" + " " + repositoryId);
  fetch("http://localhost:4000/object/repository/" + repositoryId, {
    body: JSON.stringify(objData),
    headers: {
    "content-type": "application/json"
     },
  method: 'PUT',
   })
   .then((res) => {
    if (res.status === 204) { alert('Repository Succesfully Updated'); }
    else if (res.status === 500) { alert('Error In Repository Creation'); }
    else if (res.status === 409) { alert('Duplicate Repository'); }
  })
   .then(obj => {
  console.log(obj)
  dispatch({
  payload: obj,
  type: UPDATE_REPOSITORY
   })
  console.log("obj");
   });
   };
   
   export const deleteRepository=repositoryId=>dispatch=> {
    console.log(2,"Inside DELETE");
    fetch("http://localhost:4000/object/repository/"+repositoryId, {
      headers: {
      "content-type":"application/json"
       },
    method:'DELETE'
     }).then((res) => {
      if (res.status === 204) { alert('Repository Deleted Succesfully'); }
      else if (res.status === 500) { alert('Error In Deleting Repository'); }
      else if (res.status === 404) { alert('Repository Not Found'); }
      })
    dispatch({
    id:repositoryId,
    type: DELETE_REPOSITORY
     })
     }
    
     export const updateRepositoryStatus = (repositoryId) => dispatch => {
      console.log("update Post --> params _id" + " " + repositoryId);
      fetch("http://localhost:4000/object/repository/status/" + repositoryId, {
        // body: JSON.stringify(objData),
        headers: {
        "content-type": "application/json"
         },
      method: 'PUT',
       })
      //  .then((res) => {
      //   if (res.status === 204) { alert('Repository Succesfully Updated'); }
      //   else if (res.status === 500) { alert('Error In Repository Creation'); }
      //   else if (res.status === 409) { alert('Duplicate Repository'); }
      // })
       .then(obj => {
      console.log(obj)
      dispatch({
      payload: obj,
      type: UPDATE_REPOSITORY_STATUS
       })
      console.log("obj");
       });
       };