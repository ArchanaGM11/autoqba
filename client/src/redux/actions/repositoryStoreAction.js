import {
  NEW_OBJECT_STORE,
  FETCH_OBJECT_STORE,
  EDIT_OBJECT_STORE,
  UPDATE_OBJECT_STORE,
  DELETE_OBJECT_STORE,
  FILTER_OBJECT_STORE
} from "./types";
import fetch from "../../utils/leoFetch";
import axios from "../../utils/leoAxios";
/*POST A OBJECT*/
export const createRepositoryStore = repositoryStoreData => dispatch => {
  fetch("http://localhost:4000/object/repository/store/add", {
    body: JSON.stringify(repositoryStoreData),
    headers: {
      "content-type": "application/json"
    },
    method: "POST"
  })
    .then(res => {
      if (res.status === 201) {
        alert("Repository Store Succesfully Added");
      } else if (res.status === 400) {
        alert("Error In Repository Store Creation");
      } else if (res.status === 409) {
        alert("Duplicate Repository Store");
      }
    })
    .then(res => res.json())
    .then(obj =>
      dispatch({
        payload: obj,
        type: NEW_OBJECT_STORE
      })
    )
    .catch(err => console.log(err));
};

/*FETCH ALL OBJECT */
export const fetchRepositoryStore = () => dispatch => {
  fetch('http://localhost:4000/object/repository/store')
    .then(res => res.json())
    .then(repositoryStore =>
      {
        console.log("repo store",repositoryStore),
        dispatch({
          payload: repositoryStore,
          type: FETCH_OBJECT_STORE
        })
      }
    );
   
}

/* FILTER OBJECTS*/
export const filterRepositoryStore = repoId => dispatch => {
  fetch("http://localhost:4000/object/repository/store/filter/" + repoId)
    .then(res => res.json())
    .then(repositoryStore =>
      // console.log("filter aguthu : ",repositoryStore),
      dispatch({
        payload: repositoryStore,
        type: FILTER_OBJECT_STORE
      })
    );
};

/* GET ONE OBJECT*/
export const editRepositoryStore = repositoryObjectId => dispatch => {
  console.log("Edit Post archana obj--> params _id " + repositoryObjectId);
  fetch("http://localhost:4000/object/repository/store/" + repositoryObjectId, {
    headers: {
      "content-type": "application/json"
    },
    method: "GET"
  })
    .then(res => res.json())
    .then(objectelement => {
      console.log("Edit obj Element", objectelement);
      dispatch({
        payload: objectelement,
        type: EDIT_OBJECT_STORE
      });
    })
    .catch(function(error) {
      console.log(error);
    });
};

/*UPDATE A OBJECT */
export const updateRepositoryStore = (
  repositoryObjectId,
  objData
) => dispatch => {
  console.log("update Post --> params _id" + " " + repositoryObjectId);
  console.log("json.stringify : ",JSON.stringify(objData));
  fetch("http://localhost:4000/object/repository/store/" + repositoryObjectId, {
    body: JSON.stringify(objData),
    headers: {
      "content-type": "application/json"
    },
    method: "PUT"
  })
    .then(res => {
      if (res.status === 204) {
        alert("Repository Store Succesfully Updated");
      } else if (res.status === 500) {
        alert("Error In Repository Store Creation");
      } else if (res.status === 409) {
        alert("Duplicate Repository Store");
      }
    })
    .then(obj => {
      console.log(obj);
      dispatch({
        payload: obj,
        type: UPDATE_OBJECT_STORE
      });
      console.log("obj");
    });
};

/* DELETE A OBJECT*/
export const deleteRepositoryStore = repositoryObjectId => dispatch => {
  console.log(2, "Inside DELETE");
  fetch("http://localhost:4000/object/repository/store/" + repositoryObjectId, {
    headers: {
      "content-type": "application/json"
    },
    method: "DELETE"
  });
  dispatch({
    id: repositoryObjectId,
    type: DELETE_OBJECT_STORE
  });
};
