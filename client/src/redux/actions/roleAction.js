import { FETCH_ROLE, NEW_ROLE, EDIT_ROLE, UPDATE_ROLE, DELETE_ROLE, FETCH_SYSTEM_ROLE, FETCH_PROJECT_ROLE } from './types';
import fetch from '../../utils/leoFetch'
import axios from '../../utils/leoAxios'
import Global from '../../../src/components/layout/Global/Global';
import API_ROOT from '../../utils/api-config'

export const createRole = (values) => async dispatch => {
 
  await axios.post(API_ROOT+'/users/newrole',values)
       .then(resp => {
           //console.log(5,resp.data.userMessage);
             console.log(5,resp.data.status);
             console.log(183,resp.data.status);

           if (resp.data.status === "SUCCESS") {
            Global.alertContent = {
                closable: false,
                message: resp.data.userMessage,
                status: "success"
            }
            console.log("Alert data : " +Global.alertContent.message);
            dispatch({
                payload: {
                // message: resp.data.userMessage,
                // visible: true
                },
                type: NEW_ROLE
            })
        }
        else {
            Global.alertContent = {
                closable: true,
                message: resp.data.userMessage,
                status: 'error'
            }
            
            dispatch({
                payload: {
                    // ...forgot,
                    // message: resp.data.userMessage,
                    // visible: false
                },
                type: NEW_ROLE
            })
        }
        //    dispatch({
        //     payload: {
        //         message: resp.data.userMessage
        //     },
        //     type: RESET_PASSWORD
        // })
       });
      }


// export const createRole = (object) => dispatch => {
//   console.log('New role');
//   console.log("JWt Token : " +localStorage.getItem("token"));

//   fetch('http://192.168.22.39:4000/users/newrole', {

//     body: JSON.stringify(object),
//     headers: {
//       'Authorization':'Bearer ' +localStorage.getItem("token"),
//       'content-type': 'application/json'
//     },
//     method: 'POST'
//   })
//     .then(res => res.json())
//     .then(role => dispatch({
//       payload: role,
//       type: NEW_ROLE
//     }));
//   console.log('Successfully added....')
//   console.log(object);
// };

export const fetchRole = () => dispatch => {
  fetch(API_ROOT+'/users/role/rolelist')
    .then(res => res.json())
    .then(roles =>
      dispatch({
        payload: roles,
        type: FETCH_ROLE
      })
    );
}

export const fetchRoleWithParam = (roleType) => dispatch => {
  console.log("In fetchroleparam")
  let type = "";
  if(roleType==='System') {
    type = FETCH_SYSTEM_ROLE;
  } else if(roleType ==='Project') {
    type = FETCH_PROJECT_ROLE;
  }
  console.log("In fetchroleparam11111")
  fetch(API_ROOT+'/users/role/rolelist?RoleType='+roleType)
    .then(res => res.json())
    .then(roles =>
      dispatch({
        payload: roles,
        type
      })
    );
}

export const editRole = RoleId => dispatch => {
  console.log("Edit Role --> params _id " + RoleId);
  fetch(API_ROOT+"/users/role/" + RoleId, {
    headers: {
      "content-type": "roles/json"
    },
    method: "GET"
  })
    .then(res => res.json())
    .then(element => {
      console.log("Edit Element", element);
      dispatch({
        payload: element,
        type: EDIT_ROLE
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};
export const updateRole = (RoleId, objData) => dispatch => {
  console.log("update Post --> params _id" + " " + RoleId);
  fetch(API_ROOT+"/users/role/" + RoleId, {
    body: JSON.stringify(objData),
    headers: {
      "content-type": "application/json"
    },
    method: "PUT"
  })
    .then((res) => {
      if (res.status === 204) { alert('Role Succesfully Updated'); }
      else if (res.status === 500) { alert('Error In Role Creation'); }
      else if (res.status === 409) { alert('Duplicate Role'); }
    })
    .then(obj => {
      console.log(obj);
      dispatch({
        payload: obj,
        type: UPDATE_ROLE
      });
      console.log(obj);
    });
};
export const deleteRole = id => dispatch => {
  console.log(2, "Inside Delete action");
  fetch(API_ROOT+"/users/role/" + id, {
    headers: {
      "content-type": "application/json"
    },
    method: "DELETE"
  });
  dispatch({
    id: id,
    type: "DELETE_ROLE"
  });
};



