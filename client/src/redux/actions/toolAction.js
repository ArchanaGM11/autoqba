import {
  FETCH_TOOL,
  NEW_TOOL,
  EDIT_TOOL,
  UPDATE_TOOL,
  DELETE_TOOL,
  FILTER_TOOL,
  UPDATE_TOOLSTATUS
} from "./types";
import fetch from "../../utils/leoFetch";

export const createTool = projectData => dispatch => {
  console.log("action called");
  fetch("http://192.168.22.39:4000/api/tools", {
    body: JSON.stringify(projectData),
    headers: {
      "content-type": "application/json"
    },
    method: "POST"
  })
    .then(res => {
      if (res.status === 201) {
        alert("Tool Succesfully Added");
      } else if (res.status === 400) {
        alert("Error In Tool Creation");
      } else if (res.status === 409) {
        alert("Duplicate Tool");
      } else {
        alert("Error In Tool Creation");
      }
    })
    .then(res => res.json())
    .then(obj =>
      dispatch({
        payload: obj,
        type: NEW_TOOL
      })
    )
    .catch(err => console.log(err));
  console.log("Successfully added....");
  console.log(projectData);
};

export const fetchTool = () => dispatch => {
  fetch("http://192.168.22.39:4000/api/tools")
    .then(res => res.json())
    .then(projects =>
      dispatch({
        payload: projects,
        type: FETCH_TOOL
      })
    );
};

export const editTool = ToolId => dispatch => {
  console.log("Edit Application --> params _id " + ToolId);
  fetch("http://192.168.22.39:4000/api/tools/" + ToolId, {
    headers: {
      "content-type": "tools/json"
    },
    method: "GET"
  })
    .then(res => res.json())
    .then(element => {
      console.log("Edit Element", element);
      dispatch({
        payload: element,
        type: EDIT_TOOL
      });
    })
    .catch(function(error) {
      console.log(error);
    });
};
export const updateTool = (ToolId, objData) => dispatch => {
  console.log("update Post --> params _id" + " " + ToolId);
  fetch("http://192.168.22.39:4000/api/tools/" + ToolId, {
    body: JSON.stringify(objData),
    headers: {
      "content-type": "application/json"
    },
    method: "PUT"
  })
    .then(res => {
      if (res.status === 204) {
        alert("Tool Succesfully Updated");
      } else if (res.status === 500) {
        alert("Error In Tool Creation");
      } else if (res.status === 409) {
        alert("Duplicate Tool");
      }
    })
    .then(obj => {
      console.log(obj);
      dispatch({
        payload: obj,
        type: UPDATE_TOOL
      });
      console.log(obj);
    });
};
export const deleteTool = id => dispatch => {
  console.log(2, "Inside Delete action");
  fetch("http://192.168.22.39:4000/api/tools/" + id, {
    headers: {
      "content-type": "application/json"
    },
    method: "DELETE"
  });
  dispatch({
    id: id,
    type: DELETE_TOOL
  });
};

export const viewTool = id => dispatch => {
  console.log("Inside view Project");
  fetch("http://192.168.22.39:4000/api/tools/user/" + id)
    .then(res => res.json())
    .then(tools =>
      dispatch({
        payload: tools,
        type: FILTER_TOOL
      })
    );
};

export const updateToolstatus = id => dispatch=>{
  console.log(2, "Inside Deactive action");
  fetch("http://192.168.22.39:4000/api/tools/status/" + id, {
    headers: {
      "content-type": "application/json"
    },
    method: "PUT"
  })
    .then(obj => {
      console.log(obj);
      dispatch({
        payload: obj,
        type: UPDATE_TOOLSTATUS
      });
      console.log(obj);
    });
};