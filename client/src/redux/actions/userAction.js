import { FETCH_USER, NEW_USER, EDIT_USER, UPDATE_USER, DELETE_USER, CHANGE_PASSWORD, INMEMORY_ADD_PROJECT, INMEMORY_ADD_PROJECT1,UPDATE_USERSTATUS } from './types';
import fetch from '../../utils/leoFetch'
import axios from '../../utils/leoAxios'
import Global from '../../../src/components/layout/Global/Global';
import API_ROOT from '../../utils/api-config'

export const createUser = (values) => async dispatch => {

  await axios.post(API_ROOT + '/users/newuser', values)
    .then(resp => {
      console.log(5, resp.data.status);

      if (resp.data.status === "SUCCESS") {
        Global.alertContent = {
          closable: false,
          message: resp.data.userMessage,
          status: "success"
        }
        console.log("Alert data : " + Global.alertContent.message);
        dispatch({
          payload: {

          },
          type: NEW_USER
        })
      }
      else {
        Global.alertContent = {
          closable: true,
          message: resp.data.userMessage,
          status: 'error'
        }
        localStorage.removeItem("token");
        dispatch({
          payload: {

          },
          type: NEW_USER
        })
      }

    });
}


export const fetchUser = () => dispatch => {
  fetch(API_ROOT + '/users/userlist')
    .then(res => res.json())
    .then(users =>
      dispatch({
        payload: users,
        type: FETCH_USER
      })
    );
}

export const editUser = UserId => dispatch => {
  console.log("Edit User --> params _id " + UserId);
  fetch(API_ROOT + "/users/" + UserId, {
    headers: {
      "content-type": "roles/json"
    },
    method: "GET"
  })
    .then(res => res.json())
    .then(element => {
      console.log("Edit Element", element);
      fetch(API_ROOT + "/users/getUserProjectRole/" + UserId, {
        headers: {
          "content-type": "roles/json"
        },
        method: "GET"
      }).then(res => res.json()).then(projectrole => {
        dispatch({
          payload: element,
          projectrole,
          type: EDIT_USER
        });
      })
    })
    .catch(function (error) {
      console.log(error);
    });
};
export const updateUser = (UserId, objData) => dispatch => {
  console.log("update Post --> params _id" + " " + UserId);
  fetch(API_ROOT + "/users/" + UserId, {
    body: JSON.stringify(objData),
    headers: {
      "content-type": "application/json"
    },
    method: "PUT"
  })
    .then((res) => {
      if (res.status === 204) { alert('User Succesfully Updated'); }
      else if (res.status === 500) { alert('Error In User Creation'); }
      else if (res.status === 409) { alert('Duplicate User'); }
    })
    .then(obj => {
      console.log(obj);
      dispatch({
        payload: obj,
        type: UPDATE_USER
      });
      console.log(obj);
    });
};
export const userprojectUpdate = (objData) => dispatch => {
  console.log(111, objData);
  dispatch({
    payload: objData.project,
    type: INMEMORY_ADD_PROJECT1
  });
};

export const emptyprojectupdate = () => dispatch => {
  console.log("Inside EMPTY PROJECT................")
  dispatch({
    payload: [],
    type: INMEMORY_ADD_PROJECT1
  });
};




export const deleteUser = id => dispatch => {
  console.log(2, "Inside Delete action");
  fetch(API_ROOT + "/users/" + id, {
    headers: {
      "content-type": "application/json"
    },
    method: "DELETE"
  });
  dispatch({
    id: id,
    type: "DELETE_USER"
  });
};



export const changePassword = (values) => async dispatch => {
  console.log(123, values.Email);
  var options = {
    ConfirmPassword: values.ConfirmPassword,
    Email: values.Email,
    OldPassword: values.OldPassword
  }
  console.log("To backend : ", options)
  await axios.put(API_ROOT + '/users/changepassword', options)
    .then(resp => {

      if (resp.data.status === "SUCCESS") {
        Global.alertContent = {
          closable: false,
          message: resp.data.userMessage,
          status: "success"
        }
        dispatch({
          payload: {
          },
          type: CHANGE_PASSWORD
        })
      }
      else {
        Global.alertContent = {
          closable: true,
          message: resp.data.userMessage,
          status: 'error'
        }
        dispatch({
          payload: {
          },
          type: CHANGE_PASSWORD
        })
      }
    })
};

export const updateUserstatus = id => dispatch=>{
  console.log(2, "Inside Deactive action");
  fetch("http://192.168.22.39:4000/users/status/" + id, {
    headers: {
      "content-type": "application/json"
    },
    method: "PUT"
  })
    .then(obj => {
      console.log(obj);
      dispatch({
        payload: obj,
        type: UPDATE_USERSTATUS
      });
      console.log(obj);
    });
};