import { EDIT_OBJECT_STORE } from "../actions/types";

const initialState = {
  objectElement: {},
  typeObject: []
  //   elementId : '',
};

export default function reducer(state = initialState, action) {
  console.log(action.type, action.payload);
  switch (action.type) {
    case EDIT_OBJECT_STORE: {
      let data = [];
      if (action.payload !== undefined) {
        data = action.payload;
      }
      console.log(4545, data._id);
      return {
        //...state,
        objectElement: data,
        typeObject: data.objectType
        // elementId : data._id, // Check this line using console
      };
    }

    default:
      return state;
  }
}
