import { combineReducers } from "redux";
import loginReducer from "./loginReducer.js";
import applicationReducer from "../reducers/applicationReducer";
import toolReducer from "../reducers/toolReducer";
import projectReducer from "../reducers/projectReducer";
import editReducer from "../reducers/editReducer";
import forgotPasswordReducer from "../reducers/forgotPasswordReducer";
import userReducer from "../reducers/userReducer";
import repositoryReducer from "../reducers/repositoryReducer";
import editRepositoryReducer from "./editRepositoryReducer";
import tooleditReducer from "../reducers/tooleditReducer";
import projecteditReducer from "../reducers/projecteditReducer";
import repositoryStoreReducer from "../reducers/repositoryStoreReducer";
import roleReducer from './roleReducer.js';
import roleeditReducer from './roleeditReducer';
import editRepositoryStoreReducer from "./editRepositoryStoreReducer"
import privilegeReducer from './privilegeReducer';
import privilegeeditReducer from './privilegeeditReducer';
import usereditReducer from './usereditReducer';

const appReducer = combineReducers({
  application: applicationReducer,
  edit: editReducer,
  editOr: editRepositoryReducer,
  editStore: editRepositoryStoreReducer,
  editprivilege: privilegeeditReducer,
  editproject: projecteditReducer,
  editrole: roleeditReducer,
  edittool: tooleditReducer,
  edituser: usereditReducer,
  forgot: forgotPasswordReducer,
  loginData: loginReducer,
  privilege: privilegeReducer,
  project: projectReducer,
  repository: repositoryReducer,
  repositoryStore : repositoryStoreReducer,
  role: roleReducer,
  tool: toolReducer,
  user: userReducer
});
const rootReducer = (state, action) => {
  if (action.type === 'LOGOUT') {
    state = {}
  }

  return appReducer(state, action)
}

export default rootReducer;