import { FETCH_PRIVILEGE, 
    NEW_PRIVILEGE,
    EDIT_PRIVILEGE,
    UPDATE_PRIVILEGE,
    DELETE_PRIVILEGE } from '../actions/types';

const initialState = {
 data: {},
 datas:[],
 element:[],
 item:{},
 items:{}
};


export default function (state = initialState, action) {
 console.log('in reducer');
 switch (action.type) {
   case NEW_PRIVILEGE:
     console.log("added new privilege")
     return {
       ...state,
       data: action.payload
     };
   case FETCH_PRIVILEGE:
       return {
 
         ...state,
         datas: action.payload
     } 
   case EDIT_PRIVILEGE: {
     let elements = [];
     if (action.payload !== undefined) {
       elements = action.payload;
     }
     console.log(44, elements);
     return {
       element: elements
     };
   }
   case UPDATE_PRIVILEGE:
     return {
       item: action.payload
     };
   case DELETE_PRIVILEGE:
     return {
       ...state,
       items: action.payload
     };
   default:
     return state;
 }
}

