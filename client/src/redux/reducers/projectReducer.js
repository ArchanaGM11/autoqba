import { FETCH_PROJECT, NEW_PROJECT, UPDATE_PROJECT , DELETE_PROJECT, FILTER_PROJECT} from "../actions/types";

const initialState = {
  app: {},
  ele: {},
  item: {},
  items: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case NEW_PROJECT:
      return {
        ...state,
        item: action.payload
      };
    case FETCH_PROJECT:
      return {
        // ...state,
        items: action.payload
      };
      case UPDATE_PROJECT:
        return {
          // ...state,
          ele: action.payload
        };
      case DELETE_PROJECT:
        // const _id = action.payload._id;
        return {
          ...state,
          app: action.payload
        };
        case FILTER_PROJECT:
      return {
        ...state,
        items: action.payload
      };
    default:
      return state;
  }
}
