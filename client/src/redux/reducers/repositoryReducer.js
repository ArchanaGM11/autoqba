import { FETCH_REPOSITORY, NEW_REPOSITORY , UPDATE_REPOSITORY ,UPDATE_REPOSITORY_STATUS, DELETE_REPOSITORY } from '../actions/types';

const initialState = {

  deleteElement: {}, 
  item: {}, // add repository
  items: [], //fetch repository
  updateElement : {},
  updateStatus : ''
  //versionFilter:[]
};


export default function (state = initialState, action) {
  switch (action.type) {
    case NEW_REPOSITORY:
      console.log("inside new app reducer")
      return {
        ...state,
        item: action.payload
      };
    case FETCH_REPOSITORY:
      return {
        ...state,
        items: action.payload
      };
      case UPDATE_REPOSITORY:
      return {
        // ...state,
        updateElement: action.payload
      };
      case UPDATE_REPOSITORY_STATUS:
        return {
          // ...state,
          updateElement: action.payload
        };
    case DELETE_REPOSITORY:
      // const _id = action.payload._id;
      return {
        ...state,
        deleteElement : action.payload
      };
    default:
      return state;
  }
}

