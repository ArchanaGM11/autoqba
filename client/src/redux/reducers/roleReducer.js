import { FETCH_ROLE, 
     NEW_ROLE,
     EDIT_ROLE,
     UPDATE_ROLE,
     DELETE_ROLE,
     FETCH_SYSTEM_ROLE,
     FETCH_PROJECT_ROLE } from '../actions/types';

const initialState = {
  data: {},
  datas:[],
  datas1:[],
  datas2:[],
  element:[],
  item:{},
  items:{}
};


export default function (state = initialState, action) {
  console.log('in reducer');
  switch (action.type) {
    case NEW_ROLE:
      console.log("added new role")
      return {
        ...state,
        data: action.payload
      };
    case FETCH_ROLE:
      console.log("In role")
      return {
        ...state,
        datas: action.payload
      }
    case FETCH_SYSTEM_ROLE:
      return {
        ...state,
        datas1: action.payload
      }
    case FETCH_PROJECT_ROLE:
      return {
        ...state,
        datas2: action.payload
      } 
    case EDIT_ROLE: {
      let elements = [];
      if (action.payload !== undefined) {
        elements = action.payload;
      }
      console.log(44, elements);
      return {
        element: elements
      };
    }
    case UPDATE_ROLE:
      return {
        item: action.payload
      };
    case DELETE_ROLE:
      return {
        ...state,
        items: action.payload
      };
    default:
      return state;
  }
}

