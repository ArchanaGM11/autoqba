import {
  FETCH_USER, NEW_USER,
  EDIT_USER,
  UPDATE_USER,
  DELETE_USER,
  CHANGE_PASSWORD
} from '../actions/types';

const initialState = {
  data: {},
  datas: [],
  element: [],
  item: {},
  items: {},
  pass:{}
};


export default function (state = initialState, action) {
  console.log('in reducer');
  switch (action.type) {
    case NEW_USER:
      console.log("added new user")
      return {
        ...state,
        data: action.payload
      };
    case FETCH_USER:
      return {

        ...state,
        datas: action.payload
      }
    case EDIT_USER: {
      let elements = [];
      if (action.payload !== undefined) {
        elements = action.payload;
      }
      console.log(44, elements);
      return {
        element: elements
      };
    }
    case UPDATE_USER:
      return {
        item: action.payload
      };
    case DELETE_USER:
      return {
        ...state,
        items: action.payload
      };
    case CHANGE_PASSWORD:
      console.log("added password");
      return {
        ...state,
        pass: action.payload
      
      };
    default:
      return state;
  }
}

