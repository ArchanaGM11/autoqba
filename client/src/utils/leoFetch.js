function updateOptions(options) {
  const update = { ...options };
  update.headers = {
    ...update.headers,
    'Access-Control-Allow-Origin': '*',
    token: localStorage.getItem('token')
  };
  return update;
}

export default function leoFetch(url, options) {
  return fetch(url, updateOptions(options));
}
