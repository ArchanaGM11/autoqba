import React, { Component } from "react";
import { connect } from 'react-redux'
import { Row, Col, Card, Input, Form, Icon, Checkbox, Button, Layout, Alert, Tabs, Avatar, Result, AutoComplete } from 'antd';
import "../components/login/login.css";
import Global from "../components/layout/Global/Global";

const { TabPane } = Tabs;
const { Meta } = Card;
const operations = <AutoComplete
    className="certain-category-search"
    dropdownClassName="certain-category-search-dropdown"
    filterOption={(inputValue, option) =>
        option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}

    dropdownStyle={{ width: 300 }}
    size="small"
    style={{ width: '20rem' }}

    placeholder="Search by Project Name"
    optionLabelProp="value"
>
    <Input suffix={<Icon type="search" className="certain-category-icon" />} />
</AutoComplete>;



class Dashboard extends Component {
    
    constructor(props) {
        super(props);
        
    }

    handlesubmit = e => {
        e.preventDefault();
        Global.history.push("/addproject");
    }
    redirect = e => {
        e.preventDefault();
        Global.history.push("/launchproject");
    }

    render() {
        const projects = this.props.loginData.project;
        
        return (
            <div>
               
                <Row>
                    <br /><p style={{ fontWeight: "bold", fontSize: "23px", marginBottom: "0" }}>Projects</p>
                    <hr />
                </Row>
                <Row>
                    <Tabs tabBarExtraContent={operations}>
                        <TabPane tab="Assigned Project" key="1">
                            
                            <Row>
                                <Col >
                                
                                {projects.length !== 0?
                                        projects.map((listValue, index) => {
                                            return(
                                                    <Card size="small" style={{marginBottom:"3px", width: "80rem" }} >
                                                        <Row type="flex">
                                                            <Col >
                                                                <Avatar shape="square" icon="project" />
                                                            </Col>
                                                            <Col style={{ marginLeft: "4px", marginTop: "4px" }}>
                                                                <Button type="link" onClick={this.redirect} style={{ color: "#874d00", fontWeight: "bold", marginBottom: '10px', fontSize: "18px" }}>{listValue.projectname}</Button>
                                                            </Col>
                                                            <Col >
                                                                <Button style={{ background: "#135200", color: "white" }} size="small" shape="round" >InActive</Button>
                                                            </Col>
                                                        </Row>
                                                        <Row type="flex">
                                                            <Col>
                                                                <p style={{ marginLeft: '37px', marginBottom: '0', fontSize: "12px" }} >
                                                                    Roles : {listValue.Role}
                                                        </p>
                                                                <p style={{ marginLeft: '37px', marginBottom: '0', fontSize: "12px" }}>
                                                                    Created by : {listValue.projectname}
                                                        </p>
                                                            </Col>
                                                            <Col offset={13}>
                                                                <p style={{ marginLeft: '37px', marginBottom: '0', fontSize: "12px" }}>
                                                                    Created on : Sep-17-2019
                                                        </p>
                                                            </Col>
        
                                                        </Row>
                                                    </Card> 
                                            )}
                                            )
                                        :<Result icon={<Icon type="project" theme="twoTone" />}
                                        title="Welcome, create your own project!"
                                        extra={<Button onClick={this.handlesubmit} style={{ background: "#00474f", color: "white" }}>New Project</Button>} />    
                                    }
                                </Col>
                            </Row>
                        </TabPane>

                        <TabPane tab="My Project" key="2">
                            <Row>
                                <Col >
                                    
                                    {projects.length !== 0?
                                        projects.map((listValue, index) => {
                                            return(
                                                    <Card size="small" style={{marginBottom:"3px", width: "80rem" }} >
                                                        <Row type="flex">
                                                            <Col >
                                                                <Avatar shape="square" icon="project" />
                                                            </Col>
                                                            <Col style={{ marginLeft: "4px", marginTop: "4px" }}>
                                                                <Button type="link" style={{ color: "#874d00", fontWeight: "bold", marginBottom: '10px', fontSize: "18px" }}>{listValue.projectname}</Button>
                                                            </Col>
                                                            <Col >
                                                                <Button style={{ background: "#135200", color: "white" }} size="small" shape="round" >InActive</Button>
                                                            </Col>
                                                        </Row>
                                                        <Row type="flex">
                                                            <Col>
                                                                <p style={{ marginLeft: '37px', marginBottom: '0', fontSize: "12px" }} >
                                                                    Roles : Admin
                                                        </p>
                                                                <p style={{ marginLeft: '37px', marginBottom: '0', fontSize: "12px" }}>
                                                                    Created by : DINESH ANANDASAYANAM
                                                        </p>
                                                            </Col>
                                                            <Col offset={13}>
                                                                <p style={{ marginLeft: '37px', marginBottom: '0', fontSize: "12px" }}>
                                                                    Created on : Sep-17-2019
                                                        </p>
                                                            </Col>
        
                                                        </Row>
                                                    </Card> 
                                            )}
                                            )
                                        :<Result icon={<Icon type="project" theme="twoTone" />}
                                        title="Welcome, create your own project!"
                                        extra={<Button onClick={this.handlesubmit} style={{ background: "#00474f", color: "white" }}>New Project</Button>} />    
                                    }
                                </Col>
                            </Row>
                        </TabPane>

                    </Tabs>
                </Row>


            </div>

        );
    }
}
const mapStateToProps = state => ({ loginData: state.loginData });
export default connect(mapStateToProps)(Dashboard);