import React from "react";
// import ReactDOM from 'react-dom';
import "antd/dist/antd.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  createApplication,
  fetchApplication
} from "../redux/actions/applicationAction";
import Global from "../../src/components/layout/Global/Global";
// import './index.css';
import {
  Form,
  Input,
  Button,
  Switch,
  Select,
  Card,
  Breadcrumb,
  Row
} from "antd";

const { TextArea } = Input;
const { Option } = Select;

class Addapplication extends React.Component {
  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFieldsAndScroll((err, values) => {
      var myKey = "created_by";
      values[myKey] = this.props.uid;
      //const createdby = values.created_by.concat(this.props.lname)
      // values.created_by= createdby;
      var myKey1 = "updated_by";
      values[myKey1] = this.props.uid;
      //const updatedby = values.updated_by.concat(this.props.lname)
      // values.updated_by= updatedby;
      var myKey3 = "status";
      values[myKey3] = "new";

      if (!err) {
        values[`User_id`] = this.props.uid;
        console.log("Received values of form: ", values);
        this.props.createApplication(values);
        console.log(values.visibility);
        this.props.history.push("/viewapplication/");
      }
    });
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Application</Breadcrumb.Item>
                <Breadcrumb.Item>New Application</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>New Application</h5>
            </div>
            <div className="float-right">
              <Button type="primary" htmlType="submit">
                Create Application
              </Button>
            </div>
            <hr className="hrline"></hr>
          </Row>

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Name">
                    {getFieldDecorator("application_name", {
                      rules: [
                        {
                          message: "Please enter name",
                          required: true
                        },
                        {
                          max: 100,
                          message: "name should be accept maximum 20 characters"
                        },
                        {
                          message: "name should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Version">
                    {getFieldDecorator("application_version", {
                      rules: [
                        {
                          message: "Please enter version",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "version should be accept maximum 20 characters"
                        },
                        {
                          message: "version should be minimum 2 characters",
                          min: 1
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="UI/API Technology">
                    {getFieldDecorator("api_technology", {
                      rules: [
                        {
                          message: "Please enter UI/API technology",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "UI/API technology should be accept maximum 20 characters"
                        },
                        {
                          message:
                            "UI/API technology should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Tech Stack">
                    {getFieldDecorator("techstack", {
                      rules: [
                        {
                          message: "Please enter techstock",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "techstack should be accept maximum 1000 characters"
                        },
                        {
                          message: "techstack should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<TextArea autosize={{ minRows: 2, maxRows: 3 }} />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Environment">
                    {getFieldDecorator("environment", {
                      rules: [
                        {
                          message: "Please enter environment",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "environment should be accept maximum 20 characters"
                        },
                        {
                          message: "environment should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Developed By">
                    {getFieldDecorator("developed_by", {
                      rules: [
                        {
                          message: "Please enter developed by",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "developed by should be accept maximum 20 characters"
                        },
                        {
                          message:
                            "developed by should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Implemented By">
                    {getFieldDecorator("implemented_by", {
                      rules: [
                        {
                          message: "Please enter implemented by",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "implemented by should be accept maximum 20 characters"
                        },
                        {
                          message:
                            "implemented by should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("visibility", {
                      initialValue: "public",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

Addapplication.propTypes = {
  createApplication: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired,
  uid: PropTypes.object.isRequired,
  uname: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  item: state.application.item,
  uid: state.loginData.User_id,
  uname: state.loginData.userName
});
const WrappedCreateapplication = Form.create({ name: "register" })(
  Addapplication
);

export default connect(
  mapStateToProps,
  { createApplication }
)(WrappedCreateapplication);
