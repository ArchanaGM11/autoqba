import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, Select, Card, Breadcrumb, Row } from "antd";
import { createPrivilege } from "../redux/actions/privilegeAction";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import DynamicFields from "./dynamicFields";
const { TextArea } = Input;
const { Option } = Select;

class Createprivilege extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(JSON.stringify(values));
        console.log("Received values of form: ", values);
        this.props.createPrivilege(values);
        alert("privilege added successfully!!!");
      }else {
        alert("Enter correct details..");
      }
    });
  };
  redirecthome = e =>{
    e.preventDefault();
    this.props.history.push("/home");
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    const { form } = this.props;
    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Admin Console{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Privilege{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>New Privilege</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>New Privilege</h5>
            </div>
            <div className="float-right">
              <Button type="primary" htmlType="submit">
                Create Privilege
              </Button>
            </div>
            <hr className="hrline"></hr>
          </Row>

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Privilege">
                    {getFieldDecorator("Privilege", {
                      rules: [
                        {
                          message: "Please enter Privilege",
                          required: true
                        },
                        {
                          max: 20,
                          message: "Privilege should be accept maximum 20 characters"
                        },
                        {
                          message: "Privilege should be minimum 3 characters",
                          min: 3
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>
                
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Privilege Description">
                    {getFieldDecorator("PrivilegeDescription", {
                      rules: [
                        {
                          message: "Please enter PrivilegeDescription",
                          required: true
                        },
                        {
                          max: 1000,
                          message:
                            "PrivilegeDescription should be accept maximum 1000 characters"
                        },
                        {
                          message: "PrivilegeDescription should be minimum 4 characters",
                          min: 4
                        }
                      ]
                    })(<TextArea autosize={{ minRows: 2, maxRows: 3 }} />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Privilege Type">
                    {getFieldDecorator("PrivilegeType", {
                      rules: [
                        {
                          message: "Please input your Privilege!",
                          required: true
                        }
                      ]
                    })(
                      <Select placeholder="Select Privilege Type">
                        <Option value="System">System</Option>
                        <Option value="Project">Project</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Status">
                    {getFieldDecorator("Status", {
                      rules: [
                        {
                          message: "Please input your Status!",
                          required: true
                        }
                      ]
                    })(
                      <Select placeholder="Select status">
                        <Option value="Active">Active</Option>
                        <Option value="InActive">InActive</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Created by">
                    {getFieldDecorator("Created_by", {
                      initialValue: "Admin",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select>
                        <Option value="Admin">Admin</Option>
                        <Option value="SuperAdmin">Super Admin</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Updated by">
                    {getFieldDecorator("Updated_by", {
                      initialValue: "Admin",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select>
                        <Option value="Admin">Admin</Option>
                        <Option value="SuperAdmin">Super Admin</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>

            </Card>
          </div>
        </div>
      </Form>
    );
  }
}
Createprivilege.propTypes = {
  createPrivilege: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  data: state.privilege.data
});

const WrappedCreateprivilege = Form.create({ name: "register" })(Createprivilege);

export default connect(
  mapStateToProps,
  { createPrivilege }
)(WrappedCreateprivilege);
