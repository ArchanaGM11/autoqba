import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, Select, Card, Breadcrumb, Row } from "antd";
import {
  createProject,
  userupdateProject,
  emptyuserupdate
} from "../redux/actions/projectAction";
import { fetchUser } from "../redux/actions/userAction";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import DynamicFields from "./dynamicFields";
import EditableFormTable from "./addProjectTable";
import { fetchRoleWithParam } from "../redux/actions/roleAction";
import Global from "../../src/components/layout/Global/Global";
const { TextArea } = Input;
const { Option } = Select;

class Createproject extends React.Component {
  componentWillMount() {
    this.props.emptyuserupdate();
    this.props.fetchUser();
    this.props.fetchRoleWithParam("Project");
  }
  constructor() {
    super();
    this.state = {
      childVisible: false
    };
  }
  isUserValid() {
    console.log("val : ", this.state.username);
    let val = false;
    if (this.state.username && this.state.userrole) {
      val = true;
    }
    console.log("val : ", this.state.username);
    return val;
  }

  handleSubmit = e => {
    e.preventDefault();
    console.log("ggggggggggggg", this.props.uid);

    this.props.form.validateFieldsAndScroll((err, values) => {
      values[`created_by`] = this.props.uid;
      values[`updated_by`] = this.props.uid;
      values[`project_admin`] = this.props.uid;
      // const projectadmin = values.project_admin.concat(this.props.lname);
      //values.project_admin = projectadmin;
      values[`status`] = "new";

      if (!err) {
        const users = [];
        console.log("users : ", users);
        this.props.data3.forEach(user => {
          let u = user;
          u.username = user.username.User_id;
          u.userrole = user.userrole.Role_id;
          users.push(u);
        });
        values.user = users;
        values[`User_id`] = this.props.uid;
        console.log(JSON.stringify(values));
        ///  Object.assign({},this.props.data3,[]);
        console.log("Received values of form: ", values);
        //this.props.emptyuserupdate();
        this.props.createProject(values);
        //this.props.data3 = '';
        console.log("userrrrrr", this.props.data3);
        //this.props.history.push("/viewproject/");
        Global.history.push("/viewproject/");
      }
    });
  };
  // handleUserUpdate = e => {
  //   e.preventDefault();
  //   this.props.form.validateFieldsAndScroll((err, values) => {
  //     if (!err) {
  //       this.setState({ childVisible: true });
  //       var nuser = values.user;
  //       const user = values.user.concat(this.props.data3);
  //       values.user = user;
  //       var muser = values.user;
  //       var k = 0;
  //       for (let i = 0; i < muser.length; i++) {
  //         if (nuser[0].username === muser[i].username) {
  //           k++;
  //         }
  //       }
  //       if (k === 1) {
  //         this.props.userupdateProject(values);
  //         this.props.form.setFields({
  //           "user[0][username]": undefined,
  //           "user[0][userrole]": undefined
  //         });
  //       } else {
  //         alert("user already exist");
  //       }
  //     }
  //   });
  // };

  handleUserUpdate = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(100, values);
        this.setState({ childVisible: true });
        // let usrArr = [
        //   {
        //     username: values.username,
        //     userrole: values.userrole
        //   }
        // ];

        let usrArr = [
          {
            username: this.props.user[values.username],
            userrole: this.props.role[values.userrole]
          }
        ];
        console.log("usrArr : ", usrArr);

        let oldArr = this.props.data3;
        var k = 0;
        for (let i = 0; i < oldArr.length; i++) {
          if (usrArr[0].username === oldArr[i].username) {
            k++;
          }
        }
        if (k === 0) {
          usrArr = usrArr.concat(oldArr);
          console.log(101, usrArr);
          values.user = usrArr;
          this.props.userupdateProject(values);
        } else {
          alert("user already exist");
        }
        this.props.form.setFields({
          username: undefined,
          userrole: undefined
        });

        // var nuser = values.user;
        // const user = values.user.concat(this.props.data3);
        // values.user = user;
        // var muser = values.user;
        // var k = 0;
        // for (let i = 0; i < muser.length; i++) {
        //   if (nuser[0].username === muser[i].username) {
        //     k++;
        //   }
        // }
        // if (k === 1) {
        //   this.props.userupdateProject(values);
        //   this.props.form.setFields({
        //     "user[0][username]": undefined,
        //     "user[0][userrole]": undefined
        //   });
        // } else {
        //   alert("user already exist");
        // }
      }
    });
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const { form } = this.props;
    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Project</Breadcrumb.Item>
                <Breadcrumb.Item>New Project</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>New Project</h5>
            </div>
            <div className="float-right">
              <Button type="primary" htmlType="submit">
                Create Project
              </Button>
            </div>
            <hr className="hrline"></hr>
          </Row>

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Name">
                    {getFieldDecorator("project_name", {
                      rules: [
                        {
                          message: "Please enter name",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "project name should be accept maximum 100 characters"
                        },
                        {
                          message:
                            "project name should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Project Description">
                    {getFieldDecorator("Project_Desc", {
                      rules: [
                        {
                          message: "Please enter techstock",
                          required: true
                        },
                        {
                          max: 1000,
                          message:
                            "Project Description should be accept maximum 1000 characters"
                        },
                        {
                          message:
                            "Project Description should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(<TextArea autosize={{ minRows: 2, maxRows: 3 }} />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("visibility", {
                      initialValue: "public",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>

              <Row>
                <h5>Select Users & Roles</h5>
              </Row>
              <div className="row">
                <div className="col-md-5 col-sm-12">
                  <Form.Item label="Select Username">
                    {getFieldDecorator("username", {
                      rules: [
                        {
                          message: "Please select username",
                          required: false
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select username"
                      >
                        {this.props.user.map((obj, index) => (
                          <Option key={obj.UserName} value={index}>
                            {obj.UserName}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-5 col-sm-12">
                  <Form.Item label="Select Role">
                    {getFieldDecorator("userrole", {
                      rules: [
                        {
                          message: "Please select Role",
                          required: false
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select userrole"
                      >
                        {this.props.role.map((obj, index) => (
                          <Option key={obj.Role} value={index}>
                            {obj.Role}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-2 col-sm-12">
                  <Form.Item>&nbsp;</Form.Item>
                  <Form.Item>
                    <Button
                      type="primary"
                      shape="circle"
                      icon="plus"
                      onClick={this.handleUserUpdate}
                      disabled={this.isUserValid()}
                    ></Button>
                  </Form.Item>
                </div>
              </div>

              {/* <Form.Item>
                <Button type="primary" onClick={this.handleUserUpdate}>
                  {" "}
                  Add user
                </Button>
              </Form.Item> */}
              <Form.Item>
                {this.state.childVisible ? <EditableFormTable /> : null}
              </Form.Item>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

Createproject.propTypes = {
  createProject: PropTypes.func.isRequired,
  fetchUser: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired,
  uid: PropTypes.object.isRequired,
  uname: PropTypes.object.isRequired,
  //lname: PropTypes.object.isRequired,
  user: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  data3: state.editproject.data3,
  item: state.project.item,
  role: state.role.datas2,
  uid: state.loginData.User_id,
  uname: state.loginData.userName,
  //lname: state.loginData.LastName,
  user: state.user.datas
});

const WrappedCreateproject = Form.create({ name: "register" })(Createproject);

export default connect(
  mapStateToProps,
  {
    createProject,
    emptyuserupdate,
    fetchRoleWithParam,
    fetchUser,
    userupdateProject
  }
)(WrappedCreateproject);
