import React from "react";
import Global from "../../src/components/layout/Global/Global";
import "antd/dist/antd.css";
import '../assets/css/component-styles.css';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  fetchApplication,
  filterApplication
} from "../redux/actions/applicationAction";
import { fetchTool } from "../redux/actions/toolAction";
import { createRepository } from "../redux/actions/repositoryAction";
// import './index.css';
import {
  Form,
  Input,
  Breadcrumb,
  Button,
  Switch,
  Select,
  Card,
  Row
} from "antd";

const { TextArea } = Input;
const { Option } = Select;

class AddRepository extends React.Component {
  constructor(props) {
    super(props);
    /* props for application filtering */
    this.appName = null;
    this.appVersion = null;
    this.appsByName = [];
    this.appsByNameAndVersion = [];

    /* props for tools filtering */
    this.toolName = null;
    this.toolVersion = null;
    this.toolByName = [];
    this.toolByNameAndVerison = [];
  }

      handleSubmit = e => {
        e.preventDefault();


        this.props.form.validateFieldsAndScroll((err, values) => {
          var myKey = 'created_by';
          values[myKey] = this.props.fname;
          const createdby = values.created_by.concat(this.props.lname)
           values.created_by= createdby;
           var myKey1 = 'updated_by';
           values[myKey1] = this.props.fname;
           const updatedby = values.updated_by.concat(this.props.lname)
            values.updated_by= updatedby;

          if (!err) {
            console.log("Received values of form: ", values);
    
            this.props.createRepository(values);
            this.props.form.resetFields();
             this.props.history.push("/viewrepository");
          }
        });
      };

  /* Application Filteration*/
  appSelect = params => {
    // console.log("params pandi: ",params);
    this.props.form.setFieldsValue({
      application_version: undefined,
      environment_name: undefined
    });
    var app1 = this.props.app.filter(app => {
      return params === app._id;
    });
    this.appName = app1[0].application_name;
    this.appsByName = [];
    this.appsByName = this.props.app.filter(app => {
      return this.appName === app.application_name;
    });
    //  console.log("pandi: ",this.appsByName);
  };

  versionSelect = params => {
    this.appVersion = params;
    this.appsByNameAndVersion = [];
    this.appsByNameAndVersion = this.props.app.filter(app => {
      return (
        this.appName === app.application_name &&
        this.appVersion === app.application_version
      );
    });
    console.log("vers : ", this.appsByNameAndVersion);
  };
  /* Tool Filteration*/
  toolSelect = params => {
    this.props.form.setFieldsValue({ tool_version: undefined });
    console.log("params tool pandi: ", params);
    var tool1 = this.props.too.filter(too => {
      return params === too._id;
    });
    this.toolName = tool1[0].tool_name;
    this.toolByName = [];
    this.toolByName = this.props.too.filter(too => {
      return this.toolName === too.tool_name;
    });
  };

  toolVersionSelect = params => {
    this.toolVersion = params;
    this.toolByNameAndVerison = [];
    this.toolByNameAndVerison = this.props.too.filter(too => {
      return (
        this.toolName === too.tool_name && this.toolVersion === too.tool_version
      );
    });
    console.log("vers : ", this.toolByNameAndVerison);
  };

  onSearch(sear) {
    console.log("search:", sear);
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button
                    type="link"
                    onClick={e => {
                      e.preventDefault();
                      Global.history.push("/home");
                    }}
                  >
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                {" "}
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Repository{" "}
                </Button>
              </Breadcrumb.Item>
                <Breadcrumb.Item>New Repository</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>New Repository</h5>
            </div>
            <div className="float-right">
              <Button type="primary" htmlType="submit">
                Create Repository
              </Button>
            </div>
            <hr className="hrline"></hr>
          </Row>

          <div className="cardheader">
            <Card bordered={false} className="antcardborder">
              {/* <div className="row"> */}
              <div className="row">
                <div className="col-md-1 col-sm-1  justify-content-center align-self-center">                
                    <h6 className="application-title">Application :</h6>
                </div>
                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Name">
                    {getFieldDecorator("application_name", {
                      rules: [
                        {
                          message: "Please input your application_name!",
                          required: true
                        }
                      ]
                      //  onChange: this.handleChange
                    })(
                      <Select
                        layout="inline"
                        //  onClick={this.getAllApplication()}
                        showSearch
                        placeholder="Select Application"
                        onChange={this.appSelect}
                        onSearch={this.onSearch}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.props.app.map(repos => (
                          <Option
                            key={repos.application_name}
                            value={repos._id}
                          >
                            {repos.application_name}
                            {/* #{this.appsByName.length} */}
                          </Option>
                        ))}
                        {/* <Option value="pandi">pandi</Option>
                <Option value="archana">archana</Option> */}
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Version">
                    {getFieldDecorator("application_version", {
                      rules: [
                        {
                          message: "Please input your application_version!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        //   onClick={this.getAllApplication()}
                        layout="inline"
                        showSearch
                        placeholder="Application Version"
                        disabled={this.appsByName.length < 1}
                        onChange={this.versionSelect}
                        onSearch={this.onSearch}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.appsByName.map(app => (
                          <Option key={app._id} value={app.application_version}>
                            {app.application_version}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-3 col-sm-3 ">
                  <Form.Item label="Environment">
                    {getFieldDecorator("environment_name", {
                      rules: [
                        {
                          message: "Please input your environment_name!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Environment"
                        disabled={this.appsByNameAndVersion.length < 1}
                        onSearch={this.onSearch}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.appsByNameAndVersion.map(repos => (
                          <Option key={repos._id} value={repos.environment}>
                            {repos.environment}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                </div>
                <div className="row">
                <div className="col-md-1 col-sm-1 justify-content-center align-self-center">
                  <h6 className="application-title">Tool :</h6>
                </div>
                <div className="col-md-3 col-sm-3 ">
                  <Form.Item label="Name">
                    {getFieldDecorator("tool_name", {
                      rules: [
                        {
                          message: "Please input your tool_name!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        // onClick={this.getAllTools()}
                        showSearch
                        placeholder="Select Tool Name "
                        onChange={this.toolSelect}
                        onSearch={this.onSearch}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.props.too.map(repos => (
                          <Option key={repos.tool_name} value={repos._id}>
                            {repos.tool_name}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Version">
                    {getFieldDecorator("tool_version", {
                      rules: [
                        {
                          message: "Please input your tool_version!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        // onClick={this.getAllTools()}
                        showSearch
                        placeholder="Tool Version"
                        disabled={this.toolByName.length < 1}
                        onChange={this.toolVersionSelect}
                        onSearch={this.onSearch}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.toolByName.map(too => (
                          <Option key={too._id} value={too.tool_version}>
                            {too.tool_version}
                          </Option>
                        ))}

                        {/* ))} */}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                </div>
                <div className="row">
                <div className="col-md-1 col-sm-1 justify-content-center align-self-center">
                  <h6 className="application-title">OR :</h6>
                </div>
                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Name">
                    {getFieldDecorator("or_name", {
                      rules: [
                        {
                          message: "Please input your or_name!",
                          required: true
                        }
                      ]
                    })(
                      <Input
                        type="text"
                        name="or_name"
                        className="form-control"
                        placeholder="Enter the OR Name"
                        // onChange={this.onChange}
                      />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("visibility", {
                      initialValue: "public",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                {/* </div> */}
              </div>
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

AddRepository.propTypes = {
  createRepository: PropTypes.func.isRequired,
  fetchApplication: PropTypes.func.isRequired,
  fetchTool: PropTypes.func.isRequired,
  filterApplication: PropTypes.func.isRequired,
  fname: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  lname: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  app: state.application.items,
  fname: state.loginData.FirstName,
  item: state.repository.item,
  lname: state.loginData.LastName,
  too: state.tool.items,
  versionFilter: state.application.versionFilter
});

const WrappedCreateRepository = Form.create({ name: "register" })(
  AddRepository
);

export default connect(
  mapStateToProps,
  { createRepository, fetchApplication, fetchTool, filterApplication }
)(WrappedCreateRepository);
