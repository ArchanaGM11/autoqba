import React,{ Component } from 'react';
import { Form, Input, Button, Select, Card, Breadcrumb, Row } from "antd";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import DynamicFields from "./dynamicFields";
import "antd/dist/antd.css";
const { TextArea } = Input;
const { Option } = Select;
import {createRepositoryStore} from "../redux/actions/repositoryStoreAction";
import { withRouter } from "react-router";
 class AddRepositoryStore extends React.Component {

  constructor(props) {
    super(props);
  
      this.editedComponent ='';

      this.state = {finalObj: ''};
      
    
  }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
          console.log('Values frm Object Store',values);
          if (!err) {
            console.log(JSON.stringify(values));
         let keey = 'orrefid'
         values[`orrefid`]=this.editedComponent;
          //  console.log('veppampatti ',values); 
          this.props.createRepositoryStore(values);
          
           this.props.history.push("/viewrepositorystore/"+this.editedComponent);
          }
        });
      };
      
    render(){

      const { match, location, history } = this.props;
    let myLoc = JSON.stringify(location.pathname);
    var finalstr = myLoc.replace(/"/g,"");
    this.editedComponent =finalstr.substring(finalstr.lastIndexOf('/')+1);
    //   console.log('pandian',this.editedComponent);
        const { getFieldDecorator } = this.props.form;
        const { form } = this.props;
        return (
            <Form onSubmit={this.handleSubmit}>
            <div className="container-fluid">
              <div className="Breadcrumb-top">
                <Row>
                  <Breadcrumb>
                    <Breadcrumb.Item>
                      {" "}
                      <Button type="link" onClick={this.redirecthome}>
                        {" "}
                        Home{" "}
                      </Button>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                      <Button type="link" onClick={this.redirecthome}>
                        {" "}
                        Repository Store{" "}
                      </Button>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>New Repository Store</Breadcrumb.Item>
                  </Breadcrumb>
                </Row>
              </div>
    
              <Row className="margintop">
                <div className="float-left">
                  <h5>New Store</h5>
                </div>
                <div className="float-right">
                  <Button type="primary" htmlType="submit">
                    Create Repository Store
                  </Button>
                </div>
                <hr className="hrline"></hr>
              </Row>
    
              <div className="cardheader">
                <Card className="antcardborder">
                  <div className="row">
                  
                  <div className="col-md-6 col-sm-12">
                      <Form.Item label="Select Class & Name">
                        <DynamicFields
                          {...form}
                          name="objectType"
                          fields={[
                            {
                              field: () => (
                                <Input />
                              ),
                              name: "objectClass"
                            },
                            {
                              field: () => (
                                <Input />
                              ),
                              name: "objectName"
                            }
                          ]}
                        />
                      </Form.Item>
                    </div>
                                     
                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Locator Name">
                        {getFieldDecorator("locator", {
                          rules: [
                            {
                              message: "Please enter Locator name",
                              required: true
                            },
                            {
                              max: 20,
                              message: "name should be accept maximum 10 characters"
                            },
                            {
                              message: "name should be minimum 3 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Locator Value">
                        {getFieldDecorator("locator_value", {
                          rules: [
                            {
                              message: "Please enter Locator Value",
                              required: true
                            },
                            {
                              max: 20,
                              message: "name should be accept maximum 10 characters"
                            },
                            {
                              message: "name should be minimum 3 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Screen">
                        {getFieldDecorator("screen", {
                          rules: [
                            {
                              message: "Please enter Screen name",
                              required: true
                            },
                            {
                              max: 20,
                              message: "name should be accept maximum 10 characters"
                            },
                            {
                              message: "name should be minimum 3 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Field">
                        {getFieldDecorator("field", {
                          rules: [
                            {
                              message: "Please enter Field name",
                              required: true
                            },
                            {
                              max: 20,
                              message: "name should be accept maximum 10 characters"
                            },
                            {
                              message: "name should be minimum 3 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>
                  </div>
                </Card>
              </div>
            </div>
          </Form>
        );
    };
}

AddRepositoryStore.propTypes = {
    createRepositoryStore: PropTypes.func.isRequired,
    
  };
  
  const mapStateToProps = state => ({
    objectItem: state.repositoryStore.objectItem
  });

const AddRepositoryStoreForm = Form.create({ name: "register" })(AddRepositoryStore);


export default connect(
    mapStateToProps,
    { createRepositoryStore }
  )(AddRepositoryStoreForm);

  const ShowTheLocationWithRouter = withRouter(AddRepositoryStore);
  