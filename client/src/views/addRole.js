import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, Select, Card, Breadcrumb, Row, Col, Alert } from "antd";
import { createRole } from "../redux/actions/roleAction";
import { fetchPrivilege } from "../redux/actions/privilegeAction";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import DynamicFields from "./dynamicFields";
import Global from '../components/layout/Global/Global'
const { TextArea } = Input;
const { Option } = Select;


function handleChange(value) {
  console.log(`selected ${value}`);
}

class Createrole extends React.Component {

  componentDidMount() {
    this.props.fetchPrivilege();
  }
  componentWillMount(){
    Global.clearAlert();
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(JSON.stringify(values));
        console.log("Received values of form: ", values);
        this.props.createRole(values);
      }
    });
  };
  redirecthome = e =>{
    e.preventDefault();
    this.props.history.push("/home");
  }
  handleClose(){
    //this.props.history.push("/viewuser/");
   Global.history.push("/viewrole");
   }
  

  render() {
    const { getFieldDecorator } = this.props.form;
    
            
    const { form } = this.props;
    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Admin Console{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Role{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>New Role</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>New Role</h5>
            </div>
            <div className="float-right">
              <Button type="primary" htmlType="submit">
                Create Role
              </Button>
            </div>
            <hr className="hrline"></hr>
          </Row>

          <Row type="flex" justify="center">
           <Col span={8}>
                {
                        Global.alertContent.message?
                        <Alert
                          message={Global.alertContent.status.toUpperCase()}
                          description={Global.alertContent.message}
                          type={Global.alertContent.status}
                          closable={Global.alertContent.closable}
                          closeText="OK"
                          afterClose={this.handleClose}
                          showIcon
                        />:null
                }
          </Col>
         </Row>

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Role">
                    {getFieldDecorator("Role", {
                      rules: [
                        {
                          message: "Please enter Role",
                          required: true
                        },
                        {
                          max: 20,
                          message: "Role should be accept maximum 20 characters"
                        },
                        {
                          message: "Role should be minimum 3 characters",
                          min: 3
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>
                
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Role Description">
                    {getFieldDecorator("RoleDescription", {
                      rules: [
                        {
                          message: "Please enter RoleDescription",
                          required: true
                        },
                        {
                          max: 1000,
                          message:
                            "RoleDescription should be accept maximum 1000 characters"
                        },
                        {
                          message: "RoleDescription should be minimum 4 characters",
                          min: 4
                        }
                      ]
                    })(<TextArea autosize={{ minRows: 2, maxRows: 3 }} />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Role Type">
                    {getFieldDecorator("RoleType", {
                     
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select placeholder="Select Role Type">
                        <Option value="System">System</Option>
                        <Option value="Project">Project</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Privileges">
                    {getFieldDecorator("Privilege", {
                      rules: [
                        {
                          message: "Please input your Privilege!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        mode="multiple"
                        placeholder="Please select privileges"
                        layout="inline"
                        showSearch
                        placeholder="Select Privilege"
                        onChange={handleChange}
                      >
                        {this.props.privilege.map(obj => (
                          <Option
                            key={obj.Privilege}
                            value={obj.Privilege}
                          >
                            {obj.Privilege}
                          </Option>
                        ))}
                        
                      </Select>
                    )}
                  </Form.Item>
                
                </div>
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Status">
                    {getFieldDecorator("Status", {
                      rules: [
                        {
                          message: "Please input your Status!",
                          required: true
                        }
                      ]
                    })(
                      <Select  placeholder="Select status" >
                        <Option value="Active">Active</Option>
                        <Option value="InActive">InActive</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Created by">
                    {getFieldDecorator("Created_by", {
                      initialValue: "Admin",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select >
                        <Option value="Admin">Admin</Option>
                        <Option value="SuperAdmin">Super Admin</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
                
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Updated by">
                    {getFieldDecorator("Updated_by", {
                      initialValue: "Admin",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select>
                        <Option value="Admin">Admin</Option>
                        <Option value="SuperAdmin">Super Admin</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>

              
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}
Createrole.propTypes = {
  createRole: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  fetchPrivilege: PropTypes.func.isRequired,
  privilege: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  data: state.role.data,
  privilege: state.privilege.datas
});

const WrappedCreaterole = Form.create({ name: "register" })(Createrole);

export default connect(
  mapStateToProps,
  { createRole, fetchPrivilege }
)(WrappedCreaterole);
