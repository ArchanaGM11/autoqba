import React from "react";
// import ReactDOM from 'react-dom';
import "antd/dist/antd.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createTool, fetchTool } from "../redux/actions/toolAction";
import Global from "../../src/components/layout/Global/Global";
// import './index.css';
import {
  Form,
  Input,
  Button,
  Switch,
  Select,
  Card,
  Breadcrumb,
  Row
} from "antd";
const { Option } = Select;

class AddTool extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      var myKey = "created_by";
      values[myKey] = this.props.uname;
      //const createdby = values.created_by.concat(this.props.lname)
      // values.created_by= createdby;
      var myKey1 = "updated_by";
      values[myKey1] = this.props.uname;
      // const updatedby = values.updated_by.concat(this.props.lname)
      //  values.updated_by= updatedby;
      var myKey3 = "status";
      values[myKey3] = "new";

      if (!err) {
        values[`User_id`] = this.props.uid;
        console.log("Received values of form: ", values);
        this.props.createTool(values);
        this.props.history.push("/viewtool/");
      }
    });
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Tool</Breadcrumb.Item>
                <Breadcrumb.Item>New Tool</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>New Tool</h5>
            </div>
            <div className="float-right">
              <Button type="primary" htmlType="submit">
                Create Tool
              </Button>
            </div>
            <hr className="hrline"></hr>
          </Row>

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Name">
                    {getFieldDecorator("tool_name", {
                      rules: [
                        {
                          message: "Please enter name",
                          required: true
                        },
                        {
                          max: 100,
                          message: "name should be accept maximum 20 characters"
                        },
                        {
                          message: "name should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Version">
                    {getFieldDecorator("tool_version", {
                      rules: [
                        {
                          message: "Please enter version",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "version should be accept maximum 20 characters"
                        },
                        {
                          message: "version should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Technology">
                    {getFieldDecorator("technology", {
                      rules: [
                        {
                          message: "Please enter technology",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "technology should be accept maximum 20 characters"
                        },
                        {
                          message: "technology should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Window Objects">
                    {getFieldDecorator("window_objects", {
                      rules: [
                        {
                          message: "Please enter window objects",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "window objects should be accept maximum 20 characters"
                        },
                        {
                          message:
                            "window objects should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Screen Objects">
                    {getFieldDecorator("screen_objects", {
                      rules: [
                        {
                          message: "Please enter screen objects",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "screen objects should be accept maximum 20 characters"
                        },
                        {
                          message:
                            "screen objects should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                {/* <div className="col-md-6 col-sm-12">
                  <Form.Item label="Status">
                    {getFieldDecorator("status", {
                      rules: [
                        {
                          message: "Please enter status",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "status should be accept maximum 20 characters"
                        },
                        {
                          message: "status should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div> */}

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Field Objects">
                    {getFieldDecorator("field_objects", {
                      rules: [
                        {
                          message: "Please enter field objects",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "field objects should be accept maximum 20 characters"
                        },
                        {
                          message:
                            "field objects should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(<Input />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("visibility", {
                      initialValue: "public",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

AddTool.propTypes = {
  createTool: PropTypes.func.isRequired,
  //fname: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  uid: PropTypes.object.isRequired,
  uname: PropTypes.object.isRequired
  //lname: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  //fname: state.loginData.FirstName,
  item: state.tool.item,
  uid: state.loginData.User_id,
  uname: state.loginData.userName
  // lname: state.loginData.LastName
});

const WrappedCreatetool = Form.create({ name: "register" })(AddTool);
export default connect(
  mapStateToProps,
  { createTool }
)(WrappedCreatetool);
