import React from "react";
import "antd/dist/antd.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { createUser,userprojectUpdate,emptyprojectupdate } from "../redux/actions/userAction";
import { fetchRoleWithParam } from '../redux/actions/roleAction';
import { fetchProject } from '../redux/actions/projectAction';
import { Form, Input, Button, Switch, Select, Card,Breadcrumb,Row,Alert,Col } from "antd";
import DynamicFields from "./dynamicFields";
import EditableFormTable from "./addUserTable"
import Global from '../components/layout/Global/Global'
const { TextArea } = Input;
const { Option } = Select;

class Adduser extends React.Component {
  componentWillMount() {
    console.log("Projects",this.props.project);
    this.props.emptyprojectupdate();
    this.props.fetchProject();
    this.props.fetchRoleWithParam('System');
    this.props.fetchRoleWithParam('Project');
    console.log("Projects",this.props.project);
  }
   
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const project = this.props.data3;
        values.project = project; 
        console.log(JSON.stringify(values));
        console.log("Received values of form: ", values);
        this.props.createUser(values);
      }else {
        alert("Enter correct details..");
      }
    });
  };
  redirecthome = e =>{
    e.preventDefault();
    this.props.history.push("/home");
  }
  // componentWillMount(){
  //   Global.clearAlert();
  // }

  handleClose(){
    //this.props.history.push("/viewuser/");
    Global.history.push("/viewuser");
   }

   handleProjectUpdate = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
    
        var nproject = values.project;
        const project = values.project.concat(this.props.data3);
        values.project = project;
       
          this.props.userprojectUpdate(values);
         
    });
  }
  
  render() {
    const { getFieldDecorator } = this.props.form;
    const { form } = this.props;
    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>New User</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>New User</h5>
            </div>
            <div className="float-right">
              <Button type="primary" htmlType="submit">
                Create User
              </Button>
            </div>
            <hr className="hrline"></hr>
          </Row>

          <Row type="flex" justify="center">
           <Col span={8}>
                {
                        Global.alertContent.message?
                        <Alert
                          message={Global.alertContent.status.toUpperCase()}
                          description={Global.alertContent.message}
                          type={Global.alertContent.status}
                          closable={Global.alertContent.closable}
                          closeText="OK"
                          afterClose={this.handleClose}
                          showIcon
                        />:null
                }
          </Col>
         </Row>
          <Row>
              <div className="cardheader">
                <Card className="antcardborder">
                  <div className="row">
                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="First Name">
                        {getFieldDecorator("FirstName", {
                          rules: [
                            {
                              message: "Please enter FirstName",
                              required: true
                            },
                            {
                              max: 20,
                              message: "FirstName should be accept maximum 20 characters"
                            },
                            {
                              message: "FirstName should be minimum 4 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Last Name">
                        {getFieldDecorator("LastName", {
                          rules: [
                            {
                              message: "Please enter LastName",
                              required: true
                            },
                            {
                              max: 20,
                              message:
                                "LastName should be accept maximum 20 characters"
                            },
                            {
                              message: "LastName should be minimum 2 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="User Name">
                        {getFieldDecorator("UserName", {
                          rules: [
                            {
                              message: "Please enter UserName",
                              required: true
                            },
                            {
                              max: 20,
                              message:
                                "UserName should be accept maximum 20 characters"
                            },
                            {
                              message:
                                "UserName should be minimum 8 characters",
                              min: 8
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Email">
                        {getFieldDecorator("Email", {
                          rules: [
                            {
                              message: "Please enter valid Email",
                              required: true,
                              type:'email'
                            },
                            {
                              max: 35,
                              message:
                                "Email should be accept maximum 35 characters"
                            },
                            {
                              message: "Email should be minimum 4 characters",
                              min: 4
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>

                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Company Name">
                        {getFieldDecorator("CompanyName", {
                          rules: [
                            {
                              message: "Please enter CompanyName",
                              required: true
                            },
                            {
                              max: 20,
                              message:
                                "CompanyName should be accept maximum 20 characters"
                            },
                            {
                              message: "CompanyName should be minimum 4 characters",
                              min: 3
                            }
                          ]
                        })(<Input />)}
                      </Form.Item>
                    </div>
                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Select Role">
                      {getFieldDecorator("Role", {
                          rules: [
                            {
                              message: "Please input your Role!",
                              required: true
                            }
                          ]
                        })(
                                <Select
                                  layout="inline"
                                  showSearch
                                  placeholder="Select Role"
                                >
                                  {this.props.systemRole.map(obj => (
                                    <Option
                                      key={obj.Role}
                                      value={obj.Role}
                                    >
                                      {obj.Role}
                                    </Option>
                                  ))}
                                </Select>
                            
                            )}
                      </Form.Item>
                    </div>
                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Visibility">
                        {getFieldDecorator("Visibility", {
                          initialValue: "Public",
                          rules: [
                            {
                              message: "Please input your Visibility!",
                              required: true
                            }
                          ]
                        })(
                          <Select style={{ width: 200 }}>
                            <Option value="Public">Public</Option>
                            <Option value="Private">Private</Option>
                          </Select>
                        )}
                      </Form.Item>
                    </div>
                    <div className="col-md-6 col-sm-12">
                      <Form.Item label="Status">
                        {getFieldDecorator("Status", {
                          initialValue: "Active",
                          rules: [
                            {
                              message: "Please input your Status!",
                              required: true
                            }
                          ]
                        })(
                          <Select style={{ width: 200 }}>
                            <Option value="Active">Active</Option>
                            <Option value="InActive">InActive</Option>
                          </Select>
                        )}
                      </Form.Item>
                    </div>
                    <div className="col-md-6 col-sm-12">
                  <Form.Item label="Select Project & Role">
                    <DynamicFields
                      {...form}
                      name="project"
                      fields={[
                        {
                          field: () => (
                            <Select
                              layout="inline"
                              showSearch
                              style={{ width: "100%" }}
                              placeholder="Select Project"
                            >
                              {this.props.project.map(obj => (
                                <Option key={obj.project_name} value={obj.project_id}>
                                  {obj.project_name}
                                </Option>
                              ))}
                            </Select>
                          ),
                          name: "projectname"
                        },
                        {
                          field: () => (
                            <Select
                                  layout="inline"
                                  showSearch
                                  placeholder="Select Role"
                                >
                                  {this.props.projectRole.map(obj => (
                                    <Option
                                      key={obj.Role}
                                      value={obj.Role_id}
                                    >
                                      {obj.Role}
                                    </Option>
                                  ))}
                                </Select>
                          ),
                          name: "Role"
                        }
                      ]}
                    />
                  </Form.Item>
                </div>
                    
                 
                  
                  </div>
                </Card>
                <Form.Item>
                    <Button type="primary" onClick={this.handleProjectUpdate}>
                      {" "}
                      Add project
                    </Button>
                  </Form.Item>
                <Form.Item>
                    <Row className="margintop">
                      <div className="float-left">
                        <h5>Project List</h5>
                      </div>
                    </Row>
                    <EditableFormTable></EditableFormTable>
                  </Form.Item>
              </div>
          </Row>
          
        </div>
      </Form>
    );
  }
}

Adduser.propTypes = {
  createUser: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  fetchProject:PropTypes.func.isRequired,
  fetchRoleWithParam: PropTypes.func.isRequired,
  project: PropTypes.object.isRequired,
  role: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  data: state.user.data,
  data3: state.edituser.data3, 
  project:state.project.items,
  projectRole:state.role.datas2,
  systemRole: state.role.datas1
  
});

const Createuserform = Form.create({ name: "register" })(
    Adduser
);

export default connect(
  mapStateToProps,
  { createUser,fetchRoleWithParam,fetchProject,userprojectUpdate, emptyprojectupdate }
)(Createuserform);
