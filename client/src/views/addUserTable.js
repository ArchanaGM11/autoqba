import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Select } from "antd";
import { Table, Popconfirm } from "antd";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { fetchProject } from "../redux/actions/projectAction";
import { fetchRole } from '../redux/actions/roleAction';
const { Option } = Select;
const EditableContext = React.createContext();
let data3 = [];
export class EditableCell extends React.Component {
  constructor(props) {
    super(props);
    this.state = { disabled: true };
  }

  getInput = () => {
    if (this.props.inputType === "text") {
      return <Input disabled={this.state.disabled ? "disabled" : ""} />;
    } else if (this.props.inputType === "list") {
      return (
        <Select
                                  layout="inline"
                                  showSearch
                                  placeholder="Select Role"
                                >
                                  {this.props.role.map(obj => (
                                    <Option
                                      key={obj.RoleName}
                                      value={obj.RoleName}
                                    >
                                      {obj.RoleName}
                                    </Option>
                                  ))}
                                </Select>
      );
    }
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              initialValue: record[dataIndex],
              rules: [
                {
                  message: `Please Input ${title}!`,
                  required: true
                }
              ]
            })(this.getInput())}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return (
      <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
    );
  }
}
class EditableTable extends React.Component {
  componentDidMount() {
    //this.props.fetchUser();
  }

  constructor(props) {
    super(props);
    this.state = { data3, editingKey: "" };
    this.columns = [
      {
        dataIndex: "projectname",
        editable: true,
        title: "Project name",
        width: "40%"
      },
      {
        dataIndex: "Role",
        editable: true,
        title: "Role",
        width: "40%"
      },
      {
        dataIndex: "operation",
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          console.log("record.projectname : ",record.projectname)
          console.log("record.Role : ",record.Role)
          return editable? (
            <span>
              <EditableContext.Consumer>
                
              </EditableContext.Consumer>
              
            </span>
          ) : (
            <span>
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.handleDelete(record.projectname)}
              >
                <a> Delete</a>
              </Popconfirm>
            </span>
          );
        },
        title: "Action"
      }
    ];
  }

  isEditing = record => record.projectname === this.state.editingKey;

  cancel = () => {
    this.setState({ editingKey: "" });
  };

  save(form, projectname) {
    form.validateFields((error, row) => {
      
      this.props.data3
        .filter(obj => obj.projectname === row.projectname)
        .map(
          obj =>
            (obj.Role = row.Role)
        );

      const myData = [];
      let len = this.props.data3.length;
      for (let i = 0; i < len; i++) {
        myData.push(this.props.data3[i]);
      }


      if (error) {
        return;
      }

      const newData = myData;
      const index = newData.findIndex(item => {
        if (projectname === item.projectname) {
          return true;
        } else {
          return false;
        }
      });

      if (index > -1) {
        
        let list = this.props.data3.splice(index, 1, {
          ...row
        });
       
        data3 = newData;
        this.setState({ data3: newData, editingKey: "" });
       
      } else {
        newData.push(row);
        this.setState({ data3: newData, editingKey: "" });
      }
    });
  }

  edit(projectname) {
    this.setState({ editingKey: projectname });
  }

  handleDelete = projectname => {
    const newData = [...this.props.data3];
    const index = newData.findIndex(item => {
      console.log("itemmmmmmmmmm", item);
      if (projectname === item.projectname) {
        return true;
      } else {
        return false;
      }
    });
    if (index > -1) {
      
      this.props.data3.splice(index, 1);
      this.setState({
        data3: newData.filter(item => item.projectname !== projectname)
      });
    }
  };

    render() {
    const components = {
      body: {
        cell: EditableCell
      }
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          dataIndex: col.dataIndex,
          editing: this.isEditing(record),
          inputType: col.title === "projectname" ? "text" : "list",
          record,
          title: col.title
        })
      };
    });

    return (
      <EditableContext.Provider value={this.props.form}>
        <Table
          components={components}
          bordered
          dataSource={this.props.data3}
          columns={columns}
          rowClassName="editable-row"
          pagination={{
            onChange: this.cancel
          }}
        />
      </EditableContext.Provider>
    );
  }
}
EditableTable.propTypes = {
  element: PropTypes.object.isRequired,
  fetchProject: PropTypes.func.isRequired,
  fetchRole: PropTypes.func.isRequired,
  project: PropTypes.array.isRequired,
  role: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  data3: state.edituser.data3,
  element: state.edituser.element,
  project: state.project.items,
  role: state.role.datas
});

const EditableFormTable = Form.create()(EditableTable);
export default connect(
  mapStateToProps,
  { fetchProject,fetchRole }
)(EditableFormTable);
