import React, { Component } from "react";
import { Button } from "antd";
import EditTool from "./editTool";
import EditRepository from "./editRepository";
import { editTool } from "../redux/actions/toolAction";
import { editApplication } from "../redux/actions/applicationAction";
import { editRepository } from "../redux/actions/repositoryAction";
import { editProject } from "../redux/actions/projectAction";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Form } from "antd";
import { Stats } from "fs";
class ButtonComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { isReadOnly: true };

  }
  componentDidMount() {
    console.log("anna nalla pudhi kudu", this.props.element);
    const status = this.props.element.status;
    console.log("status varuthu ::: ", status);
  }
  handleEdit = e => {
    e.preventDefault();
    this.setState({ isReadOnly: false });
    this.props.handleEdit();
  };

  handleCancel = e => {
    e.preventDefault();
    this.setState({ isReadOnly: true });
    this.props.handleCancel();
  };

  handleDeactivate = e => {
    e.preventDefault();
    this.setState({ isReadOnly: true });
    this.props.handleCancel();
  };

  render() {
    return (
      <div>
        {this.props.element.status === "New" ||
        this.props.element.status === "new" ? (
          <div className="float-right">
            {this.state.isReadOnly ? (
              <div className="float-right">
                {/* <div>status : {this.props.element.status}</div> */}
                <Button
                  type="primary"
                  htmlType="submit"
                  onClick={this.handleEdit}
                >
                  Edit
                </Button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Button
                  onClick={this.props.handleDelete}
                  className="btn btn-danger"
                  style={{ float: "right" }}
                >
                  Delete
                </Button>
              </div>
            ) : (
              <div className="float-left">
                <Button
                  type="primary"
                  htmlType="submit"
                  onClick={this.props.handleUpdate}
                >
                  Update
                </Button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Button
                  style={{ float: "right" }}
                  onClick={this.handleCancel}
                  className="btn btn-warning"
                >
                  Cancel
                </Button>
              </div>
            )}
          </div>
        ) : null}

        {this.props.element.status === "active" ||
        this.props.element.status === "Active" ? (
          <div className="float-right">
            {this.state.isReadOnly ? (
              <div className="float-right">
                <Button
                  type="primary"
                  htmlType="submit"
                  onClick={this.handleEdit}
                >
                  Edit
                </Button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Button
                  onClick={this.props.handleDeactivate}
                  className="btn btn-danger"
                  style={{ float: "right" }}
                >
                  Deactivate
                </Button>
              </div>
            ) : (
              <div className="float-left">
                <Button
                  type="primary"
                  htmlType="submit"
                  onClick={this.props.handleUpdate}
                >
                  Update
                </Button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Button
                  style={{ float: "right" }}
                  onClick={this.handleCancel}
                  className="btn btn-warning"
                >
                  Cancel
                </Button>
              </div>
            )}
          </div>
        ) : null}

        {this.props.element.status === "deactive" ||
        this.props.element.status === "Deactive" ? (
          <div className="float-right">
            {this.state.isReadOnly ? (
              <div className="float-right">
                <Button
                  type="primary"
                  htmlType="submit"
                  onClick={this.props.handleActivate}
                >
                  Activate
                </Button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Button
                  type="primary"
                  htmlType="submit"
                  onClick={this.handleEdit}
                >
                  Edit
                </Button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Button
                  onClick={this.props.handleClone}
                  type="primary"
                  style={{ float: "right" }}
                >
                  Clone
                </Button>
              </div>
            ) : (
              <div className="float-left">
                <Button
                  type="primary"
                  htmlType="submit"
                  onClick={this.props.handleUpdate}
                >
                  Update
                </Button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Button
                  style={{ float: "right" }}
                  onClick={this.handleCancel}
                  className="btn btn-warning"
                >
                  Cancel
                </Button>
              </div>
            )}
          </div>
        ) : null}

        {this.props.element.status === "invisible" ||
        this.props.element.status === "Invisible" ? (
          <div className="float-right">
            <div className="float-right">
              <Button
                type="primary"
                htmlType="submit"
                onClick={this.props.handleActivate}
              >
                Activate
              </Button>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <Button
                onClick={this.props.handleDelete}
                className="btn btn-danger"
                style={{ float: "right" }}
              >
                Delete
              </Button>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

ButtonComponent.propTypes = {
  edit: PropTypes.func.isRequired,
  editRepository: PropTypes.func.isRequired,
  editTool: PropTypes.func.isRequired,
  editproject: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
 // element: state.edittool.element,
  element: state.editOr.element,
  element1: state.edit.element,
  element2: state.editproject.element,
 
});
const ButtonComponentForm = Form.create({ name: "register" })(ButtonComponent);
export default connect(
  mapStateToProps,
  { editTool, editApplication, editProject, editRepository }
)(ButtonComponentForm);
