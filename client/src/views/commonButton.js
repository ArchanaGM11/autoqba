import React, { Component } from "react";
import { Button } from "antd";
import EditTool from "./editTool";
import EditApplication from "./editApplication";
import { editTool } from "../redux/actions/toolAction";
import { editApplication } from "../redux/actions/applicationAction";
import { editProject } from "../redux/actions/projectAction";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { editUser } from "../redux/actions/userAction" ;
import {EditUser} from "./editUser";
import {
  Form
} from "antd";
import { Stats } from 'fs';
class CommonButtons extends Component {
  constructor(props) {
    super(props);
    this.state = { isReadOnly: true };
    console.log("hiiiiiiiuuu")
  }
  componentDidMount() {
    console.log("elementtttttttt", this.props.element)
    const status = this.props.status;
    console.log("statusssssssss", status)
  }
  handleEdit = e => {
    e.preventDefault();
    this.setState({ isReadOnly: false });
    this.props.handleEdit();
  };

  handleCancel = e => {
    e.preventDefault();
    this.setState({ isReadOnly: true });
    this.props.handleCancel();
  };

  handleDeactivate = e => {
    e.preventDefault();
    this.setState({ isReadOnly: true });
    this.props.handleCancel();
  };

  render() {
    return (
      <div>
        {this.props.status === 'new' ? (
          <div className="float-right">

            {this.state.isReadOnly ? (
              <div className="float-right">
                {/* <div>status : {this.props.status}</div> */}
                <Button type="primary" htmlType="submit" onClick={this.handleEdit}>
                  Edit
           </Button>&nbsp;&nbsp;&nbsp;&nbsp;
           <Button
                  onClick={this.props.handleDelete}
                  className="btn btn-danger"
                  style={{ float: "right" }}
                >
                  Delete
           </Button>
              </div>
            ) : (
                <div className="float-left">
                  <Button
                    type="primary"
                    htmlType="submit"
                    onClick={this.props.handleUpdate}
                  >
                    Update
           </Button>&nbsp;&nbsp;&nbsp;&nbsp;
           <Button
                    style={{ float: "right" }}
                    onClick={this.handleCancel}
                    className="btn btn-warning"
                  >
                    Cancel
           </Button>
                </div>
              )}
          </div>
        ) : (null)}

        {(this.props.status === 'active' || this.props.status === 'Active' ) ? (
          <div className="float-right">

            {this.state.isReadOnly ? (
              <div className="float-right">

                <Button type="primary" htmlType="submit" onClick={this.handleEdit}>
                  Edit
           </Button>&nbsp;&nbsp;&nbsp;&nbsp;
           <Button
                  onClick={this.props.handleDeactivate}
                  className="btn btn-danger"
                  style={{ float: "right" }}
                >
                  Deactivate
           </Button>
              </div>
            ) : (
                <div className="float-left">
                  <Button
                    type="primary"
                    htmlType="submit"
                    onClick={this.props.handleUpdate}
                  >
                    Update
           </Button>&nbsp;&nbsp;&nbsp;&nbsp;
           <Button
                    style={{ float: "right" }}
                    onClick={this.handleCancel}
                    className="btn btn-warning"
                  >
                    Cancel
           </Button>
                </div>
              )}
          </div>
        ) : (null)}

        {this.props.status === 'deactive' ? (
          <div className="float-right">

            {this.state.isReadOnly ? (
              <div className="float-right">
                <Button type="primary" htmlType="submit" onClick={this.props.handleActivate}>
                  Activate
           </Button>&nbsp;&nbsp;&nbsp;&nbsp;
     
               <Button type="primary" htmlType="submit" onClick={this.handleEdit}>
                  Edit
           </Button>&nbsp;&nbsp;&nbsp;&nbsp;
           <Button
                  onClick={this.props.handleClone}
                  type="primary"
                  style={{ float: "right" }}
                >
                  Clone
           </Button>
              </div>
            ) : (
                <div className="float-left">
                  <Button
                    type="primary"
                    htmlType="submit"
                    onClick={this.props.handleUpdate}
                  >
                    Update
           </Button>&nbsp;&nbsp;&nbsp;&nbsp;
           <Button
                    style={{ float: "right" }}
                    onClick={this.handleCancel}
                    className="btn btn-warning"
                  >
                    Cancel
           </Button>
                </div>
              )}
          </div>
        ) : (null)}

        {this.props.status === 'invisible' ? (
          <div className="float-right">

            <div className="float-right">

              <Button type="primary" htmlType="submit" onClick={this.props.handleActivate}>
                Activate
           </Button>&nbsp;&nbsp;&nbsp;&nbsp;
           <Button
                onClick={this.props.handleDelete}
                className="btn btn-danger"
                style={{ float: "right" }}
              >
                Delete
           </Button>
            </div>

          </div>
        ) : (null)}


      </div>
    );
  }
}

export default CommonButtons
