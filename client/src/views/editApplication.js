import React, { Component } from "react";
import {
  editApplication,
  updateApplication,
  deleteApplication,
  updateApplicationstatus
} from "../redux/actions/applicationAction";

import PropTypes from "prop-types";
import {
  Input,
  Button,
  Icon,
  Form,
  Select,
  Card,
  Breadcrumb,
  Row,
  bind
} from "antd";
import { connect } from "react-redux";
import CommonButtons from "./commonButton";

const { TextArea } = Input;
const { Option } = Select;
//const appId = this.props.element._id;
export class EditApplication extends Component {
  constructor(props) {
    super(props);
    this.state = { disabled: true, modifyapplication: "View Application" };
  }

  handleEdit = e => {
    this.setState({ disabled: !this.state.disabled });
    this.setState({ modifyapplication: "Edit Application" });
  };

  handleCancel = e => {
    this.setState({ disabled: true });
    this.setState({ modifyapplication: "View Application" });
  };

  // handleEdit(e) {
  //   e.preventDefault();
  //   this.setState({ disabled: !this.state.disabled });
  //   this.setState({mode: 'edit'});
  // }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      var myKey = "updated_by";
      values[myKey] = this.props.uid;
      //const updatedby = values.updated_by.concat(this.props.lname)
      // values.updated_by= updatedby;
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.updateApplication(this.props.element._id, values);
        //alert("Successfully updated..");
        this.props.history.push("/viewapplication/");
      } else {
        alert("Enter correct details..");
      }
    });
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  handleDelete = e => {
    e.preventDefault();
    console.log(1, "Inside method --> handleDelete");
    this.props.deleteApplication(this.props.element._id);
    alert("Application Deleted..");
    this.props.history.push("/viewapplication/");
  };
  
  handleDeactivate = e => {
    e.preventDefault();
    alert("Deactivated");
    this.props.updateApplicationstatus(this.props.element._id);
  };

  handleActivate= e => {
    e.preventDefault();
    alert("Activated");
    this.props.updateApplicationstatus(this.props.element._id);
  };


  handleClone= e => {
    e.preventDefault();
    alert("Cloned");
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Application</Breadcrumb.Item>
                <Breadcrumb.Item>
                  {this.state.modifyapplication}
                </Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>{this.state.modifyapplication}</h5>
            </div>
            <CommonButtons
              handleEdit={this.handleEdit}
              handleDelete={this.handleDelete}
              handleUpdate={this.handleSubmit}
              handleCancel={this.handleCancel}
              handleDeactivate={this.handleDeactivate}
              handleActivate={this.handleActivate}
              handleClone={this.handleClone}
              status={this.props.element.status}
            ></CommonButtons>
            <hr className="hrline"></hr>
          </Row>

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Name">
                    {getFieldDecorator("application_name", {
                      initialValue: this.props.element.application_name,

                      rules: [
                        {
                          message: "Please enter name",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "name should be accept maximum 100 characters"
                        },
                        {
                          message: "name should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Version">
                    {getFieldDecorator("application_version", {
                      initialValue: this.props.element.application_version,
                      rules: [
                        {
                          message: "Please enter version",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "version should be accept maximum 100 characters"
                        },
                        {
                          message: "version should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="UI/API Technology">
                    {getFieldDecorator("api_technology", {
                      initialValue: this.props.element.api_technology,
                      rules: [
                        {
                          message: "Please enter US/APS technology",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "US/APS technology should be accept maximum 100 characters"
                        },
                        {
                          message:
                            "US/APS technology should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Tech Stack">
                    {getFieldDecorator("techstack", {
                      initialValue: this.props.element.techstack,
                      rules: [
                        {
                          message: "Please enter techstock",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "techstack should be accept maximum 100 characters"
                        },
                        {
                          message: "techstack should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <TextArea
                        autosize={{ minRows: 3, maxRows: 3 }}
                        disabled={this.state.disabled ? "disabled" : ""}
                      />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Environment">
                    {getFieldDecorator("environment", {
                      initialValue: this.props.element.environment,
                      rules: [
                        {
                          message: "Please enter environment",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "environment should be accept maximum 100 characters"
                        },
                        {
                          message: "environment should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Developed By">
                    {getFieldDecorator("developed_by", {
                      initialValue: this.props.element.developed_by,
                      rules: [
                        {
                          message: "Please enter developed by",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "developed by should be accept maximum 100 characters"
                        },
                        {
                          message:
                            "developed by should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Implemented By">
                    {getFieldDecorator("implemented_by", {
                      initialValue: this.props.element.implemented_by,
                      rules: [
                        {
                          message: "Please enter implemented by",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "implemented by should be accept maximum 100 characters"
                        },
                        {
                          message:
                            "implemented by should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("visibility", {
                      initialValue: this.props.element.visibility,
                      //initialValue: 'public',
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

EditApplication.propTypes = {
  deleteApplication: PropTypes.func.isRequired,
  editApplication: PropTypes.func.isRequired,
  element: PropTypes.func.isRequired,
  //fname: PropTypes.object.isRequired,
  //lname: PropTypes.object.isRequired,
  uid: PropTypes.object.isRequired,
  uname: PropTypes.object.isRequired,
  updateApplication: PropTypes.func.isRequired,
  updateApplicationstatus:PropTypes.func.isRequired
};

const mapStateToProp = state => ({
  element: state.edit.element,
  uid: state.loginData.User_id,
  uname: state.loginData.userName
  //fname: state.loginData.FirstName,
  //lname: state.loginData.LastName,
  //element: state.application.element
});

const EditApplicationForm = Form.create({ name: "index" })(EditApplication);

export default connect(
  mapStateToProp,
  { editApplication, updateApplication, deleteApplication,updateApplicationstatus }
)(EditApplicationForm);
