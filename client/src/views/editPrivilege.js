import React, { Component } from "react";
import "antd/dist/antd.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updatePrivilege, deletePrivilege } from "../redux/actions/privilegeAction";
import { Form, Input, Button, Switch, Select, Card, Breadcrumb, Row } from "antd";
const { TextArea } = Input;
const { Option } = Select;
import ComponentButtons from "./componentButtons";

export class EditPrivilege extends Component {

  constructor(props) {
    super(props);
    this.state = { disabled: true };
  }

  handleEdit = e => {
    this.setState({ disabled: !this.state.disabled });
  };

  handleCancel = e => {
    this.setState({ disabled: true });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.updatePrivilege(this.props.element._id, values);
        alert("privilege Updated..");
        this.props.history.push("/addprivilege/");
      } else {
        alert("Enter correct details..");
      }
    });
  };
  handleDelete = e => {
    e.preventDefault();
    console.log(1, "Inside method --> handleDelete");
    this.props.deletePrivilege(this.props.element._id);
    alert("privilege Deleted..");
    this.props.history.push("/addprivilege/");

  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Privilege{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    View Privilege{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Modify Privilege</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>Modify Privilege</h5>
            </div>
            <ComponentButtons
              handleEdit={this.handleEdit}
              handleDelete={this.handleDelete}
              handleUpdate={this.handleSubmit}
              handleCancel={this.handleCancel}
            ></ComponentButtons>

            <hr className="hrline"></hr>
          </Row>
          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Privilege Name">
                    {getFieldDecorator("PrivilegeName", {
                      initialValue: this.props.element.PrivilegeName,
                      rules: [
                        {
                          message: "Please enter PrivilegeName",
                          required: true
                        },
                        {
                          max: 20,
                          message: "name should be accept maximum 20 characters"
                        },
                        {
                          message: "name should be minimum 3 characters",
                          min: 3
                        }
                      ]
                    })(<Input disabled={this.state.disabled ? "disabled" : ""} />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="PrivilegeDescription">
                    {getFieldDecorator("PrivilegeDescription", {
                      rules: [
                        {
                          message: "Please enter PrivilegeDescription",
                          required: true
                        },
                        {
                          max: 1000,
                          message:
                            "PrivilegeDescription should be accept maximum 1000 characters"
                        },
                        {
                          message: "PrivilegeDescription should be minimum 4 characters",
                          min: 4
                        }
                      ]
                    })(<TextArea
                      autosize={{ minRows: 2, maxRows: 3 }}
                      disabled={this.state.disabled ? "disabled" : ""}
                    />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="PrivilegeType">
                    {getFieldDecorator("PrivilegeType", {
                      initialValue: "Add",
                      rules: [
                        {
                          message: "Please input your Privilege!",
                          required: true
                        }
                      ]
                    })(
                      <Select style={{ width: 200 }} disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Add">Add</Option>
                        <Option value="Edit">Edit</Option>
                        <Option value="Delete">Delete</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Status">
                    {getFieldDecorator("Status", {
                      initialValue: "Active",
                      rules: [
                        {
                          message: "Please input your Status!",
                          required: true
                        }
                      ]
                    })(
                      <Select style={{ width: 200 }} disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Active">Active</Option>
                        <Option value="InActive">InActive</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Created by">
                    {getFieldDecorator("Created_by", {
                      initialValue: "Admin",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select style={{ width: 200 }} disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Admin">Admin</Option>
                        <Option value="SuperAdmin">Super Admin</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Updated by">
                    {getFieldDecorator("Updated_by", {
                      initialValue: "Admin",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select style={{ width: 200 }} disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Admin">Admin</Option>
                        <Option value="SuperAdmin">Super Admin</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}
EditPrivilege.propTypes = {
  deletePrivilege: PropTypes.func.isRequired,
  element: PropTypes.object.isRequired,
  updatePrivilege: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  element: state.editprivilege.element
});
const EditPrivilegeForm = Form.create({ name: "register" })(EditPrivilege);
export default connect(
  mapStateToProps,
  { updatePrivilege, deletePrivilege }
)(EditPrivilegeForm);
