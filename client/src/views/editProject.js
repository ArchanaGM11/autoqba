import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, Select, Card, Breadcrumb, Row } from "antd";
import {
  updateProject,
  deleteProject,
  userupdateProject,
  inmemoryAddUser,
  updateProjectstatus
} from "../redux/actions/projectAction";
import { connect } from "react-redux";
import { fetchUser } from "../redux/actions/userAction";
import PropTypes from "prop-types";
import DynamicFields from "./dynamicFields";
import EditableFormTable from "./projectUserTable";
import CommonButtons from "./commonButton";
import { fetchRoleWithParam } from "../redux/actions/roleAction";


const { TextArea } = Input;
const { Option } = Select;

export class EditProject extends React.Component {
  componentWillMount() {
    this.props.fetchUser();
    this.props.fetchRoleWithParam('Project');
  }

  constructor(props) {
    super(props);
    this.state = { disabled: true, modifyproject: "View Project" };
  }

  handleEdit = e => {
    this.setState({ disabled: false });
    this.setState({ modifyproject: "Edit Project" });
  };

  handleCancel = e => {
    this.setState({ disabled: true });
    this.setState({ modifyproject: "View Project" });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      values[`updated_by`] = this.props.uid;
      if (!err) {
        const users = [];
        console.log("users : ", users);
        this.props.data2.forEach(user => {
          let u = user;
          u.username = user.username.User_id;
          u.userrole = user.userrole.Role_id;
          users.push(u);
        });
        values.user = users;
        console.log("Received values of form: ", values);
        this.props.updateProject(this.props.element._id, values);
        //this.props.history.push("/viewproject/");
        //this.props.router.push("/viewproject/");
        //this.transitionTo('/viewproject/');
        this.props.history.replace("/viewproject/");
      } else {
        alert("Enter correct details..");
      }
    });
  };

  // handleUpdate = e => {
  //   e.preventDefault();
  //   this.props.form.validateFieldsAndScroll((err, values) => {
  //     if (!err) {
  //       var nuser = values.user;
  //       const user = values.user.concat(this.props.data2);
  //       values.user = user;
  //       console.log("Updating user: ", values, this.props.element._id);
  //       var muser = values.user;
  //       var k = 0;
  //       for (let i = 0; i < muser.length; i++) {
  //         if (nuser[0].username === muser[i].username) {
  //           k++;
  //         }
  //       }
  //       if (k === 1) {
  //         this.props.inmemoryAddUser(this.props.element._id, values);
  //         this.props.form.setFields({
  //           "user[0][username]": undefined,
  //           "user[0][userrole]": undefined
  //         });
  //       } else {
  //         alert("user already exist");
  //       }
  //     }
  //   });
  // };

  handleuserUpdate = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let usrArr = [
          {
            username: this.props.user[values.username],
            userrole: this.props.role[values.userrole]
          }
        ];

        let oldArr = this.props.data2;
        var k = 0;
        for (let i = 0; i < oldArr.length; i++) {
          if (usrArr[0].username === oldArr[i].username) {
            k++;
          }
        }
        if (k === 0) {
          usrArr = usrArr.concat(oldArr);
          console.log("Ippo panna array", usrArr);
          values.user = usrArr;
          this.props.inmemoryAddUser(this.props.element._id, values);
        } else {
          alert("user already exist");
        }
        this.props.form.setFields({
          username: undefined,
          userrole: undefined
        });

        // var nuser = values.user;
        // const user = values.user.concat(this.props.data3);
        // values.user = user;
        // var muser = values.user;
        // var k = 0;
        // for (let i = 0; i < muser.length; i++) {
        //   if (nuser[0].username === muser[i].username) {
        //     k++;
        //   }
        // }
        // if (k === 1) {
        //   this.props.userupdateProject(values);
        //   this.props.form.setFields({
        //     "user[0][username]": undefined,
        //     "user[0][userrole]": undefined
        //   });
        // } else {
        //   alert("user already exist");
        // }
      }
    });
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  handleDelete = e => {
    e.preventDefault();
    console.log(1, "Inside method --> handleDelete");
    this.props.deleteProject(this.props.element._id);
    alert("Project Deleted..");
    this.props.history.push("/viewproject/");
  };

  handleDeactivate = e => {
    e.preventDefault();
    alert("Deactivated");
    this.props.updateProjectstatus(this.props.element._id);
  };

  handleActivate= e => {
    e.preventDefault();
    alert("Activated");
    this.props.updateProjectstatus(this.props.element._id);
  };


  handleClone= e => {
    e.preventDefault();
    alert("Cloned");
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { form } = this.props;
    return (
      <Form>
        <div className="container-fluid">
          <div className="container-fluid">
            <div className="Breadcrumb-top">
              <Row>
                <Breadcrumb>
                  <Breadcrumb.Item>
                    {" "}
                    <Button type="link" onClick={this.redirecthome}>
                      {" "}
                      Home{" "}
                    </Button>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Project</Breadcrumb.Item>
                  <Breadcrumb.Item>{this.state.modifyproject}</Breadcrumb.Item>
                </Breadcrumb>
              </Row>
            </div>

            <Row className="margintop">
              <div className="float-left">
                <h5>{this.state.modifyproject}</h5>
              </div>
              <CommonButtons
                handleEdit={this.handleEdit}
                handleDelete={this.handleDelete}
                handleUpdate={this.handleSubmit}
                handleCancel={this.handleCancel}
                handleDeactivate={this.handleDeactivate}
                handleActivate={this.handleActivate}
                handleClone={this.handleClone}
                status={this.props.element.status}
              ></CommonButtons>
              <hr className="hrline"></hr>
            </Row>

            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Name">
                    {getFieldDecorator("project_name", {
                      initialValue: this.props.element.project_name,
                      rules: [
                        {
                          message: "Please enter name",
                          required: true
                        },
                        {
                          max: 100,
                          message: "name should be accept maximum 20 characters"
                        },
                        {
                          message: "name should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Project Description">
                    {getFieldDecorator("Project_Desc", {
                      initialValue: this.props.element.Project_Desc,
                      rules: [
                        {
                          message: "Please enter Description",
                          required: true
                        },
                        {
                          max: 1000,
                          message:
                            "Project Description should be accept maximum 1000 characters"
                        },
                        {
                          message:
                            "Project Description should be minimum 4 characters",
                          min: 2
                        }
                      ]
                    })(
                      <TextArea
                        autosize={{ minRows: 2, maxRows: 3 }}
                        disabled={this.state.disabled ? "disabled" : ""}
                      />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("visibility", {
                      initialValue: this.props.element.visibility,
                      //initialValue: "public",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>

                {/* <div className="col-md-6 col-sm-12">
                  <Form.Item label="Select User & Role">
                    <DynamicFields
                      {...form}
                      name="user"
                      fields={[
                        {
                          field: () => (
                            <Select
                              layout="inline"
                              showSearch
                              placeholder="Select User"
                              disabled={this.state.disabled ? "disabled" : ""}
                            >
                              {this.props.user.map(obj => (
                                <Option key={obj.UserName} value={obj.UserName}>
                                  {obj.UserName}
                                </Option>
                              ))}
                            </Select>
                          ),
                          name: "username"
                        },
                        {
                          field: () => (
                            <Select
                              placeholder="Select Role"
                              disabled={this.state.disabled ? "disabled" : ""}
                            >
                              <Option value="guest">Guest</Option>
                              <Option value="developer">Developer</Option>
                              <Option value="project admin">
                                Project Admin
                              </Option>
                            </Select>
                          ),
                          name: "userrole"
                        }
                      ]}
                    />
                  </Form.Item>
                </div> */}

              </div>

              <Row>
                <h5>Select Users & Roles</h5>
              </Row>
              <div className="row">
                <div className="col-md-5 col-sm-12">
                  <Form.Item label="Select Username">
                    {getFieldDecorator("username", {
                      rules: [
                        {
                          message: "Please select username",
                          required: false
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select username"
                        disabled={this.state.disabled ? "disabled" : ""}
                      >
                        {this.props.user.map((obj, index) => (
                          <Option key={obj.UserName} value={index}>
                            {obj.UserName}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-5 col-sm-12">
                  <Form.Item label="Select Role">
                    {getFieldDecorator("userrole", {
                      rules: [
                        {
                          message: "Please select Role",
                          required: false
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select userrole"
                        disabled={this.state.disabled ? "disabled" : ""}
                      >
                        {this.props.role.map((obj, index) => (
                          <Option key={obj.Role} value={index}>
                            {obj.Role}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-2 col-sm-12">
                  <Form.Item>&nbsp;</Form.Item>
                  <Form.Item>
                    <Button
                      type="primary"
                      shape="circle"
                      icon="plus"
                      onClick={this.handleuserUpdate}
                    ></Button>
                  </Form.Item>
                </div>
              </div>

              {/* <Form.Item>
                <Button
                  type="primary"
                  onClick={this.handleUpdate}
                  disabled={this.state.disabled ? "disabled" : ""}
                >
                  {" "}
                  Add user
                </Button> */}

              <Form.Item>
                <Row className="margintop">
                  <div className="float-left">
                    <h5>Existing Users</h5>
                  </div>
                </Row>
                <EditableFormTable
                  editDisabled={this.state.disabled}
                ></EditableFormTable>
              </Form.Item>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  data2: state.editproject.data2,
  element: state.editproject.element,
  role: state.role.datas2,
  uid: state.loginData.User_id,
  uname: state.loginData.userName,
  user: state.user.datas
});

const WrappedEditProject = Form.create({ name: "register" })(EditProject);

export default connect(
  mapStateToProps,
  {
    deleteProject,
    fetchRoleWithParam,
    fetchUser,
    inmemoryAddUser,
    updateProject,
    updateProjectstatus,
    userupdateProject
  }
)(WrappedEditProject);

