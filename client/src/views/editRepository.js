import React, { Component } from "react";
import {
  editRepository,
  updateRepository,
  updateRepositoryStatus,
  deleteRepository
} from "../redux/actions/repositoryAction";
import { fetchApplication } from "../redux/actions/applicationAction";
import { fetchTool } from "../redux/actions/toolAction";
import '../assets/css/component-styles.css';
import ManageRepository from "../views/manageRepository";
import PropTypes from "prop-types";
import {
  Input,
  Button,
  Icon,
  Form,
  Select,
  Card,
  Breadcrumb,
  Row,
  bind
} from "antd";
import { connect } from "react-redux";
import Global from "../../src/components/layout/Global/Global";
import CommonButton from "./commonButton";
const { TextArea } = Input;
const { Option } = Select;
export class EditRepository extends Component {

  constructor(props) {
    super(props);
    /* props for application filtering */
    this.appName = null;
    this.appVersion = null;
    this.appsByName = [];
    this.appsByNameAndVersion = [];

    /* props for tools filtering */
    this.toolName = null;
    this.toolVersion = null;
    this.toolByName = [];
    this.toolByNameAndVerison = [];

    /* button disable*/
    this.state = { disabled: true };
  }
  handleEdit = e => {
    this.setState({ disabled: !this.state.disabled });
  };

  handleCancel = e => {
    this.setState({ disabled: true });
  };
componentWillMount(){
  console.log("edit repository component will mount:::")
}
  componentDidMount() {
    this.props.fetchApplication();
    this.props.fetchTool();
    console.log("edit repository component did mount:::")
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      var myKey = 'updated_by';
      values[myKey] = this.props.fname;
      const updatedby = values.updated_by.concat(this.props.lname)
       values.updated_by= updatedby;
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.updateRepository(this.props.element._id, values);
        //   this.props.history.push("/viewrepository");
      } else {
        alert("Enter correct details..");
      }
    });
  };
  handleDelete = e => {
    e.preventDefault();
    console.log(1, "Inside method --> handleDelete");
    this.props.deleteRepository(this.props.element._id);
    //  this.props.history.push("/viewrepository");
  };
 redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };
  handleDeactivate = e => {
    e.preventDefault();
    console.log("handle Deactive :::",this.props.element._id);
    this.props.updateRepositoryStatus(this.props.element._id)
    alert("Deactivated");
  };

  handleActivate= e => {
    this.props.updateRepositoryStatus(this.props.element._id)
    alert("Activated");
  };

  handleClone= e => {
    e.preventDefault();
    alert("Cloned");
  };
  /* Application Filteration*/
  appSelect = params => {
    this.props.form.setFieldsValue({
      application_version: undefined,
      environment_name: undefined
    });
    var app1 = this.props.app.filter(app => {
      return params === app._id;
    });
    this.appName = app1[0].application_name;
    this.appsByName = [];
    this.appsByName = this.props.app.filter(app => {
      return this.appName === app.application_name;
    });
  };

  versionSelect = params => {
    this.appVersion = params;
    this.appsByNameAndVersion = [];
    this.appsByNameAndVersion = this.props.app.filter(app => {
      return (
        this.appName === app.application_name &&
        this.appVersion === app.application_version
      );
    });
    console.log("vers : ", this.appsByNameAndVersion);
  };
  /* Tool Filteration*/
  toolSelect = params => {
    this.props.form.setFieldsValue({ tool_version: undefined });
    console.log("params tool pandi: ", params);
    var tool1 = this.props.too.filter(too => {
      return params === too._id;
    });
    this.toolName = tool1[0].tool_name;
    this.toolByName = [];
    this.toolByName = this.props.too.filter(too => {
      return this.toolName === too.tool_name;
    });
  };

  toolVersionSelect = params => {
    this.toolVersion = params;
    this.toolByNameAndVerison = [];
    this.toolByNameAndVerison = this.props.too.filter(too => {
      return (
        this.toolName === too.tool_name && this.toolVersion === too.tool_version
      );
    });
    console.log("vers : ", this.toolByNameAndVerison);
  };

  onSearch(sear) {
    console.log("search:", sear);
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                {" "}
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Repository{" "}
                </Button>
              </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <Button
                    type="link"
                    onClick={e => {
                      e.preventDefault();
                      Global.history.push("/viewrepository");
                    }}
                  >
                    {" "}
                    View Repository{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
            <Button
                  type="link"
                  onClick={e => {
                    e.preventDefault();
                    Global.history.push("/managerepository/"+this.props.element._id);
                  }}
                >
                  {" "}
                  Manage Repository{" "}
                </Button>
            </Breadcrumb.Item>
                <Breadcrumb.Item>Modify Repository</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>Modify Repository</h5>
            </div>
            <CommonButton
              handleEdit={this.handleEdit}
              handleDelete={this.handleDelete}
              handleUpdate={this.handleSubmit}
              handleCancel={this.handleCancel}
              handleDeactivate={this.handleDeactivate}
              handleActivate={this.handleActivate}
              handleClone={this.handleClone}
              status = {this.props.element.status}
            ></CommonButton>
            <hr className="hrline"></hr>
          </Row>
          <div className="cardheader">
            <Card
              // title="Edit Repository"
              // bordered={false}
              className="antcardborder"
            >
              {/* <div className="row"> */}
              <div className="row">
              <div className="col-md-1 col-sm-1  justify-content-center align-self-center">
                <h6 className="application-title">Application :</h6>
                </div>
                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Name">
                    {getFieldDecorator("application_name", {
                      initialValue: this.props.element.application_name,
                      rules: [
                        {
                          message: "Please input your application_name!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select Application"
                        disabled={this.state.disabled ? "disabled" : ""}
                        onChange={this.appSelect}
                        onSearch={this.onSearch}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.props.app.map(repos => (
                          <Option
                            key={repos.application_name}
                            value={repos._id}
                          >
                            {repos.application_name}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Version">
                    {getFieldDecorator("application_version", {
                      initialValue: this.props.element.application_version,
                      rules: [
                        {
                          message: "Please input your application_version!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Application Version"
                        disabled={this.appsByName.length < 1}
                        onChange={this.versionSelect}
                        onSearch={this.onSearch}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.appsByName.map(app => (
                          <Option key={app._id} value={app.application_version}>
                            {app.application_version}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Environment">
                    {getFieldDecorator("environment_name", {
                      initialValue: this.props.element.environment_name,
                      rules: [
                        {
                          message: "Please input your environment_name!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Environment"
                        onSearch={this.onSearch}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                        disabled={this.appsByNameAndVersion.length < 1}
                      >
                        {this.appsByNameAndVersion.map(repos => (
                          <Option key={repos._id} value={repos.environment}>
                            {repos.environment}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                </div>
                <div className="row">
                <div className="col-md-1 col-sm-1 justify-content-center align-self-center">
               <h6 className="application-title">Tool :</h6>
                </div>
                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Name">
                    {getFieldDecorator("tool_name", {
                      initialValue: this.props.element.tool_name,
                      rules: [
                        {
                          message: "Please input your tool_name!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Select Tool Name "
                        disabled={this.state.disabled ? "disabled" : ""}
                        onChange={this.toolSelect}
                        onSearch={this.onSearch}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.props.too.map(repos => (
                          <Option key={repos.tool_name} value={repos._id}>
                            {repos.tool_name}
                          </Option>
                        ))}
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Version">
                    {getFieldDecorator("tool_version", {
                      initialValue: this.props.element.tool_version,
                      rules: [
                        {
                          message: "Please input your tool_version!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        layout="inline"
                        showSearch
                        placeholder="Tool Version"
                        disabled={this.toolByName.length < 1}
                        onChange={this.toolVersionSelect}
                        onSearch={this.onSearch}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {this.toolByName.map(too => (
                          <Option key={too._id} value={too.tool_version}>
                            {too.tool_version}
                          </Option>
                        ))}

                        {/* ))} */}
                      </Select>
                    )}
                  </Form.Item>
                </div>
                </div>
                 <div className="row">
                 <div className="col-md-1 col-sm-1 justify-content-center align-self-center">
                <h6  className="application-title">OR :</h6>
                </div>
                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Name">
                    {getFieldDecorator("or_name", {
                      initialValue: this.props.element.or_name,
                      rules: [
                        {
                          message: "Please input your or_name!",
                          required: true
                        }
                      ]
                    })(
                      <Input
                        type="text"
                        name="or_name"
                        className="form-control"
                        placeholder="Enter the OR Name"
                        disabled={this.state.disabled ? "disabled" : ""}
                      />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-3 col-sm-3">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("visibility", {
                      initialValue: this.props.element.visibility,
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
                </div>
              {/* </div> */}
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

EditRepository.propTypes = {
  deleteRepository: PropTypes.func.isRequired,
  editRepository: PropTypes.func.isRequired,
  element: PropTypes.func.isRequired,
  fetchApplication: PropTypes.func.isRequired,
  fetchTool: PropTypes.func.isRequired,
  fname: PropTypes.object.isRequired,
  lname: PropTypes.object.isRequired,
  updateRepository: PropTypes.func.isRequired,
  updateRepositoryStatus : PropTypes.func.isRequired
};

const mapStateToProp = state => ({
  app: state.application.items,
  element: state.editOr.element,
  fname: state.loginData.FirstName,
  item: state.repository.item,
  lname: state.loginData.LastName,
  too: state.tool.items,
  updateStatus : state.repository.updateStatus
});

const EditRepositoryForm = Form.create({ name: "edit" })(EditRepository);

export default connect(
  mapStateToProp,
  {
    deleteRepository,
    editRepository,
    fetchApplication,
    fetchTool,
    updateRepository,
    updateRepositoryStatus
  }
)(EditRepositoryForm);
