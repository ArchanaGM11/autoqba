import React, { Component } from "react";
import {
  updateRepositoryStore,
  deleteRepositoryStore
} from "../redux/actions/repositoryStoreAction";
import DynamicFields from "./dynamicFields";
import ManageObject from "../views/manageObjects"
import PropTypes from "prop-types";
import {
  Input,
  Button,
  Icon,
  Form,
  Select,
  Card,
  Breadcrumb,
  Row,
  bind
} from "antd";
import { connect } from "react-redux";
import ComponentButtons from "./componentButtons";

const { TextArea } = Input;
const { Option } = Select;
//const appId = this.props.element._id;
export class EditRepositoryStore extends Component {
  constructor(props) {
    super(props);
    this.state = { disabled: true };
  }

  handleEdit = e => {
    this.setState({ disabled: !this.state.disabled });
  };

  handleCancel = e => {
    this.setState({ disabled: true });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
          console.log('this.props.typeObject',this.props.typeObject);
        const objectType = values.objectType.concat(this.props.typeObject)
       values.objectType= objectType;
        console.log("Received values of form: ", values.objectType);
         console.log("edit repo store value ",values);
      // this.props.updateRepositoryStore(this.props.objectElement._id, values);
        // this.props.history.push("/viewrepositorystore");
      } else {
        alert("Enter correct details..");
      }
    });
  };

  handleDelete = e => {
    e.preventDefault();
    console.log(1, "Inside method --> handleDelete");
    this.props.deleteRepositoryStore(this.props.objectElement._id);
    alert("Repository Store Deleted..");
    this.props.history.push("/viewrepositorystore");
  };

  render() {
//     console.log("Kams",this.props.objectClass);
//  const objectClass = this.props.objectClass;
//  console.log(objectClass);
    const { getFieldDecorator } = this.props.form;
    const { form } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Repository Store{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Modify Repository Store</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>Modify Repository Store</h5>
            </div>
            <ComponentButtons
              handleEdit={this.handleEdit}
              handleDelete={this.handleDelete}
              handleUpdate={this.handleSubmit}
              handleCancel={this.handleCancel}
            ></ComponentButtons>
            <hr className="hrline"></hr>
          </Row>

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
              
				  

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Locator">
                    {getFieldDecorator("locator", {
                      initialValue: this.props.objectElement.locator,
                      rules: [
                        {
                          message: "Please enter locator",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "Locator should be accept maximum 10 characters"
                        },
                        {
                          message:
                            "Locator should be minimum 3 characters",
                          min: 4
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Locator Value">
                    {getFieldDecorator("locator_value", {
                      initialValue: this.props.objectElement.locator_value,
                      rules: [
                        {
                          message: "Please enter Locator Value",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "Locator Value should be accept maximum 1000 characters"
                        },
                        {
                          message: "Locator Value should be minimum 3 characters",
                          min: 4
                        }
                      ]
                    })(
                        <Input disabled={this.state.disabled ? "disabled" : ""} />
                      )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Screen">
                    {getFieldDecorator("screen", {
                      initialValue: this.props.objectElement.screen,
                      rules: [
                        {
                          message: "Please enter Screen",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "Screen should be accept maximum 10 characters"
                        },
                        {
                          message: "Screen should be minimum 3 characters",
                          min: 4
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Field">
                    {getFieldDecorator("field", {
                      initialValue: this.props.objectElement.field,
                      rules: [
                        {
                          message: "Please enter Field",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "Field should be accept maximum 10 characters"
                        },
                        {
                          message:
                            "Field should be minimum 3 characters",
                          min: 4
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Status">
                    {getFieldDecorator("status", {
                      initialValue: this.props.objectElement.status,
                      //initialValue: 'public',
                      rules: [
                        {
                          message: "Please input your Status!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="public">Activate</Option>
                        <Option value="private">Deactivate</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
                
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("visibility", {
                      initialValue: this.props.objectElement.visibility,
                      //initialValue: 'public',
                      rules: [
                        {
                          message: "Please input your Visibility!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-6 col-sm-12">
                      <Form.Item label="Select Class & Name">
                        <DynamicFields
                          {...form}
                          name="objectType"
                          fields={[
                            {
                              field: () => (
                                <Input placeholder = "enter a class"
                                disabled={this.state.disabled ? "disabled" : ""} />
                              ),
                              name: "objectClass"
                            },
                            {
                              field: () => (
                                <Input  placeholder = "enter a name"
                                 disabled={this.state.disabled ? "disabled" : ""} />
                              ),
                              name: "objectName"
                            }
                          ]}
                        />
                      </Form.Item>
                    </div>
                    </div>
                   
                    <Form.Item>
                <ManageObject></ManageObject>
              </Form.Item>

             
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

EditRepositoryStore.propTypes = {
  deleteRepositoryStore: PropTypes.func.isRequired,
  // objectClass: PropTypes.array.isRequired,
  // objectElement: PropTypes.func.isRequired,
  typeObject:PropTypes.array.isRequired,
  updateRepositoryStore: PropTypes.func.isRequired
};

const mapStateToProp = state => ({
  deleteObjectElement: state.repositoryStore.deleteObjectElement,
  // objectClass: state.objectClass.datas,
  objectElement: state.editStore.objectElement,
  typeObject: state.editStore.typeObject,
  updateObjectElement: state.repositoryStore.updateObjectElement
 
});

const EditRepositoryStoreForm = Form.create({ name: "edit Store" })(EditRepositoryStore);

export default connect(
  mapStateToProp,
  {  updateRepositoryStore, deleteRepositoryStore }
)(EditRepositoryStoreForm);
