import React, { Component } from "react";
import "antd/dist/antd.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateRole, deleteRole } from "../redux/actions/roleAction";
import { fetchPrivilege } from "../redux/actions/privilegeAction";
import { Form, Input, Button, Switch, Select, Card, Breadcrumb, Row } from "antd";
const { TextArea } = Input;
const { Option } = Select;
import ComponentButtons from "./componentButtons";

function handleChange(value) {
  console.log(`selected ${value}`);
}

export class EditRole extends Component {

  componentDidMount() {
    this.props.fetchPrivilege();
  }    

  constructor(props) {
    super(props);
    this.state = { disabled: true };
  }

  handleEdit = e => {
    this.setState({ disabled: !this.state.disabled });
  };

  handleCancel = e => {
    this.setState({ disabled: true });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.updateRole(this.props.element._id, values);
        alert("Role Updated..");
        this.props.history.push("/addrole/");
      } else {
        alert("Enter correct details..");
      }
    });
  };
  handleDelete = e => {
    e.preventDefault();
    console.log(1, "Inside method --> handleDelete");
    this.props.deleteRole(this.props.element._id);
    alert("Role Deleted..");
    this.props.history.push("/addrole/");

  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Role{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    View Role{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Modify Role</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>Modify Role</h5>
            </div>
            <ComponentButtons
              handleEdit={this.handleEdit}
              handleDelete={this.handleDelete}
              handleUpdate={this.handleSubmit}
              handleCancel={this.handleCancel}
            ></ComponentButtons>

            <hr className="hrline"></hr>
          </Row>
          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Role Name">
                    {getFieldDecorator("RoleName", {
                      initialValue: this.props.element.RoleName,
                      rules: [
                        {
                          message: "Please enter RoleName",
                          required: true
                        },
                        {
                          max: 20,
                          message: "name should be accept maximum 20 characters"
                        },
                        {
                          message: "name should be minimum 3 characters",
                          min: 3
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="RoleDescription">
                    {getFieldDecorator("RoleDescription", {
                      rules: [
                        {
                          message: "Please enter RoleDescription",
                          required: true
                        },
                        {
                          max: 1000,
                          message:
                            "RoleDescription should be accept maximum 1000 characters"
                        },
                        {
                          message: "RoleDescription should be minimum 4 characters",
                          min: 4
                        }
                      ]
                    })(
                      <TextArea
                        autosize={{ minRows: 3, maxRows: 3 }}
                        disabled={this.state.disabled ? "disabled" : ""}
                      />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="RoleType">
                    {getFieldDecorator("RoleType", {
                      initialValue: "Admin",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Admin">Admin</Option>
                        <Option value="SuperAdmin">Super Admin</Option>
                        <Option value="User">User</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Privileges">
                    {getFieldDecorator("PrivilegeType", {
                      initialValue: "Adduser",
                      rules: [
                        {
                          message: "Please input your Privilege!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        mode="multiple"
                        disabled={this.state.disabled ? "disabled" : ""}
                        placeholder="Please select privileges"
                        layout="inline"
                        showSearch
                        placeholder="Select Privilege"
                        onChange={handleChange}
                      >
                        {this.props.privilege.map(obj => (
                          <Option
                            key={obj.PrivilegeName}
                            value={obj.PrivilegeName}
                          >
                            {obj.PrivilegeName}
                          </Option>
                        ))}
                        
                      </Select>
                    )}
                  </Form.Item>
                
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Status">
                    {getFieldDecorator("Status", {
                      initialValue: "Active",
                      rules: [
                        {
                          message: "Please input your Status!",
                          required: true
                        }
                      ]
                    })(
                      <Select style={{ width: 200 }} disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Active">Active</Option>
                        <Option value="InActive">InActive</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Created by">
                    {getFieldDecorator("Created_by", {
                      initialValue: "Admin",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select style={{ width: 200 }} disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Admin">Admin</Option>
                        <Option value="SuperAdmin">Super Admin</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Updated by">
                    {getFieldDecorator("Updated_by", {
                      initialValue: "Admin",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select style={{ width: 200 }} disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Admin">Admin</Option>
                        <Option value="SuperAdmin">Super Admin</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}
EditRole.propTypes = {
  deleteRole: PropTypes.func.isRequired,
  element: PropTypes.object.isRequired,
  fetchPrivilege: PropTypes.func.isRequired,
  privilege: PropTypes.array.isRequired,
  updateRole: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  element: state.editrole.element,
  privilege: state.privilege.datas
});
const EditRoleForm = Form.create({ name: "register" })(EditRole);
export default connect(
  mapStateToProps,
  { updateRole, deleteRole,fetchPrivilege }
)(EditRoleForm);
