import React, { Component } from "react";
import "antd/dist/antd.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateTool, deleteTool, updateToolstatus } from "../redux/actions/toolAction";
import {
  Form,
  Input,
  Button,
  Switch,
  Select,
  Card,
  Breadcrumb,
  Row
} from "antd";
import CommonButtons from "./commonButton";

const { Option } = Select;

export class EditTool extends Component {
  constructor(props) {
    super(props);
    this.state = { disabled: true, modifytool: "View Tool" };
  }

  handleEdit = e => {
    this.setState({ disabled: !this.state.disabled });
    this.setState({ modifytool: "Edit Tool" });
  };

  handleCancel = e => {
    this.setState({ disabled: true });
    this.setState({ modifytool: "View Tool" });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      var myKey = "updated_by";
      values[myKey] = this.props.uid;
      //const updatedby = values.updated_by.concat(this.props.lname)
      //values.updated_by= updatedby;
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.updateTool(this.props.element._id, values);
        //alert("Successfully updated..");
        this.props.history.push("/viewtool/");
      } else {
        alert("Enter correct details..");
      }
    });
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  handleDelete = e => {
    e.preventDefault();
    console.log(1, "Inside method --> handleDelete");
    this.props.deleteTool(this.props.element._id);
    alert("Tool Deleted..");
    this.props.history.push("/viewtool/");
    //this.props.history.push("/addapplication/");
  };

  handleDeactivate = e => {
    e.preventDefault();
    alert("Deactivated");
    this.props.updateToolstatus(this.props.element._id);
  };

  handleActivate= e => {
    e.preventDefault();
    alert("Activated");
    this.props.updateToolstatus(this.props.element._id);
  };


  handleClone= e => {
    e.preventDefault();
    alert("Cloned");
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Tool</Breadcrumb.Item>
                <Breadcrumb.Item>{this.state.modifytool}</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>{this.state.modifytool}</h5>
            </div>
            <CommonButtons
              handleEdit={this.handleEdit}
              handleDelete={this.handleDelete}
              handleUpdate={this.handleSubmit}
              handleCancel={this.handleCancel}
              handleDeactivate={this.handleDeactivate}
              handleActivate={this.handleActivate}
              handleClone={this.handleClone}
              status={this.props.element.status}            
            ></CommonButtons>

            <hr className="hrline"></hr>
          </Row>
          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Name">
                    {getFieldDecorator("tool_name", {
                      initialValue: this.props.element.tool_name,
                      rules: [
                        {
                          message: "Please enter name",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "name should be accept maximum 100 characters"
                        },
                        {
                          message: "name should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Version">
                    {getFieldDecorator("tool_version", {
                      initialValue: this.props.element.tool_version,
                      rules: [
                        {
                          message: "Please enter version",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "version should be accept maximum 100 characters"
                        },
                        {
                          message: "version should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Technology">
                    {getFieldDecorator("technology", {
                      initialValue: this.props.element.technology,
                      rules: [
                        {
                          message: "Please enter technology",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "technology should be accept maximum 100 characters"
                        },
                        {
                          message: "technology should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Window Objects">
                    {getFieldDecorator("window_objects", {
                      initialValue: this.props.element.window_objects,
                      rules: [
                        {
                          message: "Please enter window objects",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "window objects should be accept maximum 100 characters"
                        },
                        {
                          message:
                            "window objects should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Screen Objects">
                    {getFieldDecorator("screen_objects", {
                      initialValue: this.props.element.screen_objects,
                      rules: [
                        {
                          message: "Please enter screen objects",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "screen objects should be accept maximum 100 characters"
                        },
                        {
                          message:
                            "screen objects should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                {/* <div className="col-md-6 col-sm-12">
                  <Form.Item label="Status">
                    {getFieldDecorator("status", {
                      initialValue: this.props.element.status,
                      rules: [
                        {
                          message: "Please enter status",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "status should be accept maximum 100 characters"
                        },
                        {
                          message: "status should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div> */}

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Field Objects">
                    {getFieldDecorator("field_objects", {
                      initialValue: this.props.element.field_objects,
                      rules: [
                        {
                          message: "Please enter field objects",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "field objects should be accept maximum 100 characters"
                        },
                        {
                          message:
                            "field objects should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("visibility", {
                      initialValue: this.props.element.visibility,
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="public">Public</Option>
                        <Option value="private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}
EditTool.propTypes = {
  deleteTool: PropTypes.func.isRequired,
  element: PropTypes.object.isRequired,
  uid: PropTypes.object.isRequired,
  uname: PropTypes.object.isRequired,
  updateTool: PropTypes.func.isRequired,
  updateToolstatus:PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  element: state.edittool.element,
  uid: state.loginData.User_id,
  uname: state.loginData.userName
});
const EditToolForm = Form.create({ name: "register" })(EditTool);
export default connect(
  mapStateToProps,
  { updateTool, deleteTool,updateToolstatus }
)(EditToolForm);
