import React, { Component } from "react";
import "antd/dist/antd.css";
import { Table } from "antd";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateUser, deleteUser, updateUserstatus } from "../redux/actions/userAction";
import { fetchProject } from '../redux/actions/projectAction';
import { fetchRole } from '../redux/actions/roleAction';
import { Form, Input, Button, Switch, Select, Card, Breadcrumb, Row, Alert, Icon } from "antd";
import DynamicFields from "./dynamicFields";
const { TextArea } = Input;
const { Option } = Select;
import Highlighter from "react-highlight-words";
import ComponentButtons from "./componentButtons";
import EditableFormTable from "./userProjectAssociation";
import Global from '../components/layout/Global/Global';
import CommonButtons from "./commonButton";

export class EditUser extends Component {

  componentDidMount() {
    this.props.fetchRole();
    this.props.fetchProject();
  }

  constructor(props) {
    super(props);
    this.state = { disabled: true };
  }

  handleEdit = e => {
    this.setState({ disabled: !this.state.disabled });
  };

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={node => {
              this.searchInput = node;
            }}
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={e =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
            style={{ width: 188, marginBottom: 8, display: "block" }}
          />
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm)}
            icon="search"
            size="small"
            style={{ width: 90, marginRight: 8 }}
          >
            Search
        </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
        </Button>
        </div>
      ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={String(text)}
      //textToHighlight={text.toString()}
      />
    )
  });

  handleCancel = e => {
    this.setState({ disabled: true });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.updateUser(this.props.element._id, values);
        //alert("User Updated..");
        console.log("User Updated 111")
        // this.props.history.push("/adduser/");
      } else {
        alert("Enter correct details..");
      }
    });
  };
  handleDelete = e => {
    e.preventDefault();
    console.log(1, "Inside method --> handleDelete");
    this.props.deleteUser(this.props.element._id);
    this.props.history.push("/adduser/");

  };
  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  }
  redirectview = e => {
    e.preventDefault();
    this.props.history.push("/viewuser");
  }

  handleDeactivate = e => {
    e.preventDefault();
    alert("Deactivated");
    this.props.updateUserstatus(this.props.element._id);
  };

  handleActivate = e => {
    e.preventDefault();
    alert("Activated");
    this.props.updateUserstatus(this.props.element._id);
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { form } = this.props;
    const data = [];
    {
      this.props.items.map(object =>
        data.push({
          project_name: object.project_name,
          user: object.user.userrole
        })
      );
    }

    const columns = [
      {
        dataIndex: "project_name",
        key: "project_name",
        title: "Project Name",
        ...this.getColumnSearchProps("project_name")
      },
      {
        dataIndex: "user",
        key: "user",
        title: "Role",
        ...this.getColumnSearchProps("user")
      },
    ]
    //console.log(112,this.props.project);
    //const project = this.props.project;
    //console.log(project);
    return (
      <Form>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    Home{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                  <Button type="link" onClick={this.redirectview}>
                    {" "}
                    View User{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Modify User</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row className="margintop">
            <div className="float-left">
              <h5>Modify User</h5>
            </div>

            <CommonButtons
              handleEdit={this.handleEdit}
              handleDelete={this.handleDelete}
              handleUpdate={this.handleSubmit}
              handleCancel={this.handleCancel}
              handleDeactivate={this.handleDeactivate}
              handleActivate={this.handleActivate}

              status={this.props.element.Status}
            ></CommonButtons>

            <hr className="hrline"></hr>
          </Row>

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="First Name">
                    {getFieldDecorator("FirstName", {
                      initialValue: this.props.element.FirstName,
                      rules: [
                        {
                          message: "Please enter FirstName",
                          required: true
                        },
                        {
                          max: 20,
                          message: "FirstName should be accept maximum 20 characters"
                        },
                        {
                          message: "FirstName should be minimum 4 characters",
                          min: 4
                        }
                      ]
                    })(<Input disabled={this.state.disabled ? "disabled" : ""} />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Last Name">
                    {getFieldDecorator("LastName", {
                      initialValue: this.props.element.LastName,
                      rules: [
                        {
                          message: "Please enter LastName",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "LastName should be accept maximum 20 characters"
                        },
                        {
                          message: "LastName should be minimum 2 characters",
                          min: 4
                        }
                      ]
                    })(<Input disabled={this.state.disabled ? "disabled" : ""} />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="User Name">
                    {getFieldDecorator("UserName", {
                      initialValue: this.props.element.UserName,
                      rules: [
                        {
                          message: "Please enter UserName",
                          required: true
                        },
                        {
                          max: 15,
                          message:
                            "UserName should be accept maximum 20 characters"
                        },
                        {
                          message:
                            "UserName should be minimum 4 characters",
                          min: 8
                        }
                      ]
                    })(<Input disabled={this.state.disabled ? "disabled" : ""} />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Email">
                    {getFieldDecorator("Email", {
                      initialValue: this.props.element.Email,
                      rules: [
                        {
                          message: "Please enter Email",
                          required: true
                        },
                        {
                          max: 35,
                          message:
                            "Email should be accept maximum 20 characters"
                        },
                        {
                          message: "Email should be minimum 4 characters",
                          min: 4
                        }
                      ]
                    })(<Input disabled={this.state.disabled ? "disabled" : ""} />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Company Name">
                    {getFieldDecorator("CompanyName", {
                      initialValue: this.props.element.CompanyName,
                      rules: [
                        {
                          message: "Please enter CompanyName",
                          required: true
                        },
                        {
                          max: 20,
                          message:
                            "CompanyName should be accept maximum 20 characters"
                        },
                        {
                          message: "CompanyName should be minimum 4 characters",
                          min: 3
                        }
                      ]
                    })(<Input disabled={this.state.disabled ? "disabled" : ""} />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Select Role">
                    {getFieldDecorator("Role", {
                      initialValue: "Please select Role",
                      rules: [
                        {
                          message: "Please input your Role!",
                          required: true
                        }
                      ]
                    })(
                      <Select
                        disabled={this.state.disabled ? "disabled" : ""}
                        layout="inline"
                        showSearch
                        placeholder="Select Role"
                      >
                        {this.props.role.map(obj => (
                          <Option
                            key={obj.RoleName}
                            value={obj.RoleName}
                          >
                            {obj.RoleName}
                          </Option>
                        ))}
                      </Select>

                    )}
                  </Form.Item>
                </div>


                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Visibility">
                    {getFieldDecorator("Visibility", {
                      initialValue: "Public",
                      rules: [
                        {
                          message: "Please input your Visibility!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Public">Public</Option>
                        <Option value="Private">Private</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-6 col-sm-12">
                  <Form.Item label="Status">
                    {getFieldDecorator("Status", {
                      initialValue: "Active",
                      rules: [
                        {
                          message: "Please input your Status!",
                          required: true
                        }
                      ]
                    })(
                      <Select disabled={this.state.disabled ? "disabled" : ""}>
                        <Option value="Active">Active</Option>
                        <Option value="InActive">InActive</Option>
                      </Select>
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-6 col-sm-12">


                </div>

              </div>
              <div className="cardheader">
                <Card>

                  <Table
                    dataSource={data}
                    columns={columns}
                    pagination={{ pageSize: 28 }}
                    rowKey="_id"
                  />


                  <div style={{ color: "red" }} />
                </Card>
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}
EditUser.propTypes = {
  deleteUser: PropTypes.func.isRequired,
  element: PropTypes.object.isRequired,
  fetchProject: PropTypes.func.isRequired,
  fetchRole: PropTypes.func.isRequired,
  project: PropTypes.array.isRequired,
  role: PropTypes.array.isRequired,
  updateUser: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  element: state.edituser.element,
  items: state.project.items,
  project: state.project.items,
  role: state.role.datas,
});
const EditUserForm = Form.create({ name: "register" })(EditUser);
export default connect(
  mapStateToProps,
  { updateUser, deleteUser, fetchRole, fetchProject, updateUserstatus }
)(EditUserForm);
