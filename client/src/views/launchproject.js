import React, { Component } from "react";
import "antd/dist/antd.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { fetchPrivilege } from "../redux/actions/privilegeAction";
import { Form, Input, Button, Switch, Select, Card, Breadcrumb, Row } from "antd";
const { TextArea } = Input;
const { Option } = Select;
import ComponentButtons from "./componentButtons";
import Global from "../components/layout/Global/Global";


export class LaunchProject extends Component {

  
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div><h3> Welcome your Role is : {Global.role} </h3>
      </div>
    );
  }
}
LaunchProject.propTypes = {
 
};
const mapStateToProps = state => ({
 
});
const launchproject = Form.create({ name: "register" })(LaunchProject);
export default connect(mapStateToProps,{})(launchproject);
