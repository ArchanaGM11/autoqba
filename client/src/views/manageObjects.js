import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, Select, Card } from "antd";
import { Table, InputNumber, Popconfirm } from "antd";
import { connect } from "react-redux";
import PropTypes from "prop-types";
const { TextArea } = Input;
const { Option } = Select;
const EditableContext = React.createContext();
let typeObject = [];
export class EditableCell extends React.Component {
  constructor(props) {
    super(props);
    this.state = { disabled: true };
  }



 getInput = () => {
    if (this.props.inputType === "text" || this.props.inputType === "list") {
      return <Input  />;
    }
    return <Input />;
  };


  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              initialValue: record[dataIndex],
              rules: [
                {
                  message: `Please Input ${title}!`,
                  required: true
                }
              ]
            })(this.getInput())}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return (
      <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
    );
  }
}
class ManageObject extends React.Component {
  constructor(props) {
    super(props);
    this.state = { typeObject, editingKey: "" };
    this.columns = [
      {
        dataIndex: "objectClass",
        editable: true,
        title: "Class",
        width: "25%"
      },
      {
        dataIndex: "objectName",
        editable: true,
        title: "Name",
        width: "40%"
      },
      {
        dataIndex: "operation",
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <a
                    onClick={() => this.save(form, record.objectClass)}
                    style={{ marginRight: 8 }}
                  >
                    Save
                  </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm
                title="Sure to cancel?"
                onConfirm={() => this.cancel(record.objectClass)}
              >
                <a>Cancel</a>
              </Popconfirm>
            </span>
          ) : (
            <span>
              <a
                disabled={editingKey !== ""}
                onClick={() => this.edit(record.objectClass)}
              >
                Edit
              </a>
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.handleDelete(record.objectClass)}
              >
                <a>Delete</a>
              </Popconfirm>
            </span>
          );
        },
        title: "Action"
      }
    ];
  }

 
  isEditing = record => record.objectClass === this.state.editingKey;

  cancel = () => {
    this.setState({ editingKey: "" });
  };

  save(form, objectClass) {
    form.validateFields((error, row) => {

       const myData =[];
       let len = this.props.typeObject.length;
       for (let i = 0; i < len; i++) {
         myData.push(row);
        // myData.push(this.props.typeObject[i]);
         console.log("array myData : ", myData);
       }
       console.log('myDataaaaaaaaaaaa : ',myData);

      // this.props.typeObject = myData;
       console.log('this.props.typeObject this.props.typeObject',this.props.typeObject);
      if (error) {
        return;
      }
     /// const newData = [...this.props.typeObject];
     const newData = myData;
      console.log("newData : ", newData);
      const index = newData.findIndex(item => {
        if (objectClass === item.objectClass) {
          return true;
        } else {
          return false;
        }
      });


      if (index > -1) {
        // const item = newData[index];
        console.log("newData : ", newData);
         let list = this.props.typeObject.splice(index, 1, {
          //...newData,
          ...row
        });
        console.log("list : ", list);
         typeObject = newData;
        this.setState({ typeObject: newData, editingKey: "" });
        
        console.log("Type Object",typeObject);
      } else {
        newData.push(row);
        this.setState({ typeObject: newData, editingKey: "" });
      }
    });
  }

  edit(objectClass) {
   
     console.log('inside edit',this.props.typeObject);
    this.setState({ editingKey: objectClass });
  }
  handleDelete = objectClass => {
    const newData = [...this.props.typeObject];
    const index = newData.findIndex(item => {
      console.log("itemmmmmmmmmm",item);
          if (objectClass === item.objectClass) {
            return true;
          } else {
            return false;
          }
        });
      if (index > -1) {
        console.log("index:  : ", index);
          // const item = newData[index];
          console.log("newData delete: ", newData);
           this.props.typeObject.splice(index,1);
   
     
      console.log("Type Object",typeObject);
   
      this.setState({
        typeObject: newData.filter(item => item.objectClass !== objectClass)
      });
    };
  }

  render() {
    console.log(15454,this.props.typeObject);
    const components = {
      body: {
        cell: EditableCell
      }
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          dataIndex: col.dataIndex,
          editing: this.isEditing(record),
          // inputType: col.title === "Class" === "text" ,
          inputType: col.title === "Class" ? "text" : "list",
          record,
          title: col.title
        })
      };
    });

    return (
      <EditableContext.Provider value={this.props.form}>
        <Table
          components={components}
          bordered
          dataSource={this.props.typeObject}
          columns={columns}
          rowClassName="editable-row"
          pagination={{
            onChange: this.cancel
          }}
        />
      </EditableContext.Provider>
    );
  }
}
ManageObject.propTypes = {
  objectElement: PropTypes.object.isRequired,
  typeObject: PropTypes.array.isRequired
 };

 const mapStateToProps = state => ({
  objectElement: state.editStore.objectElement,
  typeObject:state.editStore.typeObject

 });

 const ManageObjectTable = Form.create()(ManageObject);
 export default connect(mapStateToProps)(ManageObjectTable);
