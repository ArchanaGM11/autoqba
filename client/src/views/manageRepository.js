import React, { Component } from "react";
import "antd/dist/antd.css";
import {
  Switch,
  Button,
  Form,
  Input,
  Row,
  Breadcrumb,
  Collapse,
  Card,
  Descriptions
} from "antd";
import { fetchRepository } from "../redux/actions/repositoryAction";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { editRepository } from "../redux/actions/repositoryAction";
import { BrowserRouter as Link } from "react-router-dom";
import {
  fetchApplication,
  editApplication
} from "../redux/actions/applicationAction";
import { fetchTool } from "../redux/actions/toolAction";
import { withRouter } from "react-router";
import Global from "../../src/components/layout/Global/Global";
const { Panel } = Collapse;

class ManageRepository extends Component {
  constructor(props) {
    super(props);

    // this.editedComponent = "";
    (this.matchApp = {}),
      (this.matchAppName = ""),
      (this.matchAppVersion = ""),
      (this.matchAppEnvironment = ""),
      (this.matchTool = {}),
      (this.matchToolName = ""),
      (this.matchToolVersion = "");
  }
  state = {
    disabled: true
  };
  toggle = () => {
    this.setState({
      disabled: !this.state.disabled
    });
  };
  handleClick(e) {
    // console.log("manage object : ", this.props.element._id+"::::"+this.editedComponent);
    this.props.history.push("/viewrepositorystore/" + this.props.element._id);
  }
  handleTestMethod(e) {
    console.log("manage object : ", e);
  }
  handleEditClick(e) {
    this.props.history.push("/editRepository/" + this.props.element._id);
  }
  render() {
    const { getFieldDecorator } = this.props.form;

    const Content = ({ children }) => {
      return (
        <div className="content">
          <div className="main">{children}</div>
        </div>
      );
    };
    const renderContent = (column = 2) => (
      <Descriptions size="small" column={column}>
        <Descriptions.Item label="OR Name">
          <b>{this.props.element.or_name}</b>
        </Descriptions.Item>
        <br />
        <Descriptions.Item label="Visibility">
          {this.props.element.visibility}
        </Descriptions.Item>
        <Descriptions.Item label="Created Date">
          {this.props.element.created_on}
        </Descriptions.Item>
        <Descriptions.Item label="Status">
          {this.props.element.status}
        </Descriptions.Item>
        <Descriptions.Item label="Created By">
          {this.props.element.created_by}
        </Descriptions.Item>
        <Descriptions.Item label="Last Modified Date">
          {this.props.element.updated_on}
        </Descriptions.Item>
        <Descriptions.Item label="Last Modified By">
          {this.props.element.updated_by}
        </Descriptions.Item>
      </Descriptions>
    );
    // const { match, location, history } = this.props;
    // let myLoc = JSON.stringify(location.pathname);
    // var finalstr = myLoc.replace(/"/g, "");
    // this.editedComponent = finalstr.substring(finalstr.lastIndexOf("/") + 1);
    // const columnOR = [
    //   {
    //     dataIndex: "or_name",
    //     key: "or_name",
    //     title: "Name"
    //   }
    // ];


    this.props.app.filter(a => {
      this.matchApp = a._id === this.props.element.application_name;
      if (this.matchApp) {
        (this.matchAppName = a.application_name),
          (this.matchAppVersion = a.application_version),
          (this.matchAppEnvironment = a.environment);
      }
    });

    this.props.too.filter(t => {
      this.matchTool = t._id === this.props.element.tool_name;
      if (this.matchTool) {
        (this.matchToolName = t.tool_name),
          (this.matchToolVersion = t.tool_version);
      }
    }); //tool end

    return (
      <Form>
      <div className="container-fluid">
        <div className="Breadcrumb-top">
          <Row>
            <Breadcrumb>
              <Breadcrumb.Item>
                {" "}
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Home{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                {" "}
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Repository{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                <Button
                  type="link"
                  onClick={e => {
                    e.preventDefault();
                    Global.history.push("/viewrepository");
                  }}
                >
                  {" "}
                  View Repository{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>Manage Repository</Breadcrumb.Item>
            </Breadcrumb>
          </Row>
        </div>

        <Row className="margintop">
          <div className="float-left">
            <h5>Manage Repository</h5>
          </div>
          <div style={{ float: "right" }}>
            <Button
              className="margin-left"
              type="primary"
              onClick={() => this.handleEditClick()}
              //   this.handleEditClick(record.repositoryId)}
            >
              Edit
            </Button>

            <Button
              className="margin-left"
              type="primary"
              onClick={r => this.handleTestMethod(r.repositoryId)}
            >
              Deactivate
            </Button>

            <Button
              className="margin-left"
              type="primary"
              onClick={r => this.handleClick(r.repositoryId)}
            >
              Work with Objects
            </Button>
          </div>
          <hr className="hrline"></hr>
        </Row>
        <Content>{renderContent()}</Content>
        <Collapse>
          <Panel key="1" header="Linked Entities">
            <div className="cardheader">
            <Card bordered={false} className="antcardborder">
              <div className="row">
              <div className="col-md-1 col-sm-1  justify-content-center align-self-center">
               <h6 className="application-title">Application :</h6>
                </div>
                <div className="col-md-3 col-sm-3">
                    <Form.Item label="Name">
                    {getFieldDecorator("application_name", {
                      initialValue: this.matchAppName,
                      rules: [
                        {
                          message: "Please input your application_name!",
                          required: true
                        }
                      ]
                    })(<Input 
                      disabled={this.state.disabled ? "disabled" : ""}
                    />)}
                    </Form.Item>
                    </div>
                    <div className="col-md-3 col-sm-3">
                    <Form.Item label="Version">
                    {getFieldDecorator("application_version", {
                      initialValue: this.matchAppVersion,
                      rules: [
                        {
                          message: "Please input your application_version!",
                          required: true
                        }
                      ]
                    })(
                        <Input
                        disabled={this.state.disabled ? "disabled" : ""}
                        />)}
                    </Form.Item>
                    </div>
                    <div className="col-md-3 col-sm-3">
                    <Form.Item label="Environmemt">
                    {getFieldDecorator("environment_name", {
                      initialValue: this.matchAppEnvironment,
                      rules: [
                        {
                          message: "Please input your environment_name!",
                          required: true
                        }
                      ]
                    })(
                        <Input
                        disabled={this.state.disabled ? "disabled" : ""}
                        />)}
                    </Form.Item>
                    </div>
                </div>
                <div className="row">
                 <div className="col-md-1 col-sm-1 justify-content-center align-self-center">
                <h6 className="application-title">Tool :</h6>
                </div>
                <div className="col-md-3 col-sm-3">
                    <Form.Item label="Name">
                    {getFieldDecorator("tool_name", {
                      initialValue: this.matchToolName,
                      rules: [
                        {
                          message: "Please input your tool_name!",
                          required: true
                        }
                      ]
                    })(
                        <Input
                        disabled={this.state.disabled ? "disabled" : ""}
                        />)}
                    </Form.Item>
                    </div>
                    <div className="col-md-3 col-sm-3">
                    <Form.Item label="Version">
                    {getFieldDecorator("tool_version", {
                      initialValue: this.matchToolVersion,
                      rules: [
                        {
                          message: "Please input your tool_version!",
                          required: true
                        }
                      ]
                    })(
                        <Input
                        disabled={this.state.disabled ? "disabled" : ""}
                        />)}
                    </Form.Item>
                    </div>
                    </div>
            </Card>
            </div>
          </Panel>
        </Collapse>
        <br />
      </div>
      </Form>
    );
  }
}

ManageRepository.propTypes = {
  editApplication: PropTypes.func.isRequired,
  editRepository: PropTypes.func.isRequired,
  fetchApplication: PropTypes.func.isRequired,
  fetchRepository: PropTypes.func.isRequired,
  fetchTool: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired
};

const mapStateToProp = state => ({
  app: state.application.items,
  appElement: state.edit.element,
  element: state.editOr.element,
  items: state.repository.items,
  manageElement: state.editOr.manageElement,
  too: state.tool.items
});

const ManageRepositoryForm = Form.create({ name: "index" })(ManageRepository);

export default connect(
  mapStateToProp,
  {
    editApplication,
    editRepository,
    fetchApplication,
    fetchRepository,
    fetchTool
  }
)(ManageRepositoryForm);

const ShowTheLocationWithRouter = withRouter(ManageRepository);
// managerepository.js
