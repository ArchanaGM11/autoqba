import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, Select, Card } from "antd";
import { Table, InputNumber, Popconfirm } from "antd";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { fetchUser } from "../redux/actions/userAction";
const EditableContext = React.createContext();
let data2 = [];

export class EditableCell extends React.Component {
  constructor(props) {
    super(props);
    this.state = { disabled: true };
  }

  getInput = () => {
    if (this.props.inputType === "text") {
      return <Input disabled={this.state.disabled ? "disabled" : ""} />;
    } else if (this.props.inputType === "list") {
      return (
        <Input disabled={this.state.disabled ? "disabled" : ""} />
        // <Select style={{ width: "100%" }} placeholder="Select Role">
        //   <Option value="guest">Guest</Option>
        //   <Option value="developer">Developer</Option>
        //   <Option value="project admin">Project Admin</Option>
        // </Select>
      );
    }
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    if (editing && title === "User name" && inputType === "text") {
      console.log(
        "dataIndex : ",
        record,
        record.username.UserName,
        dataIndex,
        record.userrole.Role
      );
      return (
        <td {...restProps}>
          <Form.Item style={{ margin: 0 }}>
            <Input
              disabled={this.state.disabled ? "disabled" : ""}
              value={record.username.UserName}
            />
          </Form.Item>
        </td>
      );
    }
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              initialValue: record[dataIndex],
              rules: [
                {
                  message: `Please Input ${title}!`,
                  required: true
                }
              ]
            })(this.getInput())}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return (
      <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
    );
  }
}
class EditableTable extends React.Component {
  componentDidMount() {
    //this.props.fetchUser();
  }

  constructor(props) {
    super(props);
    this.state = { update: false, data2, editingKey: "" };
    this.columns = [
      {
        dataIndex: "username.UserName",
        editable: true,
        title: "User name",
        width: "25%"
      },
      {
        dataIndex: "userrole.Role",
        editable: true,
        title: "Role",
        width: "40%"
      },
      {
        dataIndex: "operation",
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <a
                    onClick={() => this.save(form, record.username)}
                    style={{ marginRight: 8 }}
                  >
                    Save
                  </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm
                title="Sure to cancel?"
                onConfirm={() => this.cancel(record.username)}
              >
                <a>Cancel</a>
              </Popconfirm>
            </span>
          ) : (
            <span>
              <a
                disabled={editingKey !== "" || this.props.editDisabled}
                onClick={() => this.edit(record.username)}
                style={{display:"none"}}
              >
                Edit
              </a>
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.handleDelete(record.username)}
              >
                <a disabled={this.props.editDisabled} style={{color:"red"}}> Delete</a>
              </Popconfirm>
            </span>
          );
        },
        title: "Action"
      }
    ];
  }

  isEditing = record => record.username === this.state.editingKey;

  loadData() {
    console.log("Inside loadData.......");
    if (!this.props.data2 || !this.props.user || !this.props.role) {
      return;
    }
    this.props.data2.forEach(data => {
      this.props.user.forEach(user => {
        if (user.User_id === data.username) {
          data.username = user;
          console.log("User name", data.username);
        }
      });

      this.props.role.forEach(role => {
        console.log("Role..................", role);
        if (role.Role_id === data.userrole) {
          data.userrole = role;
          console.log("Data Role...........", data.userrole);
          console.log("Role assigned", data.userrole.Role);
        }
      });
    });
    return this.props.data2;
  }

  cancel = () => {
    this.setState({ editingKey: "" });
  };

  save(form, username) {
    //  console.log("username : ", username);
    form.validateFields((error, row) => {
      //   console.log("row : ", row);
      if (error) {
        //     console.log("ERR : ", error);
        return;
      }

      const newData = [];
      this.props.data2.forEach(tt => {
        if (tt.username === username.UserName) {
          tt[`userrole`] = "dfhgrfdhfdh";
        }
        //   console.log("tt : ", tt, row.userrole);
        newData.push(tt);
      });

      //  console.log("newData : ", newData);
      const index = newData.findIndex(item => {
        if (username.UserName === item.username.UserName) {
          return true;
        } else {
          return false;
        }
      });

      //  console.log("index : ", index);
      if (index > -1) {
        data2 = newData;
      } else {
        newData.push(row);
      }
      this.setState({ data2: newData, editingKey: "" });
    });
  }

  edit(username) {
    this.setState({ editingKey: username });
  }

  handleDelete = username => {
    const newData = [...this.props.data2];
    const index = newData.findIndex(item => {
      // console.log("itemmmmmmmmmm", item);
      if (username === item.username) {
        return true;
      } else {
        return false;
      }
    });
    if (index > -1) {
      // const item = newData[index];
      // console.log("Index", index);
      // console.log("newData : ", newData);
      //this.props.data2.pop()
      this.props.data2.splice(index, 1);

      // data2 = newData;
      //  console.log("Data22222", data2);

      this.setState({
        data2: newData.filter(item => item.username !== username)
      });
    }
  };

  // handleAdd = () => {
  //   console.log(12345677, this.props.userlist);
  //   var user = {
  //     username: "",
  //     userrole: "",
  //   };

  //   this.props.data2.push(user);
  //   this.setState(this.props.data2);
  //   console.log(12345677, "outside Handle add");
  // };

  render() {
    const components = {
      body: {
        cell: EditableCell
      }
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          dataIndex: col.dataIndex,
          editing: this.isEditing(record),
          inputType: col.title === "User name" ? "text" : "list",
          record,
          title: col.title
        })
      };
    });

    return (
      <EditableContext.Provider value={this.props.form}>
        <Table
          components={components}
          bordered
          dataSource={this.loadData()}
          columns={columns}
          rowClassName="editable-row"
          pagination={{
            onChange: this.cancel
          }}
        />
      </EditableContext.Provider>
    );
  }
}
EditableTable.propTypes = {
  element: PropTypes.object.isRequired,
  fetchUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  data2: state.editproject.data2,
  element: state.editproject.element,
  role: state.role.datas2,
  //user: state.editproject.element.user,
  user: state.user.datas
});

const EditableFormTable = Form.create()(EditableTable);
export default connect(
  mapStateToProps,
  { fetchUser }
)(EditableFormTable);
