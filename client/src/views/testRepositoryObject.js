import React from "react";
import "antd/dist/antd.css";
import { Table } from "antd";
import { Input, Button, Icon, Form, Card, Breadcrumb, Row } from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import {
  fetchRepositoryStore,
  editRepositoryStore
} from "../redux/actions/repositoryStoreAction";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import Global from "../../src/components/layout/Global/Global";
export class TestRepositoryStore extends React.Component {

  constructor(props) {
    super(props);
      this.editedComponent ='';
    
  }

  componentWillMount() {
    console.log("fetch repository store inside");
    this.props.fetchRepositoryStore();
  }

  handleAdd = () => {
    this.props.history.push("/addrepositorystore/"+this.editedComponent);
  }

  handleClick(e) {
    this.props.editRepositoryStore(e);
    this.props.history.push("/editrepositorystore/"+this.editedComponent+'/store/' + e); // navigate to ur page
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={String(text)}
      />
    )
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  
  render() {
    const { match, location, history } = this.props;
    let myLoc = JSON.stringify(location.pathname);
    var finalstr = myLoc.replace(/"/g,"");
    this.editedComponent =finalstr.substring(finalstr.lastIndexOf('/')+1);

    const data = [];
    {

      this.props.objectItems.filter(object1 =>
        object1.orrefid===this.editedComponent).map(object => 
          data.push({
            // class1: object.class1,
            field: object.field,
            locator: object.locator,
            locator_value: object.locator_value,
            // name1: object.name1,
            repositObject_id: object.repositObject_id,
            repositoryObjectId: object._id,

            screen: object.screen,
            status: object.status,
            updated_by: object.updated_by,
            visibility: object.visibility
            // updated_on: object.updated_on,
          })
        
          );

      
      
    }

    const columns = [

      {
        dataIndex: "locator",
        key: "locator",
        title: "Locator",
        ...this.getColumnSearchProps("locator"),
         render: (text, record) => {
          return (
            <a
              className="tabletd"
              onClick={() => {
                this.handleClick(record.repositoryObjectId);
              }}
            >
              {text}
            </a>
          );
        }
      },
      {
        dataIndex: "locator_value",
        key: "locator_value",
        title: "Locator Value",
        ...this.getColumnSearchProps("locator_value")
      },
      {
        dataIndex: "screen",
        key: "screen",
        title: "Screen",
        ...this.getColumnSearchProps("screen")
      },
      {
        dataIndex: "field",
        key: "field",
        title: "Field",
        ...this.getColumnSearchProps("field")
      },
      {
        dataIndex: "status",
        key: "status",
        title: "Status",
        ...this.getColumnSearchProps("status")
      },

      {
        dataIndex: "visibility",
        key: "visibility",
        title: "Visibility",
        ...this.getColumnSearchProps("visibility")
      }
    ];

    return (
      <div className="container-fluid">
        <div className="Breadcrumb-top">
          <Row>
            <Breadcrumb>
              <Breadcrumb.Item>
                {" "}
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Home{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
              <Button
                  type="link"
                  onClick={e => {
                    e.preventDefault();
                    Global.history.push("/viewrepository");
                  }}
                >
                  {" "}
                  View Repository{" "}
                </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
            <Button
                  type="link"
                  onClick={e => {
                    e.preventDefault();
                    Global.history.push("/managerepository/"+this.editedComponent);
                  }}
                >
                  {" "}
                  Manage Repository{" "}
                </Button>
            </Breadcrumb.Item>
              <Breadcrumb.Item>View Repository Store</Breadcrumb.Item>
            </Breadcrumb>
          </Row>
        </div>

        <Row className="margintop">
          <div className="float-left">
            <h5>View Repository Store</h5>
          </div>
          <div className="float-right">
          <Button onClick={this.handleAdd} type="primary" style={{ marginBottom: 16 }}>
 Add a row
 </Button>
          </div>
          <hr className="hrline"></hr>
        </Row>

        <div className="cardheader">
          <Card>
            <Table
              dataSource={data}
              columns={columns}
              pagination={{ pageSize: 28 }}
              rowKey="_id"
            />
            <div style={{ color: "red" }} />
          </Card>
        </div>
      </div>
    );
  }
}

TestRepositoryStore.propTypes = {
  editRepositoryStore: PropTypes.func.isRequired,
  fetchRepositoryStore: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  objectElement: state.editStore.objectElement,
  objectItems: state.repositoryStore.objectItems
});
const TestRepositoryStoreTable = Form.create({ name: "repository store" })(
  TestRepositoryStore
);
export default connect(
  mapStateToProps,
  {
    editRepositoryStore,
    fetchRepositoryStore
  }
)(TestRepositoryStoreTable);

const ShowTheLocationWithRouter = withRouter(TestRepositoryStore);
//viewrepository