import React from "react";
import "antd/dist/antd.css";
import { Form, Input, Button, Select, Card } from "antd";
import { Table, InputNumber, Popconfirm } from "antd";
import { connect } from "react-redux";
import PropTypes from "prop-types";
const { TextArea } = Input;
const { Option } = Select;
const EditableContext = React.createContext();
const data = [];
export class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              initialValue: record[dataIndex],
              rules: [
                {
                  message: `Please Input ${title}!`,
                  required: true
                },
              ],
            })(this.getInput())}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
  }
}
class EditableTable extends React.Component {
    constructor(props) {
    super(props);
    this.state = { data, editingKey: '' };
    this.columns = [
      {
        dataIndex: 'projectname',
        editable: true,
        title: 'Project name',
        width: '60%',
      },
      {
        dataIndex: 'projectrole',
        editable: true,
        title: 'Project Role',
        width: '40%',
      }
      
    ];
  }

  isEditing = record => record.key === this.state.editingKey;

  cancel = () => {
    this.setState({ editingKey: '' });
  };

  save(form) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      console.log('elementsssssssssss',this.state.elements);
      //const newData = [...this.state.elements];
      //const index = newData.findIndex(item => key === item.key);
      // if (index > -1) {
      //   const item = newData[index];
      //   newData.splice(index, 1, {
      //     ...item,
      //     ...row,
      //   });
      //   this.setState({ data: newData, editingKey: '' });
      // } 
      // else {
      //   newData.push(row);
      //   this.setState({ data: newData, editingKey: '' });
      // }
    });
  }

  edit(key) {
    this.setState({ editingKey: key });

  }


  render() {

    const components = {
      body: {
        cell: EditableCell,
      },
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          dataIndex: col.dataIndex,
          editing: this.isEditing(record),
          inputType: col.dataIndex === 'age' ? 'number' : 'text',
          record,
          title: col.title,
        }),
      };
    });

    return (
      <EditableContext.Provider value={this.props.form}>
        <Table
          components={components}
          bordered
          dataSource={this.props.dataa}
          columns={columns}
          rowClassName="editable-row"
          pagination={{
            onChange: this.cancel,
          }}
        />
      </EditableContext.Provider>
    );
  }
}
 EditableTable.propTypes = {
  element: PropTypes.object.isRequired,
  project: PropTypes.object.isRequired
 };

 const mapStateToProps = state => ({
  dataa:state.edituser.dataa,
  element: state.edituser.element,
  project: state.edituser.element.project
 });

 const EditableFormTable = Form.create()(EditableTable);
 export default connect(mapStateToProps)(EditableFormTable);
