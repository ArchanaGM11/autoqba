import React from "react";
import "antd/dist/antd.css";
import { Table } from "antd";
import { Input, Button, Icon, Form, Card, Breadcrumb, Row } from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import Global from "../../src/components/layout/Global/Global";
import {
  fetchApplication,
  editApplication,
  viewApplication
} from "../redux/actions/applicationAction";
//import { loginAction } from "../redux/actions/loginAction"
import PropTypes from "prop-types";
import { fetchUser } from "../redux/actions/userAction";

export class Viewapplication extends React.Component {
  constructor(props) {
    super(props);
    /* matching User */
    this.matchusername = "";
  }
  componentWillMount() {
    this.props.viewApplication(this.props.uid);
    this.props.fetchUser();
    //this.props.fetchApplication();
  }

  handleClick(e) {
    this.props.editApplication(e);
    this.props.history.push("/editapplication/" + e); // navigate to ur page
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={String(text)}
      />
    )
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  render() {
    const data = [];
    if (this.props.items) {
      this.props.items.forEach(object => {
        console.log("items", object.User_id);
        this.props.user.filter(u => {
          console.log("User Name", u.userName);
          //console.log("User",t.User_id)
          if (u.User_id === object.User_id) {
            this.matchusername = u.UserName;
            //console.log("User Name: ",this.matchusername);
          }
          // else {
          //   this.matchusername = "";
          // }
        }); //tool end

        data.push({
          // api_technology: object.api_technology,
          applicationId: object._id,
          // application_id: object.application_id,
          application_name: object.application_name,
          application_version: object.application_version,
          created_by: this.matchusername,
          created_on: object.created_on,
          //  developed_by: object.developed_by,
          environment: object.environment,
          //  implemented_by: object.implemented_by,
          status: object.status,
          //  techstack: object.techstack,
          updated_by: this.matchusername,
          updated_on: object.updated_on,
          visibility: object.visibility
        });
      });
    }

    const columns = [
      // {
      //   dataIndex: "application_id",
      //   key: "application_id",
      //   title: "Application Id",
      //   ...this.getColumnSearchProps("application_id")
      // },
      {
        dataIndex: "application_name",
        key: "application_name",
        title: "Application Name",
        ...this.getColumnSearchProps("application_name"),
        render: (text, record) => {
          // let api = "/editapplication/";
          //this ID be sued for POST delete row like a API below
          // api = api + record.repositoryId;
          return (
            <a
              className="tabletd"
              onClick={() => {
                this.handleClick(record.applicationId);
              }}
            >
              {text}
            </a>
          );
        }
      },
      {
        dataIndex: "application_version",
        key: "application_version",
        title: "Application Version",
        ...this.getColumnSearchProps("application_version")
      },
      // {
      //   dataIndex: "api_technology",
      //   key: "api_technology",
      //   title: "Api Technology",
      //   ...this.getColumnSearchProps("api_technology")
      // },
      // {
      //   dataIndex: "techstack",
      //   key: "techstack",
      //   title: "Techstack",
      //   ...this.getColumnSearchProps("techstack")
      // },
      {
        dataIndex: "environment",
        key: "environment",
        title: "Environment",
        ...this.getColumnSearchProps("environment")
      },
      // {
      //   dataIndex: "developed_by",
      //   key: "developed_by",
      //   title: "Developed By",
      //   ...this.getColumnSearchProps("developed_by")
      // },
      // {
      //   dataIndex: "implemented_by",
      //   key: "implemented_by",
      //   title: "Implemented By",
      //   ...this.getColumnSearchProps("implemented_by")
      // },
      {
        dataIndex: "visibility",
        key: "visibility",
        title: "Visibility",
        ...this.getColumnSearchProps("visibility")
      },
      {
        dataIndex: "status",
        key: "status",
        title: "Status",
        ...this.getColumnSearchProps("status")
      },
      {
        dataIndex: "created_by",
        key: "created_by",
        title: "Created By",
        ...this.getColumnSearchProps("created_by")
      },
      {
        dataIndex: "created_on",
        key: "created_on",
        title: "Created On",
        ...this.getColumnSearchProps("created_on")
      },
      {
        dataIndex: "updated_by",
        key: "updated_by",
        title: "Updated By",
        ...this.getColumnSearchProps("updated_by")
      },
      {
        dataIndex: "updated_on",
        key: "updated_on",
        title: "Updated On",
        ...this.getColumnSearchProps("updated_on")
      }
    ];

    return (
      <div className="container-fluid">
        <div className="Breadcrumb-top">
          <Row>
            <Breadcrumb>
              <Breadcrumb.Item>
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Home{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>Application</Breadcrumb.Item>
              <Breadcrumb.Item>View Application</Breadcrumb.Item>
            </Breadcrumb>
          </Row>
        </div>

        <Row className="margintop">
          <div className="float-left">
            <h5>View Application</h5>
          </div>
          <hr className="hrline"></hr>
        </Row>

        <div className="cardheader">
          <Card>
            <Table
              dataSource={data}
              columns={columns}
              pagination={{ pageSize: 28 }}
              rowKey="_id"
            />
            <div style={{ color: "red" }} />
          </Card>
        </div>
      </div>
    );
  }
}

Viewapplication.propTypes = {
  editApplication: PropTypes.func.isRequired,
  element: PropTypes.object.isRequired,
  fetchApplication: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  pass: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  element: state.application.element,
  items: state.application.items,
  uid: state.loginData.User_id,
  uname: state.loginData.userName,
  user: state.user.datas
});
const ViewapplicationTable = Form.create({ name: "index" })(Viewapplication);
export default connect(
  mapStateToProps,
  { fetchApplication, editApplication, viewApplication, fetchUser }
)(ViewapplicationTable);
