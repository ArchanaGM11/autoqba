import React from "react";
import "antd/dist/antd.css";
import { Table } from "antd";
import { Input, Button, Icon, Form, Card, Breadcrumb, Row } from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import { fetchPrivilege } from "../redux/actions/privilegeAction";
import { editPrivilege } from "../redux/actions/privilegeAction";
import PropTypes from "prop-types";
export class Viewprivilege extends React.Component {
  componentWillMount() {
    console.log("fetchPrivilege inside");
    this.props.fetchPrivilege();
    console.log(101,this.props)
  }

  handleClick(e) {
    this.props.editPrivilege(e);
   this.props.history.push("/editprivilege/" + e);
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={String(text)}
        //textToHighlight={text.toString()}
      />
    )
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };
  redirecthome = e =>{
    e.preventDefault();
    this.props.history.push("/home");
  }
  render() {
    const data = [];
    {
      this.props.datas.map(object =>
        data.push({
          Created_by: object.Created_by,
          Privilege: object.Privilege,
          PrivilegeDescription: object.PrivilegeDescription,
          PrivilegeType: object.PrivilegeType,
          Privilege_id: object.Privilege_id,
          Status: object.Status,
          Updated_by: object.Updated_by,
          privilegeId: object._id,
        })
      );
    }
    const columns = [
      {
        dataIndex: "Privilege_id",
        key: "Privilege_id",
        title: "Privilege_id",
        ...this.getColumnSearchProps("Privilege_id")
      },
      {
        dataIndex: "Privilege",
        key: "Privilege",
        title: "Privilege",
        ...this.getColumnSearchProps("Privilege"),
        render: (text, record) => {
        
          return (
            <a
              className="tabletd"
              onClick={() => {
                this.handleClick(record.privilegeId);
              }}
            >
              {text}
            </a>
          );
        }
      },
      {
        dataIndex: "PrivilegeDescription",
        key: "PrivilegeDescription",
        title: "PrivilegeDescription",
        ...this.getColumnSearchProps("PrivilegeDescription")
      },
      {
        dataIndex: "PrivilegeType",
        key: "PrivilegeType",
        title: "PrivilegeType",
        ...this.getColumnSearchProps("PrivilegeType")
      },
      {
        dataIndex: "Status",
        key: "Status",
        title: "Status",
        ...this.getColumnSearchProps("Status")
      },
      {
        dataIndex: "Created_by",
        key: "Created_by",
        title: "Created_by",
        ...this.getColumnSearchProps("Created_by")
      },
      {
        dataIndex: "Updated_by",
        key: "Updated_by",
        title: "Updated_by",
        ...this.getColumnSearchProps("Updated_by")
      },
      
    ];

    return (
      <div className="container-fluid">
        <div className="Breadcrumb-top">
          <Row>
            <Breadcrumb>
              <Breadcrumb.Item>
                {" "}
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Home{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Privilege{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>View Privilege</Breadcrumb.Item>
            </Breadcrumb>
          </Row>
        </div>

        <Row className="margintop">
          <div className="float-left">
            <h5>View Privilege</h5>
          </div>
          <hr className="hrline"></hr>
        </Row>

        <div className="cardheader">
          <Card>
            <Table
              dataSource={data}
              columns={columns}
              pagination={{ pageSize: 28 }}
              rowKey="_id"
            />
            <div style={{ color: "red" }} />
          </Card>
        </div>
      </div>
    );
  }
}

Viewprivilege.propTypes = {
  datas: PropTypes.array.isRequired,
  editPrivilege: PropTypes.func.isRequired,
  fetchPrivilege: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    datas: state.privilege.datas,
    element: state.privilege.element,
});

const ViewprivilegeTable = Form.create({ name: "index" })(Viewprivilege);
export default connect(
  mapStateToProps,
  { fetchPrivilege, editPrivilege }
)(ViewprivilegeTable);
