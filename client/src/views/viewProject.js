import React from "react";
import "antd/dist/antd.css";
import { Table } from "antd";
import { Input, Button, Icon, Form, Card, Breadcrumb, Row } from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import {
  fetchProject,
  editProject,
  emptyeditproject,
  viewProject
} from "../redux/actions/projectAction";
import PropTypes from "prop-types";
import { fetchUser } from "../redux/actions/userAction";
import Global from "../../src/components/layout/Global/Global";
export class Viewproject extends React.Component {
  constructor(props) {
    super(props);
    /* matching User */
    this.matchusername = "";
  }

  componentWillMount() {
    this.props.viewProject(this.props.uid);
    this.props.emptyeditproject();
    this.props.fetchUser();
    //this.props.fetchProject();
    console.log("Exit Fetch Project");
  }

  handleClick(e) {
    this.props.editProject(e);
    this.props.history.push("/editProject/" + e); // navigate to ur page
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={String(text)}
      />
    )
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  render() {
    const data1 = [];
    if (this.props.items) {
      this.props.items.forEach(object => {
        console.log("items", object.User_id);
        this.props.user.filter(u => {
          console.log("User Name", u.userName);
          //console.log("User",t.User_id)
          if (u.User_id === object.User_id) {
            this.matchusername = u.UserName;
            //console.log("User Name: ",this.matchusername);
          }
        });

        data1.push({
          // project_id: object.project_id,
          created_by: this.matchusername,
          created_on: object.created_on,
          projectId: object._id,
          project_admin: this.matchusername,
          project_name: object.project_name,
          // Project_Desc: object.Project_Desc,
          status: object.status,
          updated_by: this.matchusername,
          updated_on: object.updated_on,
          userscount: object.userscount,
          visibility: object.visibility
          // user: object.user,
          //  user_id: object.user_id,
          //  user_name: object.user_name,
          //  role: object.role,
        });
      });
    }

    console.log(1, this.props.items);
    const columns = [
      {
        dataIndex: "project_name",
        key: "project_name",
        title: "Project Name",
        ...this.getColumnSearchProps("project_name"),
        render: (text, record) => {
          // let api = "/editapplication/";
          //this ID be sued for POST delete row like a API below
          // api = api + record.repositoryId;
          return (
            <a
              className="tabletd"
              onClick={() => {
                this.handleClick(record.projectId);
              }}
            >
              {text}
            </a>
          );
        }
      },
      {
        dataIndex: "visibility",
        key: "visibility",
        title: "Visibility",
        ...this.getColumnSearchProps("visibility")
      },
      {
        dataIndex: "status",
        key: "status",
        title: "Status",
        ...this.getColumnSearchProps("status")
      },
      {
        dataIndex: "userscount",
        key: "userscount",
        title: "users",
        ...this.getColumnSearchProps("userscount")
      },
      {
        dataIndex: "project_admin",
        key: "project_admin",
        title: "Project Admin",
        ...this.getColumnSearchProps("project_admin")
      },
      {
        dataIndex: "created_by",
        key: "created_by",
        title: "Created By",
        ...this.getColumnSearchProps("created_by")
      },
      {
        dataIndex: "created_on",
        key: "created_on",
        title: "Created On",
        ...this.getColumnSearchProps("created_on")
      },
      {
        dataIndex: "updated_by",
        key: "updated_by",
        title: "Updated By",
        ...this.getColumnSearchProps("updated_by")
      },
      {
        dataIndex: "updated_on",
        key: "updated_on",
        title: "Updated On",
        ...this.getColumnSearchProps("updated_on")
      }
    ];
    return (
      <div className="container-fluid">
        <div className="Breadcrumb-top">
          <Row>
            <Breadcrumb>
              <Breadcrumb.Item>
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Home{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>Project</Breadcrumb.Item>
              <Breadcrumb.Item>View Project</Breadcrumb.Item>
            </Breadcrumb>
          </Row>
        </div>

        <Row className="margintop">
          <div className="float-left">
            <h5>View Project</h5>
          </div>
          <hr className="hrline"></hr>
        </Row>

        <div className="cardheader">
          <Card>
            <Table
              dataSource={data1}
              columns={columns}
              pagination={{ pageSize: 28 }}
              rowKey="_id"
            />
            <div style={{ color: "red" }} />
          </Card>
        </div>
      </div>
    );
  }
}
Viewproject.propTypes = {
  editProject: PropTypes.func.isRequired,
  fetchProject: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired
};
const mapStateToProps = state => ({
  items: state.project.items,
  uid: state.loginData.User_id,
  user: state.user.datas
});

const ViewprojectTable = Form.create({ name: "index" })(Viewproject);
export default connect(
  mapStateToProps,
  { fetchProject, editProject, emptyeditproject, viewProject, fetchUser }
)(ViewprojectTable);
