import React from "react";
import "antd/dist/antd.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Form,
  Table,
  Input,
  Button,
  Icon,
  Pagination,
  Card,
  Breadcrumb,
  Row
} from "antd";
//import {Link} from 'react';
import Highlighter from "react-highlight-words";
import {
  fetchRepository,
  editRepository
} from "../redux/actions/repositoryAction";
import {
  fetchApplication,
  editApplication
} from "../redux/actions/applicationAction";
import { fetchTool } from "../redux/actions/toolAction";
import { Link } from "react-router-dom";
import Global from "../../src/components/layout/Global/Global";
// import { push } from 'react-router-redux';
class ViewRepository extends React.Component {
  constructor(props) {
    super(props);
    /* matching apllication */
    (this.matchApp = {}),
      (this.matchAppName = ""),
      (this.matchAppVersion = ""),
      (this.matchAppEnvironment = ""),
      (this.matchTool = {}),
      (this.matchToolName = ""),
      (this.matchToolVersion = "");
  }

  componentDidMount() {
    // console.log("Going to Fetch Repository");
    this.props.fetchApplication();
    this.props.fetchRepository();
    this.props.fetchTool();
    console.log(100, this.props);
  }

  componentWillUnmount() {
    console.log(100, this.props);
  }

  handleClick(e) {
    console.log("view Repository event : ", e.repositoryId);
    this.props.editRepository(e.repositoryId);

    console.log(101, this.props);

    this.props.history.push("/managerepository/" + e.repositoryId);
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      clearFilters,
      confirm,
      selectedKeys,
      setSelectedKeys
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={String(text)}
      />
    )
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  render() {
    const data1 = [];
    const data2 = [];

    if (this.props.items) {
      this.props.items.forEach(element => {
        if(this.props.app){
        this.props.app.filter(a => {
          this.matchApp = a._id === element.application_name;
          if (this.matchApp) {
            // console.log("matchApp app name", a.application_name);
            // console.log("matchApp verison name", a.application_version);
            (this.matchAppName = a.application_name),
              (this.matchAppVersion = a.application_version),
              (this.matchAppEnvironment = a.environment);
          }
        }); //application end
      }//if app loop end

      if(this.props.too){
        this.props.too.filter(t => {
          this.matchTool = t._id === element.tool_name;
          // console.log("match Tool : ",t._id,element.tool_name)
          if (this.matchTool) {
            // console.log("matchTool app name", t.tool_name);
            // console.log("matchTool verison name", t.tool_version);
            (this.matchToolName = t.tool_name),
              (this.matchToolVersion = t.tool_version);
            // console.log("Tool Name: ",this.matchToolName);
          }
        }); //tool end
      }//if app loop end
        data2.push({
          application_name: this.matchAppName,
          application_version: this.matchAppVersion,
          environment_name: this.matchAppEnvironment,
          key: element.uid,
          or_name: element.or_name,
          repositoryId: element._id,
          repository_id: element.repository_id,
          status: element.status,
          tool_name: this.matchToolName,
          tool_version: this.matchToolVersion,
          visibility: element.visibility
        }); // Data Push
      });
    } else {
      console.log("ForEach error ");
    }

    const columns = [
      {
        ...this.getColumnSearchProps("or_name"),
        align: "center",
        dataIndex: "or_name",
        key: "or_name",
        render: (text, record) => {
          let api = "/managerepository/";
          //this ID be sued for POST delete row like a API below
          api = api + record.repositoryId;
          return (
            <a
              className="tabletd"
              onClick={() => {
                this.handleClick(record);
              }}
            >
              {text}
            </a>
          );
        },
        title: "OR"
      },
      {
        ...this.getColumnSearchProps("visibility"),
        align: "center",
        dataIndex: "visibility",
        key: "visibility",
        title: "Visibility"
      },
      {
        children: [
          {
            ...this.getColumnSearchProps("application_name"),
            align: "center",
            dataIndex: "application_name",
            key: "application_name",
            title: "Name"
          },
          {
            ...this.getColumnSearchProps("application_version"),
            align: "center",
            dataIndex: "application_version",
            key: "application_version",
            title: "Version"
          },
          {
            ...this.getColumnSearchProps("environment_name"),
            align: "center",
            dataIndex: "environment_name",
            key: "environment_name",
            title: "Environment"
          }
        ],
        title: "Application"
      },
      {
        children: [
          {
            ...this.getColumnSearchProps("tool_name"),
            align: "center",
            dataIndex: "tool_name",
            key: "tool_name",
            title: "Name"
          },
          {
            ...this.getColumnSearchProps("tool_version"),
            align: "center",
            dataIndex: "tool_version",
            key: "tool_version",
            title: "Version"
          },
          {
            ...this.getColumnSearchProps("status"),
            align: "center",
            dataIndex: "status",
            key: "status",
            title: "Status"
          }
        ],
        title: "Tool"
      }
    ];

    // console.log(1111111111111, this.props.items);
    return (
      <div className="container-fluid">
        <div className="Breadcrumb-top">
          <Row>
            <Breadcrumb>
              <Breadcrumb.Item>
                {" "}
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Home{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                {" "}
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Repository{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>View Repository</Breadcrumb.Item>
            </Breadcrumb>
          </Row>
        </div>

        <Row className="margintop">
          <div className="float-left">
            <h5>View Repository</h5>
          </div>
          <hr className="hrline"></hr>
        </Row>

        <div className="cardheader">
          <Card>
            <Table
              dataSource={data2}
              columns={columns}
              pagination={{ pageSize: 28 }}
              rowKey="_id"
              bordered
            />
            <div style={{ color: "red" }} />
          </Card>
        </div>
      </div>
    );
  }
}

ViewRepository.propTypes = {
  editApplication: PropTypes.func.isRequired,
  editRepository: PropTypes.func.isRequired,
  fetchApplication: PropTypes.func.isRequired,
  fetchRepository: PropTypes.func.isRequired,
  fetchTool: PropTypes.func.isRequired
  // items: PropTypes.array.isRequired,
};

const mapStateToProp = state => ({
  app: state.application.items,
  eleApp: state.edit.appName,
  element: state.editOr.element,
  items: state.repository.items,
  too: state.tool.items
});

const ViewRepositoryTable = Form.create({ name: "index" })(ViewRepository);

export default connect(
  mapStateToProp,
  {
    editApplication,
    editRepository,
    fetchApplication,
    fetchRepository,
    fetchTool,
  }
)(ViewRepositoryTable);
