import React from "react";
import "antd/dist/antd.css";
import { Table } from "antd";
import {
  Input,
  Button,
  Icon,
  Select,
  Form,
  Card,
  Breadcrumb,
  Row,
  Divider,
  Popconfirm
} from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import {
  createRepositoryStore,
  fetchRepositoryStore,
  filterRepositoryStore,
  editRepositoryStore,
  updateRepositoryStore,
  deleteRepositoryStore
} from "../redux/actions/repositoryStoreAction";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import Global from "../../src/components/layout/Global/Global";
const EditableContext = React.createContext();
let filterItems = [];
export class EditableCell extends React.Component {
  constructor(props) {
    super(props);
    this.state = { disabled: true };
  }

  getInput = index => {
    if (this.props.inputType === "text") {
      return <Input />;
    } else if (this.props.inputType === "list") {
      if (index === "locator") {
        return (
          <Select style={{ width: "100%" }} placeholder="Select Role">
            <Option value="id">Id</Option>
            <Option value="name">Name</Option>
            <Option value="class">Class</Option>
            <Option value="tag">Tag</Option>
            <Option value="css">CSS</Option>
            <Option value="xpath">XPath</Option>
            <Option value="linktext">Link Text</Option>
            <Option value="partiallinktext">Partial Link Text</Option>
          </Select>
        );
      } else if (index === "class1") {
        return (
          <Select style={{ width: "100%" }} placeholder="Select Class 1">
            <Option value="mobile">Mobile</Option>
            <Option value="browser">Browser</Option>
            <Option value="application">Application</Option>
          </Select>
        );
      } else if (index === "class3") {
        return (
          <Select style={{ width: "100%" }} placeholder="Select Class 1">
            <Option value="textbox">TextBox</Option>
            <Option value="checkbox">CheckBox</Option>
            <Option value="radio">Radio</Option>
            <Option value="search">Search</Option>
            <Option value="password">Password</Option>
            <Option value="dropdown">Dropdown</Option>
          </Select>
        );
      }
    }
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              initialValue: record[dataIndex],
              rules: [
                {
                  message: `Please Input ${title}!`,
                  required: true
                }
              ]
            })(this.getInput(dataIndex))}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return (
      <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
    );
  }
}
export class ViewRepositoryStore extends React.Component {
  componentWillMount() {
    console.log("fetch repository store inside");
    //     const { match, location, history } = this.props;
    //     let myLoc = JSON.stringify(location.pathname);
    //     var finalstr = myLoc.replace(/"/g, "");
    //     this.editedComponent = finalstr.substring(finalstr.lastIndexOf("/") + 1);
    // console.log("filter component store : ",this.props.element._id+":::::"+this.editedComponent);
    this.props.filterRepositoryStore(this.props.element._id);
  }
  constructor(props) {
    super(props);
    // this.editedComponent = "";
    this.state = { editingKey: "", filterItems };
    this.columns = [
      {
        dataIndex: "class1",
        editable: true,
        key: "class1",
        title: "Class1 ",
        ...this.getColumnSearchProps("class1")
      },
      {
        dataIndex: "name1",
        editable: true,
        key: "name1",
        title: "Name1 ",
        ...this.getColumnSearchProps("name1")
      },
      {
        dataIndex: "class2",
        editable: true,
        key: "class2",
        title: "Class2 ",
        ...this.getColumnSearchProps("class2")
      },
      {
        dataIndex: "name2",
        editable: true,
        key: "name2",
        title: "Name2 ",
        ...this.getColumnSearchProps("name2")
      },
      {
        dataIndex: "class3",
        editable: true,
        key: "class3",
        title: "Class3 ",
        ...this.getColumnSearchProps("class3")
      },
      {
        dataIndex: "name3",
        editable: true,
        key: "name3",
        title: "Name3 ",
        ...this.getColumnSearchProps("name3")
      },
      {
        dataIndex: "locator",
        editable: true,
        key: "locator",
        title: "Locator",
        ...this.getColumnSearchProps("locator")
      },
      {
        dataIndex: "locator_value",
        editable: true,
        key: "locator_value",
        title: "LocatorValue",
        ...this.getColumnSearchProps("locator_value")
      },
      {
        dataIndex: "screen",
        editable: true,
        key: "screen",
        title: "Screen",
        ...this.getColumnSearchProps("screen")
      },
      {
        dataIndex: "field",
        editable: true,
        key: "field",
        title: "Field",
        ...this.getColumnSearchProps("field")
      },
      {
        dataIndex: "operation",
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <a
                    onClick={() => this.save(form, record)}
                    style={{ marginRight: 8 }}
                  >
                    Save
                  </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm
                title="Sure to cancel?"
                onConfirm={() => this.cancel(record._id)}
              >
                <a>Cancel</a>
              </Popconfirm>
            </span>
          ) : (
            <span>
              <a
                disabled={editingKey !== ""}
                onClick={() => this.edit(record._id)}
              >
                Edit
                <Divider type="vertical" />
              </a>

              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => this.handleDelete(record._id)}
              >
                <a>Delete</a>
              </Popconfirm>
            </span>
          );
        },
        title: "Action"
      }
    ]; //this.columns end
  } // constructor ends

  isEditing = record => record._id === this.state.editingKey;

  cancel = rowkey => {
    if (rowkey) {
      console.log(this.props.filterItems.length);
    } else {
      this.props.filterItems.pop();
    }

    // this.props.filterItems.pop();
    this.setState({ editingKey: "" });
  };

  save(form, repObj) {
    const { record } = this.props;
    console.log("save method :", repObj);

    if (repObj.locator_value === "" && repObj.field === "") {
      form.validateFields((error, row) => {
        console.log("validate row : ", row);
        let keey = "orrefid";
        row[`orrefid`] = this.props.element._id;
        const myData = [];
        myData.push(row)
        console.log("myDataaaaa add row : ", myData)
        this.props.createRepositoryStore(myData);
        if (error) {
          return;
        }
      });
     } 
   else {
      form.validateFields((error, row) => {
        console.log("row :::::", row);
        const myData = [];
        myData.push(row);
        console.log("myDataaaa edit row : ", myData);
        const index = myData.findIndex(item => {
          console.log("myData Itemmm::::", item);
          if (item) {
            return true;
          } else {
            return false;
          }
        });
        console.log("Item index :::::::: ====", index);
        // const newData = [this.props.filterItems]
        if (index > -1) {
          // let list = this.props.filterItems.splice(index, 1, {
          //   ...newData,
          //   ...row
          // });
          // console.log("list item ::::", list);
          filterItems = myData;
          console.log("filterItem after data set::::", filterItems);
          this.props.updateRepositoryStore(repObj._id, filterItems);
        }
        if (error) {
          return;
        }
      });
    }
  }

  edit(id) {
    this.setState({ editingKey: id });
  }
  handleAdd = () => {
    console.log("handle add : empty row");
    var store = {
      _id: "",
      class1: "",
      class2: "",
      class3: "",
      field: "",
      locator: "",
      locator_value: "",
      name1: "",
      name2: "",
      name3: "",
      screen: ""
    };
    this.props.filterItems.push(store);
    this.setState(this.props.filterItems);
  };

  handleDelete = id => {
    this.props.deleteRepositoryStore(id);
    console.log("delete repo : ", this.props.deleteObjectElement);
    this.props.filterItems.pop();
  };

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={String(text)}
      />
    )
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  render() {
    const components = {
      body: {
        cell: EditableCell
      }
    };

    const mycolumns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      function getDynamicType(title) {
        if (title === "locator" || title === "class1" || title === "class3") {
          return "list";
        } else {
          return "text";
        }
      }
      return {
        ...col,
        onCell: record => ({
          dataIndex: col.dataIndex,
          editing: this.isEditing(record),
          inputType: getDynamicType(col.dataIndex),
          record,
          title: col.title
        })
      };
    });

    return (
      <div className="container-fluid">
        <div className="Breadcrumb-top">
          <Row>
            <Breadcrumb>
              <Breadcrumb.Item>
                {" "}
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Home{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                {" "}
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Repository{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                <Button
                  type="link"
                  onClick={e => {
                    e.preventDefault();
                    Global.history.push("/viewrepository");
                  }}
                >
                  {" "}
                  View Repository{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                <Button
                  type="link"
                  onClick={e => {
                    e.preventDefault();
                    Global.history.push(
                      "/managerepository/" + this.props.element._id
                    );
                  }}
                >
                  {" "}
                  Manage Repository{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>View Repository Store</Breadcrumb.Item>
            </Breadcrumb>
          </Row>
        </div>

        <Row className="margintop">
          <div className="float-left">
            <h5>View Repository Store</h5>
          </div>
          <div className="float-right">
            <Button
              onClick={this.handleAdd}
              type="primary"
              style={{ marginBottom: 16 }}
            >
              Add Object
            </Button>
          </div>
          <hr className="hrline"></hr>
        </Row>

        <div className="cardheader">
          <Card>
            <EditableContext.Provider value={this.props.form}>
              <Table
                components={components}
                bordered
                dataSource={this.props.filterItems}
                columns={mycolumns}
                rowClassName="editable-row"
                rowKey="_id"
                pagination={{
                  onChange: this.cancel
                }}
              />
              <div style={{ color: "red" }} />
            </EditableContext.Provider>
          </Card>
        </div>
      </div>
    );
  }
}

ViewRepositoryStore.propTypes = {
  createRepositoryStore: PropTypes.func.isRequired,
  deleteRepositoryStore: PropTypes.func.isRequired,
  editRepositoryStore: PropTypes.func.isRequired,
  fetchRepositoryStore: PropTypes.func.isRequired,
  filterRepositoryStore: PropTypes.func.isRequired,
  updateRepositoryStore: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  deleteObjectElement: state.repositoryStore.deleteObjectElement,
  element: state.editOr.element,
  filterItems: state.repositoryStore.filterItems,
  objectElement: state.editStore.objectElement,
  objectItem: state.repositoryStore.objectItem,
  objectItems: state.repositoryStore.objectItems,
  updateObjectElement: state.repositoryStore.updateObjectElement
});
const ViewRepositoryStoreTable = Form.create({ name: "repository store" })(
  ViewRepositoryStore
);
export default connect(
  mapStateToProps,
  {
    createRepositoryStore,
    deleteRepositoryStore,
    editRepositoryStore,
    fetchRepositoryStore,
    filterRepositoryStore,
    updateRepositoryStore
  }
)(ViewRepositoryStoreTable);

const ShowTheLocationWithRouter = withRouter(ViewRepositoryStore);

// edit and delete
// test repository
