import React from "react";
import "antd/dist/antd.css";
import { Table } from "antd";
import { Input, Button, Icon, Form, Card, Breadcrumb, Row } from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import { fetchTool, viewTool } from "../redux/actions/toolAction";
import { editTool } from "../redux/actions/toolAction";
import PropTypes from "prop-types";
import Global from "../../src/components/layout/Global/Global";
import { fetchUser } from "../redux/actions/userAction";
export class Viewtool extends React.Component {
  constructor(props) {
    super(props);
    /* matching User */
    this.matchusername = "";
  }
  componentWillMount() {
    this.props.viewTool(this.props.uid);
    this.props.fetchUser();
    console.log("fetchTool inside");
    //this.props.fetchTool();
  }

  handleClick(e) {
    this.props.editTool(e);
    this.props.history.push("/edittool/" + e); // navigate to ur page
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={String(text)}
      />
    )
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  render() {
    const data = [];
    if (this.props.items) {
      this.props.items.forEach(object => {
        console.log("items", object.User_id);
        this.props.user.filter(u => {
          console.log("User Name", u.userName);
          //console.log("User",t.User_id)
          if (u.User_id === object.User_id) {
            this.matchusername = u.UserName;
            //console.log("User Name: ",this.matchusername);
          }
        });

        data.push({
          created_by: this.matchusername,
          created_on: object.created_on,
          // field_objects: object.field_objects,
          // screen_objects: object.screen_objects,
          status: object.status,
          technology: object.technology,
          toolId: object._id,
          // tool_id: object.tool_id,
          tool_name: object.tool_name,
          tool_version: object.tool_version,
          updated_by: this.matchusername,
          updated_on: object.updated_on,
          visibility: object.visibility
          //window_objects: object.window_objects
        });
      });
    }
    const columns = [
      // {
      //   dataIndex: "tool_id",
      //   key: "tool_id",
      //   title: "Tool_id",
      //   ...this.getColumnSearchProps("tool_id")
      // },
      {
        dataIndex: "tool_name",
        key: "tool_name",
        title: "Tool_name",
        ...this.getColumnSearchProps("tool_name"),
        render: (text, record) => {
          // let api = "/edittool/";
          // //this ID be sued for POST delete row like a API below
          // api = api + record.repositoryId;
          return (
            <a
              className="tabletd"
              onClick={() => {
                this.handleClick(record.toolId);
              }}
            >
              {text}
            </a>
          );
        }
        //render: text => <a href="/edittool">{text}</a>
      },
      {
        dataIndex: "tool_version",
        key: "tool_version",
        title: "Tool_version",
        ...this.getColumnSearchProps("tool_version")
      },
      {
        dataIndex: "technology",
        key: "technology",
        title: "Technology",
        ...this.getColumnSearchProps("technology")
      },
      // {
      //   dataIndex: "window_objects",
      //   key: "window_objects",
      //   title: "Window Objects",
      //   ...this.getColumnSearchProps("window_objects")
      // },
      // {
      //   dataIndex: "screen_objects",
      //   key: "screen_objects",
      //   title: "Screen Objects",
      //   ...this.getColumnSearchProps("screen_objects")
      // },
      // {
      //   dataIndex: "field_objects",
      //   key: "field_objects",
      //   title: "Field Objects",
      //   ...this.getColumnSearchProps("field_objects")
      // },
      {
        dataIndex: "visibility",
        key: "visibility",
        title: "Visibility",
        ...this.getColumnSearchProps("visibility")
      },
      {
        dataIndex: "status",
        key: "status",
        title: "Status",
        ...this.getColumnSearchProps("status")
      },
      {
        dataIndex: "created_by",
        key: "created_by",
        title: "Created By",
        ...this.getColumnSearchProps("created_by")
      },
      {
        dataIndex: "created_on",
        key: "created_on",
        title: "Created On",
        ...this.getColumnSearchProps("created_on")
      },
      {
        dataIndex: "updated_by",
        key: "updated_by",
        title: "Updated By",
        ...this.getColumnSearchProps("updated_by")
      },
      {
        dataIndex: "updated_on",
        key: "updated_on",
        title: "Updated On",
        ...this.getColumnSearchProps("updated_on")
      }
    ];

    return (
      <div className="container-fluid">
        <div className="Breadcrumb-top">
          <Row>
            <Breadcrumb>
              <Breadcrumb.Item>
                <Button type="link" onClick={this.redirecthome}>
                  {" "}
                  Home{" "}
                </Button>
              </Breadcrumb.Item>
              <Breadcrumb.Item>Tool</Breadcrumb.Item>
              <Breadcrumb.Item>View Tool</Breadcrumb.Item>
            </Breadcrumb>
          </Row>
        </div>

        <Row className="margintop">
          <div className="float-left">
            <h5>View Tool</h5>
          </div>
          <hr className="hrline"></hr>
        </Row>

        <div className="cardheader">
          <Card>
            <Table
              dataSource={data}
              columns={columns}
              pagination={{ pageSize: 28 }}
              rowKey="_id"
            />
            <div style={{ color: "red" }} />
          </Card>
        </div>
      </div>
    );
  }
}

Viewtool.propTypes = {
  editTool: PropTypes.func.isRequired,
  fetchTool: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  element: state.tool.element,
  items: state.tool.items,
  uid: state.loginData.User_id,
  user: state.user.datas
});

const ViewtoolTable = Form.create({ name: "index" })(Viewtool);
export default connect(
  mapStateToProps,
  { fetchTool, editTool, viewTool, fetchUser }
)(ViewtoolTable);
