import React from "react";
import "antd/dist/antd.css";
import { Table } from "antd";
import { Input, Button, Icon, Form, Card, Breadcrumb, Row } from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import { fetchUser } from "../redux/actions/userAction";
import { editUser } from "../redux/actions/userAction";
import PropTypes from "prop-types";
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

export class Viewuser extends React.Component {
  componentWillMount() {
    console.log("View all the users");
    this.props.fetchUser();
    console.log(102,this.props)
  }

  handleClick(e) {
   this.props.editUser(e);
   this.props.history.push("/edituser/" + e);
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={node => {
              this.searchInput = node;
            }}
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={e =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
            style={{ width: 188, marginBottom: 8, display: "block" }}
          />
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm)}
            icon="search"
            size="small"
            style={{ width: 90, marginRight: 8 }}
          >
            Search
        </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
        </Button>
        </div>
      ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={String(text)}
        //textToHighlight={text.toString()}
      />
    )
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };
  redirecthome = e => {
    e.preventDefault();
    this.props.history.push("/home");
  }

  render() {
    const data = [];
    {
      if(this.props.datas){
      this.props.datas.map(object =>
        data.push({
          CompanyName: object.CompanyName,
          Created_at: object.Created_at,
          Email: object.Email,
          FirstName: object.FirstName,
          LastName: object.LastName,
          Role: object.Role,
          Status: object.Status,
          Updated_at: object.Updated_at,
          UserName: object.UserName,
          User_id:object.User_id,
          Visibility: object.Visibility,
          userId: object._id,

        })
      );
      }
    }

    const columns = [
      {
        dataIndex: "User_id",
        key: "User_id",
        title: "User_id",
        ...this.getColumnSearchProps("User_id")
      },
      {
        dataIndex: "UserName",
        key: "UserName",
        title: "User Name",
        ...this.getColumnSearchProps("UserName"),
        render: (text, record) => {
        
          return (
            <a
              className="tabletd"
              onClick={() => {
                this.handleClick(record.userId);
              }}
            >
              {text}
            </a>
          );
        } 
      },
      {
        dataIndex: "FirstName",
        key: "FirstName",
        title: "First Name",
        ...this.getColumnSearchProps("FirstName")
      },
      {
        dataIndex: "LastName",
        key: "LastName",
        title: "Last Name",
        ...this.getColumnSearchProps("LastName")
      },
      {
        dataIndex: "Email",
        key: "Email",
        title: "Email",
        ...this.getColumnSearchProps("Email")
      },
      {
        dataIndex: "CompanyName",
        key: "CompanyName",
        title: "Company Name",
        ...this.getColumnSearchProps("CompanyName")
      },
      {
        dataIndex: "Role",
        key: "Role",
        title: "Role",
        ...this.getColumnSearchProps("Role")
      },
      {
        dataIndex: "Visibility",
        key: "Visibility",
        title: "Visibility",
        ...this.getColumnSearchProps("Visibility")
      },
      {
        dataIndex: "Status",
        key: "Status",
        title: "Status",
        ...this.getColumnSearchProps("Status")
      },
      {
        dataIndex: "Created_at",
        key: "Created_at",
        title: "Created at",
        ...this.getColumnSearchProps("Created_at")
      },
      {
        dataIndex: "Updated_at",
        key: "Updated_at",
        title: "Updated at",
        ...this.getColumnSearchProps("Updated_at")
      }
    ];

    return (
      <div className="container-fluid">
        <div className="Breadcrumb-top">
          <Row>
            <Breadcrumb>
              <Breadcrumb.Item> <Button type="link" onClick={this.redirecthome}> Home </Button></Breadcrumb.Item>
              <Breadcrumb.Item>View User</Breadcrumb.Item>
            </Breadcrumb>
          </Row>
        </div>

        <Row className="margintop">
          <div className="float-left">
            <h5>View User</h5>
          </div>
          <hr className="hrline"></hr>
        </Row>


        <div className="cardheader">
          <Card>

            <Table
              dataSource={data}
              columns={columns}
              pagination={{ pageSize: 28 }}
              rowKey="_id"
            />


            <div style={{ color: "red" }} />
          </Card>
        </div>

      </div>
    );
  }
}

Viewuser.propTypes = {
  datas: PropTypes.array.isRequired,
  editUser: PropTypes.func.isRequired,
  fetchUser: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  datas: state.user.datas,
  element:state.user.element,
});

const ViewuserTable = Form.create({ name: "index" })(Viewuser);
export default connect(
  mapStateToProps,
  { fetchUser, editUser }
)(ViewuserTable);
