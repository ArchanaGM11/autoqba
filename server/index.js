const express = require('express')
const bodyParser= require('body-parser')
cors = require('cors')
const app= express()

const tokenValidator = require('./src/user-management/controllers/token.controller')
app.use(tokenValidator)

const logger = require('./src/user-management/controllers/log.controller')
app.use(logger);

var routes = require('./src/project-admin/routes/index.route');
const mongoose= require('mongoose')
var options = {
    server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 }, useCreateIndex: true, useNewUrlParser: true}
      };
let dbCon = 'mongodb://uci.server:27017/AutoQ-BA';
console.log(100,process.env.NODE_ENV)
if(process.env.NODE_ENV === 'local') {
  dbCon = 'mongodb://localhost:27017/AutoQ-BA';
}
mongoose.connect(dbCon,options)
mongoose.Promise=global.Promise;

const db=mongoose.connection
db.on('error',(error)=>console.error(error))
db.once('open',()=> console.log('Connected to Database'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())
app.use('/api', routes);


/* User management */
const usersRouter = require('./src/user-management/routes/user.route')
app.use('/users',usersRouter)

const resetRouter = require('./src/user-management/routes/user.route')
app.use('/password',resetRouter)

/* Object Repository*/
const repositoryRouter = require('./src/object-repository/routes/repository.route')
app.use('/object',repositoryRouter);


app.listen(4000,() => console.log('Server Started', 'Connected to DB:',dbCon))

//server side index.js