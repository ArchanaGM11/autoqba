const mongoose = require('mongoose')
const Schema = mongoose.Schema;

let userProjectRole = new Schema({
    User_id: {
        type: String,
        unique: [true, 'User Id should be unique'],
        //required: [true, 'User Id required'],
    },
    Role_id: {
        type: String,
        unique: [true, 'Role Id should be unique'],
        //required: [true, 'User Id required'],
    },
    project_id: {
        type: String,
        unique: [true, 'Project Id should be unique'],
        //required: [true, 'User Id required'],
    },
    Status: {
        type: String,
        default: 'Active',
        required: [true, 'Status is required']
    },
    Visibility: {
        type: String,
        default: 'Private',
        required: [true, 'Visibility is required']
    },
    project: {
        type: Array,
        required: [true, "Project required"]
    },
    Created_at: {
        type: String,
    },
    Updated_at: {
        type: String,
    }
});

module.exports = mongoose.model('userprojectrole', userProjectRole)