var mongoose = require("mongoose");
var dateFormat = require("dateformat");
require("../models/repository.model");
var ObjectRepository = mongoose.model("Repository");
var autoIncrement = require("mongodb-autoincrement");
const db = mongoose.connection;
var collectionName = "repository";

/*Application import */
var appController = require("../../project-admin/controllers/application.controller");
var toolController = require("../../project-admin/controllers/tool.controller");
//POST
module.exports.addObjectRepository = function(req, res) {
  console.log("POST new ObjectRepository");

  ObjectRepository.find(
    {
      $and: [
        { application_name: req.body.application_name },
        { application_version: req.body.application_version },
        { environment: req.body.environment },
        { tool_name: req.body.tool_name },
        { tool_version: req.body.tool_version }
      ]
    },
    function(err, docs) {
      if (docs.length) {
        res.status(409).json(err);
        console.log("Duplicate entry");
        //  cb('Name exists already',null);
      } else {
        var or_name = req.body.or_name;
        var visibility = req.body.visibility;
        var application_name = req.body.application_name;
        var application_version = req.body.application_version;
        var environment_name = req.body.environment_name;
        var tool_name = req.body.tool_name;
        var tool_version = req.body.tool_version;
        var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss tt");
        var created_on = myDate;
        var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss tt");
        var updated_on = myDate;
        var created_by = req.body.created_by;
        var updated_by = req.body.updated_by;

        autoIncrement.getNextSequence(db, collectionName, function(
          err,
          autoIndex
        ) {
          ObjectRepository.create(
            {
              repository_id: "OR" + autoIndex,
              or_name: or_name,
              visibility: visibility,
              application_name: application_name,
              application_version: application_version,
              environment_name: environment_name,
              tool_name: tool_name,
              tool_version: tool_version,
              created_on: created_on,
              updated_on: updated_on,
              created_by: created_by,
              updated_by: updated_by
            },
            function(err, objectrepository) {
              if (err) {
                console.log("Error creating repository");
                res.status(400).json(err);
                console.log(err);
              } else {
                res.status(201).json(objectrepository);
                console.log(
                  "success OR :::",
                  objectrepository.application_name
                );
                appController.appUpdatebyStatusRepo(
                  objectrepository.application_name
                );
                toolController.toolUpdatebyStatusRepo(objectrepository.tool_name);
              }
            }
          );
        });
      }
    }
  );
};

module.exports.getAllObjectRepository = (req, res) => {
  ObjectRepository.find(function(err, objectrepository) {
    if (err) {
      res.status(404).json({ message: err });
    } else {
      res.status(200).json(objectrepository);
    }
  });
};

// get one object repository
module.exports.getOneObjectRepository = function(req, res) {
  console.log("Get One Repository --> repository.controller");
  var id = req.params.repositoryId;
  console.log("GET objectrepositoryId", id);

  ObjectRepository.findById(id).exec(function(err, doc) {
    var response = {
      status: 200,
      message: doc
    };
    if (err) {
      console.log("Error finding repository", err);
      response.status = 500;
      response.message = err;
    } else if (!doc) {
      console.log(doc);
      console.log("Repository Id not found in database", id);
      response.status = 404;
      response.message = {
        message: "Object Repository not found " + id
      };
    }
    res.status(response.status).json(response.message);
    console.log(doc);
  });
};

/* UPDATE STATUS*/
module.exports.repositoryUpdatebyStatus = (req, res) => {
  console.log("repository Update status : : : ", req + ":::" + req.params);

  var repositoryId = "";
  if (req.params) {
    repositoryId = req.params.repositoryId;
    console.log("repository req.params  : : : ", repositoryId);
  } else {
    repositoryId = req;
    console.log("repository req : : : ", repositoryId);
  }

  console.log("repository updated by repo", repositoryId);
  ObjectRepository.findById(repositoryId).exec(function(err, doc) {
    console.log("doc length of repo repo", doc);
    if (doc.status === "new" || doc.status === "New") {
      doc.status = "active";
      doc.save(function(err, repositoryupdated) {
        console.log(repositoryupdated);
      });
    }
    else if(doc.status === "active" || doc.status === "Active"){
      doc.status = "deactive";
      doc.save(function(err, repositoryupdated) {
        console.log(repositoryupdated);
      });
    }
    else if(doc.status === "deactive" || doc.status === "Deactive"){
      doc.status = "active";
      doc.save(function(err, repositoryupdated) {
        console.log(repositoryupdated);
      });
    }
  });
};

// inside the render() --> under columns
module.exports.updateOneObjectRepository = function(req, res) {
  ObjectRepository.find(
    {
      $and: [
        { application_name: req.body.application_name },
        { application_version: req.body.application_version },
        { environment: req.body.environment },
        { tool_name: req.body.tool_name },
        { tool_version: req.body.tool_version },
        { repositoryId: { $nin: [req.body.repositoryId] } }
      ]
    },
    function(err, docs) {
      if (docs.length > 0) {
        dupId = docs[0]._id;
        if (dupId == req.params.objectrepositoryId) {
          var objectrepositoryId = req.params.repositoryId;
          Objectrepository.findById(objectrepositoryId).exec(function(
            err,
            objectrepository
          ) {
            if (err) {
              res.status(500).json(err);
              return;
            } else if (!objectrepository) {
              res.status(404).lson({
                message: "Objectrepository Id not found " + objectrepositoryId
              });
              return;
            }
            var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
            objectrepository.application_name = req.body.application_name;
            objectrepository.application_version = req.body.application_version;
            objectrepository.environment_name = req.body.environment_name;
            objectrepository.tool_name = req.body.tool_name;
            objectrepository.tool_version = req.body.tool_version;
            objectrepository.or_name = req.body.or_name;
            objectrepository.visibility = req.body.visibility;
            objectrepository.updated_on = myDate;
            objectrepository.updated_by = req.body.updated_by;

            objectrepository.save(function(err, objectrepositoryUpdated) {
              if (err) {
                res.status(500).json(err);
              } else {
                res.status(204).json(objectrepositoryUpdated);
              }
            });
          });
        } else {
          if (docs.length) {
            res.status(409).json(err);
            console.log("Duplicate entry");
          }
        }
      } else {
        var objectrepositoryId = req.params.repositoryId;
        ObjectRepository.findById(objectrepositoryId).exec(function(
          err,
          objectrepository
        ) {
          if (err) {
            console.log("Error finding repository");
            res.status(500).json(err);
            return;
          } else if (!objectrepository) {
            res.status(404).lson({
              message: "Object Repository Id not found " + objectrepositoryId
            });
            return;
          }
          var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss tt");
          objectrepository.application_name = req.body.application_name;
          objectrepository.application_version = req.body.application_version;
          objectrepository.environment_name = req.body.environment_name;
          objectrepository.tool_name = req.body.tool_name;
          objectrepository.tool_version = req.body.tool_version;
          objectrepository.or_name = req.body.or_name;
          objectrepository.visibility = req.body.visibility;
          objectrepository.updated_on = myDate;
          objectrepository.updated_by = req.body.updated_by;

          objectrepository.save(function(err, objectrepositoryUpdated) {
            if (err) {
              console.log("Error Save repository");
              res.status(500).json(err);
            } else {
              console.log("Save");
              res.status(204).json(objectrepositoryUpdated);
            }
          });
        });
      }
    }
  );
};

//delete one object repository
module.exports.deleteOneObjectRepository = function(req, res) {
  var objectrepositoryId = req.params.repositoryId;

  console.log("GET objectrepositoryId", objectrepositoryId);

  ObjectRepository.findByIdAndDelete(objectrepositoryId)
    .select("-objrep")
    .exec(function(err, objectrepository) {
      if (err) {
        console.log("Error finding objectrepository");
        res.status(500).json(err);
        return;
      } else if (!objectrepository) {
        console.log(
          "ObjectRepositoryId not found in database",
          objectrepositoryId
        );
        res.status(404).lson({
          message: "ObjectRepository ID not found " + objectrepositoryId
        });
        return;
      }
      objectrepository.save(function(err, objectrepositoryDelete) {
        if (err) {
          res.status(500).json(err);
        } else {
          res.status(204).json();
          console.log("objectrepository Deleted Successfully");
        }
      });
    });
};
