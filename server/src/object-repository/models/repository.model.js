const mongoose = require("mongoose");
const Schema = mongoose.Schema;
let repository = new Schema(
  {
    repository_id: {
      type: String,
      unique: [true, "rep Id should be unique"],
      required: [true, "rep Id required"]
      // default: '15'
    },
    or_name: {
      type: String,
      required: [true, "OR Name required"],
      minlength: 2,
      maxlength: 100
    },
    visibility: {
      type: String,
      //required: [true, 'visibility required'],
      default: "public",
      minlength: 2,
      maxlength: 100
    },

    application_name: {
      type: String,
      required: [true, "Application Name required"]
    },
    application_version: {
      type: String,
      required: [true, "Application Version required"]
    },
    environment_name: {
      type: String,
      required: [true, "Environment required"]
    },
    tool_name: {
      type: String,
      required: [true, "Tool Name required"]
    },
    tool_version: {
      type: String,
      required: [true, "Application Version required"]
    },
    status: {
      type: String,
      required: [true, "Status required"],
      default: "new",
      minlength: 2,
      maxlength: 100
    },
    created_by: {
      type: String,
      default: "admin",
      minlength: 2,
      maxlength: 100
    },
    created_on: {
      type: String,
    },
    updated_by: {
      type: String,
      default: "admin",
      minlength: 2,
      maxlength: 100
    },
    updated_on: {
      type: String,
    }
  },
  {
    collection: "repository"
  }
);

module.exports = mongoose.model("Repository", repository);
