var mongoose = require("mongoose");
var dateFormat = require("dateformat");
require("../models/applications.model");
var Application = mongoose.model("Application");
var autoIncrement = require("mongodb-autoincrement");

const db = mongoose.connection;
var collectionName = "applications";

module.exports.addApplication = async (req, res) => {
  Application.find(
    {
      $and: [
        { application_name: req.body.application_name },
        { application_version: req.body.application_version },
        { environment: req.body.environment }
      ]
    },
    function(err, docs) {
      if (docs.length) {
        res.status(409).json(err);
        console.log("Duplicate entry");
      } else {
        var application_name = req.body.application_name;
        var application_version = req.body.application_version;
        var api_technology = req.body.api_technology;
        var techstack = req.body.techstack;
        var environment = req.body.environment;
        var developed_by = req.body.developed_by;
        var implemented_by = req.body.implemented_by;
        var visibility = req.body.visibility;
        var status = req.body.status;
        var User_id=req.body.User_id;
        var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
        var created_on = myDate;
        var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
        var updated_on = myDate;
        var created_by = req.body.User_id;
        var updated_by = req.body.User_id;

        autoIncrement.getNextSequence(db, collectionName, function(
          err,
          autoIndex
        ) {
          Application.create(
            {
              application_id: "Application" + autoIndex,
              application_name: application_name,
              application_version: application_version,
              api_technology: api_technology,
              techstack: techstack,
              environment: environment,
              developed_by: developed_by,
              implemented_by: implemented_by,
              visibility: visibility,
              status:status,
              created_on: created_on,
              updated_on: updated_on,
              created_by: created_by,
              updated_by: updated_by,
              User_id:User_id
            },
            function(err, application) {
              if (err) {
                res.status(400).json(err);
                //  autoIndex=autoIndex-1;
                console.log(err);
              } else {
                res.status(201).json(application);
                console.log(application);
              }
            }
          );
        });
      }
    }
  );
};

module.exports.getAllApplication = (req, res) => {
  Application.find(function(err, application) {
    if (err) {
      res.status(404).json({ message: err });
    } else {
      res.status(200).json(application);
    }
  });
};

module.exports.applicationsGetOne = (req, res) => {
  var applicationsId = req.params.applicationsId;
  Application.findById(applicationsId).exec(function(err, doc) {
    var response = {
      status: 200,
      message: doc
    };
    if (err) {
      response.status = 500;
      response.message = err;
    } else if (!doc) {
      response.status = 404;
      response.message = {
        message: "Application Id not found " + id
      };
    }
    res.status(response.status).json(response.message);
  });
};

module.exports.applicationsUpdatebyStatus = (req, res) => {
  var applicationsId = "";
  if (req.params) {
    applicationsId = req.params.applicationsId;
  } else if(req) {
    applicationsId = req;
  }
  console.log("applicationsUpdatebyStatus::::",applicationsId);
  Application.findById(applicationsId).exec(function (err, doc) {
     if (doc.status === "new") {
       doc.status = "active";
     }else if (doc.status === "active") {
       doc.status = "deactive";
     }else if (doc.status === "deactive" || doc.status === "invisible") {
       doc.status = "active";
     }
     doc.save(function (err, applicationsupdated) {
       console.log(applicationsupdated)
     });
     console.log('doc status after active', doc.status);
   });
 };
 

 module.exports.appUpdatebyStatusRepo = (req, res) => {
  var applicationsId = req;
  console.log("applicationsUpdatebyStatus::::",applicationsId);
  Application.findById(applicationsId).exec(function (err, doc) {
     if (doc.status === "new") {
       doc.status = "active";
     }
     doc.save(function (err, applicationsupdated) {
       console.log(applicationsupdated)
     });
   });
 };

module.exports.applicationsUpdateOne = (req, res) => {
  Application.find(
    {
      $and: [
        { application_name: req.body.application_name },
        { application_version: req.body.application_version },
        { environment: req.body.environment }
      ]
    },
    function(err, docs) {
      if (docs.length > 0) {
        dupId = docs[0]._id;
        if (dupId == req.params.applicationsId) {
          var applicationsId = req.params.applicationsId;
          Application.findById(applicationsId).exec(function(err, application) {
            if (err) {
              res.status(500).json(err);
              return;
            } else if (!application) {
              res.status(404).lson({
                message: "Application Id not found " + applicationsId
              });
              return;
            }
            var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
            application.application_name = req.body.application_name;
            application.application_version = req.body.application_version;
            application.api_technology = req.body.api_technology;
            application.techstack = req.body.techstack;
            application.environment = req.body.environment;
            application.developed_by = req.body.developed_by;
            application.implementedby = req.body.implementedby;
            application.visibility = req.body.visibility;
           // application.User_id = req.body.User_id;
            application.updated_on = myDate;
            application.updated_by = req.body.User_id;

            application.save(function(err, applicationUpdated) {
              if (err) {
                res.status(500).json(err);
              } else {
                res.status(204).json(applicationUpdated);
              }
            });
          });
        } else {
          if (docs.length) {
            res.status(409).json(err);
            console.log("Duplicate entry");
          }
        }
      } else {
        var applicationsId = req.params.applicationsId;
        Application.findById(applicationsId).exec(function(err, application) {
          if (err) {
            res.status(500).json(err);
            return;
          } else if (!application) {
            res.status(404).lson({
              message: "Application Id not found " + applicationsId
            });
            return;
          }
          var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
          application.application_name = req.body.application_name;
          application.application_version = req.body.application_version;
          application.api_technology = req.body.api_technology;
          application.techstack = req.body.techstack;
          application.environment = req.body.environment;
          application.developed_by = req.body.developed_by;
          application.implementedby = req.body.implementedby;
          application.visibility = req.body.visibility;
        //  application.User_id = req.body.User_id;
          application.updated_on = myDate;
          application.updated_by = req.body.User_id;

          application.save(function(err, applicationUpdated) {
            if (err) {
              res.status(500).json(err);
            } else {
              res.status(204).json(applicationUpdated);
            }
          });
        });
      }
    }
  );
};

module.exports.applicationsDeleteOne = (req, res) => {
  var applicationsId = req.params.applicationsId;
  Application.findByIdAndRemove(applicationsId).exec(function(
    err,
    application
  ) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(204).json();
    }
  });
};

module.exports.applicationsByFilter = (req, res) => {
  var filter = { application_name: req.query.appName };
  console.log("filter1111 : ", req.query.appName);
  if (req.query.appVersion != "undefined")
    filter.application_version = req.query.appVersion;
  filter = { api_technology: "Phython" };
  console.log("filter : ", filter);

  Application.findById("5d5d0ba2962e4f4160814a6b").exec(function(err, apps) {
    if (err) throw err;

    console.log("values", apps);
  });

  Application.find(
    { $and: [{ techstack: "Jython" }, { environment: "Eclipse" }] },
    function(err, doc) {
      console.log("doccccc : ", doc);
      var response = {
        status: 200,
        message: doc
      };
      if (err) {
        response.status = 500;
        response.message = err;
        console.error("ERR : ", err);
      } else if (!doc) {
        response.status = 404;
        response.message = {
          message: "Application Id not found " + id
        };
      }
      res.status(response.status).json(response.message);
    }
  );
};

module.exports.recentApplication = (req, res) => {
  Application.find(function(err, application) {
    if (err) {
      console.log(err);
    } else {
      res.json(application);
    }
  })
    .sort({ $natural: -1 })
    .limit(3);
};

module.exports.filterApplication = (req, res) => {
  console.log("filter Application");
  var userId = req.params.userId;
  console.log("selected userId: ", userId);
 

  Application.find({ User_id: userId }, function(err, doc) {
    console.log("Filter Application : ", doc);
    var response = {
      status: 200,
      message: doc
    };
    if (err) {
      response.status = 500;
      response.message = err;
      console.error("ERR : ", err);
    } else if (!doc) {
      response.status = 404;
      response.message = {
        message: "User Id not found " + id
      };
    }
    res.status(response.status).json(response.message);
  });
};
