var mongoose = require("mongoose");
var dateFormat = require("dateformat");
console.log("working");
require("../models/projects.model");
var Project = mongoose.model("Project");
var autoIncrement = require("mongodb-autoincrement");

const db = mongoose.connection;
var collectionName = "projects";

module.exports.addProject = async (req, res) => {
  var project_name = req.body.project_name;
  var Project_Desc = req.body.Project_Desc;
  var visibility = req.body.visibility;
  var user = req.body.user;
  var userscount = user.length;
  var User_id = req.body.User_id;
  var status = req.body.status;
  var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
  var created_on = myDate;
  var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
  var updated_on = myDate;
  var created_by = req.body.User_id;
  var updated_by = req.body.User_id;
  var project_admin = req.body.project_admin;

  autoIncrement.getNextSequence(db, collectionName, function(err, autoIndex) {
    Project.create(
      {
        project_id: "project" + autoIndex,
        project_name: project_name,
        Project_Desc: Project_Desc,
        project_admin: project_admin,
        visibility: visibility,
        user: user,
        User_id:User_id,
        userscount: userscount,
        status:status,
        created_on: created_on,
        updated_on: updated_on,
        created_by: created_by,
        updated_by: updated_by
      },
      function(err, project) {
        if (err) {
          res.status(400).json(err);
        } else {
          res.status(201).json(project);
          console.log("Successfully posted");
          console.log(project);
        }
      }
    );
  });
};

module.exports.getAllProject = (req, res) => {
  Project.find(function(err, project) {
    if (err) {
      console.log(err);
    } else {
      res.json(project);
    }
  });
};

module.exports.projectsGetOne = (req, res) => {
  console.log("GET PROJECT");
  var projectsId = req.params.projectsId;
  Project.findById(projectsId).exec(function(err, doc) {
    var response = {
      status: 200,
      message: doc
    };
    if (err) {
      response.status = 500;
      response.message = err;
    } else if (!doc) {
      response.status = 404;
      response.message = {
        message: "ProjectId not found " + id
      };
    }
    res.status(response.status).json(response.message);
  });
};

module.exports.projectsUpdateOne = (req, res) => {
  var projectsId = req.params.projectsId;
  console.log(projectsId);
  Project.findById(projectsId).exec(function(err, project) {
    if (err) {
      res.status(500).json(err);
      return;
    } else if (!project) {
      res.status(404).lson({
        message: "projectId not found " + projectsId
      });

      return;
    }

    var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
    project.project_name = req.body.project_name;
    project.Project_Desc = req.body.Project_Desc;
    project.visibility = req.body.visibility;
    project.user = req.body.user;
    project.userscount = project.user.length;
  //  project.User_id = req.body.User_id;
    project.updated_on = myDate;
    project.updated_by = req.body.User_id;

    project.save(function(err, projectUpdated) {
      if (err) {
        res.status(500).json(err);
      } else {
        console.log("Project Updated", projectUpdated);
        res.status(204).json();
      }
    });
  });
};

module.exports.projectDeleteOne = (req, res) => {
  var projectsId = req.params.projectsId;
  Project.findByIdAndRemove(projectsId).exec(function(err, project) {
    if (err) {
      res.status(404).json(err);
    } else {
      res.status(204).json();
    }
  });
};

module.exports.recentProject = (req, res) => {
  Project.find(function(err, project) {
    if (err) {
      console.log(err);
    } else {
      res.json(project);
    }
  })
    .sort({ $natural: -1 })
    .limit(3);
};

module.exports.filterProject = (req, res) => {
  console.log("filter Project");
  var userId = req.params.userId;
  console.log("selected userId: ", userId);

  Project.find({ User_id: userId }, function(err, doc) {
    console.log("Filter Project : ", doc);
    var response = {
      status: 200,
      message: doc
    };
    if (err) {
      response.status = 500;
      response.message = err;
      console.error("ERR : ", err);
    } else if (!doc) {
      response.status = 404;
      response.message = {
        message: "User Id not found " + id
      };
    }
    res.status(response.status).json(response.message);
  });
};

module.exports.projectsUpdatebyStatus = (req, res) => {
  console.log('inside ststus update')
  var projectsId = req.params.projectsId;
  Project.findById(projectsId).exec(function (err, doc) {
     if (doc.status === "new") {
       doc.status = "active";
     }else if (doc.status === "active") {
       doc.status = "deactive";
     }else if (doc.status === "deactive" || doc.status === "invisible") {
       doc.status = "active";
     }
     doc.save(function (err, projectsupdated) {
       console.log(projectsupdated)
     });
     console.log('doc status after active', doc.status);
   });
 };
 