var mongoose = require("mongoose");
var dateFormat = require("dateformat");
require("../models/tools.model");
var Tool = mongoose.model("Tool");
var autoIncrement = require("mongodb-autoincrement");

const db = mongoose.connection;
var collectionName = "tools";

module.exports.addTool = async (req, res) => {
  console.log(req.body.tool_name);
  Tool.find(
    {
      $and: [
        { tool_name: req.body.tool_name },
        { tool_version: req.body.tool_version }
      ]
    },
    function(err, docs) {
      if (docs.length) {
        res.status(409).json(err);
        console.log("Duplicate entry");
      } else {
        var tool_name = req.body.tool_name;
        var tool_version = req.body.tool_version;
        var technology = req.body.technology;
        var window_objects = req.body.window_objects;
        var screen_objects = req.body.screen_objects;
        var field_objects = req.body.field_objects;
        var visibility = req.body.visibility;
        var status = req.body.status;
        var User_id = req.body.User_id;
        var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
        var created_on = myDate;
        var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
        var updated_on = myDate;
        var created_by = req.body.User_id;
        var updated_by = req.body.User_id;

        autoIncrement.getNextSequence(db, collectionName, function(
          err,
          autoIndex
        ) {
          Tool.create(
            {
              tool_id: "Tool" + autoIndex,
              tool_name: tool_name,
              tool_version: tool_version,
              technology: technology,
              window_objects: window_objects,
              screen_objects: screen_objects,
              field_objects: field_objects,
              visibility: visibility,
              status:status,
              User_id:User_id,
              created_on: created_on,
              updated_on: updated_on,  
              created_by: created_by,
              updated_by: updated_by
            },
            function(err, tool) {
              if (err) {
                res.status(400).json(err);
                console.log(err);
              } else {
                res.status(201).json(tool);
                console.log("Success");
                console.log(tool);
              }
            }
          );
        });
      }
    }
  );
};

module.exports.getAllTool = (req, res) => {
  Tool.find({}, function(err, data) {
    if (err) {
      console.log(err);
    } else {
      res.json(data);
    }
  });
};

module.exports.toolGetOne = (req, res) => {
  var toolId = req.params.toolId;
  console.log("GET toolId", toolId);
  Tool.findById(toolId).exec(function(err, doc) {
    var response = {
      status: 200,
      message: doc
    };
    if (err) {
      console.log("Error finding tool");
      response.status = 500;
      response.message = err;
    } else if (!doc) {
      console.log("ToolId not found in database", id);
      response.status = 404;
      response.message = {
        message: "ToolId not found " + id
      };
    }
    res.status(response.status).json(response.message);
  });
};

module.exports.toolsUpdatebyStatus = (req, res) => {
  var toolId = "";
  if (req.params) {
    toolId = req.params.toolId;
  } else if(req){
    toolId = req;
  }
   Tool.findById(toolId).exec(function (err, doc) {
     if (doc.status === "new") {
       doc.status = "active";
     }else if (doc.status === "active") {
       doc.status = "deactive";
     }else if (doc.status === "deactive" || doc.status === "invisible") {
       doc.status = "active";
     }
     doc.save(function (err, toolsupdated) {
       console.log(toolsupdated)
     });
     console.log('doc status after active', doc.status);
   });
 };
 

 module.exports.toolUpdatebyStatusRepo = (req, res) => {
  var toolId = req;

   Tool.findById(toolId).exec(function (err, doc) {
     if (doc.status === "new") {
       doc.status = "active";
     }
     doc.save(function (err, toolsupdated) {
       console.log(toolsupdated)
     });
    
   });
 };
 
module.exports.toolsUpdateOne = (req, res) => {
  Tool.find(
    {
      $and: [
        { tool_name: req.body.tool_name },
        { tool_version: req.body.tool_version }
      ]
    },
    function(err, docs) {
      if (docs.length > 0) {
        dupId = docs[0]._id;
        if (dupId == req.params.toolId) {
          console.log(dupId);
          console.log(req.params.toolId);
          var toolId = req.params.toolId;
          Tool.findById(toolId).exec(function(err, tool) {
            if (err) {
              res.status(500).json(err);
              return;
            } else if (!tool) {
              res.status(404).lson({
                message: "ToolId not found " + toolId
              });
              return;
            }
            var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
            tool.tool_name = req.body.tool_name;
            tool.tool_version = req.body.tool_version;
            tool.technology = req.body.technology;
            tool.window_objects = req.body.window_objects;
            tool.screen_objects = req.body.screen_objects;
            tool.field_objects = req.body.field_objects;
            tool.visibility = req.body.visibility;
            tool.updated_on = myDate;
            tool.updated_by = req.body.User_id;

            tool.save(function(err, toolUpdated) {
              if (err) {
                console.log("Error Save tool");
                res.status(500).json(err);
              } else {
                console.log("Save");
                res.status(204).json(toolUpdated);
              }
            });
          });
        } else {
          if (docs.length) {
            res.status(409).json(err);
            console.log("Duplicate entry");
          }
        }
      } else {
        var toolId = req.params.toolId;
        Tool.findById(toolId).exec(function(err, tool) {
          if (err) {
            res.status(500).json(err);
            return;
          } else if (!tool) {
            res.status(404).lson({
              message: "ToolId not found " + toolId
            });
            return;
          }
          var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
          tool.tool_name = req.body.tool_name;
          tool.tool_version = req.body.tool_version;
          tool.technology = req.body.technology;
          tool.window_objects = req.body.window_objects;
          tool.screen_objects = req.body.screen_objects;
          tool.field_objects = req.body.field_objects;
          tool.visibility = req.body.visibility;
          //tool.User_id = req.body.User_id;
          tool.updated_on = myDate;
          tool.updated_by = req.body.User_id;

          tool.save(function(err, toolUpdated) {
            if (err) {
              console.log("Error Save tool");
              res.status(500).json(err);
            } else {
              console.log("Save");
              res.status(204).json(toolUpdated);
            }
          });
        });
      }
    }
  );
};

module.exports.toolDeleteOne = (req, res) => {
  console.log("Inside tool Delete");
  var toolId = req.params.toolId;
  Tool.findByIdAndRemove(toolId).exec(function(err, tool) {
    if (err) {
      res.status(404).json(err);
    } else {
      console.log("Tool deleted, id:", toolId);
      res.status(204).json();
    }
  });
};

module.exports.recentTool = (req, res) => {
  Tool.find(function(err, tool) {
    if (err) {
      console.log(err);
    } else {
      res.json(tool);
    }
  })
    .sort({ $natural: -1 })
    .limit(3);
};

module.exports.filterTool = (req, res) => {
  console.log("filter Tool");
  var userId = req.params.userId;
  console.log("selected userId: ", userId);

  Tool.find({ User_id: userId }, function(err, doc) {
    console.log("Filter Tool : ", doc);
    var response = {
      status: 200,
      message: doc
    };
    if (err) {
      response.status = 500;
      response.message = err;
      console.error("ERR : ", err);
    } else if (!doc) {
      response.status = 404;
      response.message = {
        message: "User Id not found " + id
      };
    }
    res.status(response.status).json(response.message);
  });
};

module.exports.toolsUpdateStatus = (req, res) => { 
  console.log('inside ststus update')
  var toolId = req.params.toolId;
   Tool.findById(toolId).exec(function (err, doc) {
     if (doc.status === "new" || doc.status === "New") {
       doc.status = "active";
     }else if (doc.status === "active") {
       doc.status = "deactive";
     }else if (doc.status === "inactive" || doc.status === "inactive") {
       doc.status = "active";
     }
     doc.save(function (err, toolsupdated) {
       console.log(toolsupdated)
     });
     console.log('doc status after active', doc.status);
   });
 };
 