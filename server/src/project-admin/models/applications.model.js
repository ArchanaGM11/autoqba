const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let applicationSchema = new Schema({
  application_id: {
    type: String,
    unique: [true, 'Application Id should be unique'],
    required: [true, 'Application Id required'],
  },
  application_name: {
    type: String,
    required: [true, 'Application Name required'],
    minlength: 2,
    maxlength: 100
  },
  application_version: {
    type: String,
    required: [true, 'Application version required'],
    minlength: 1,
    maxlength: 100
  },
  api_technology: {
    type: String,
    required: [true, 'UI/API technology required'],
    minlength: 2,
    maxlength: 100
  },
  techstack: {
    type: String,
    required: [true, 'Tech Stack required'],
    minlength: 2,
    maxlength: 100
  },
  environment: {
    type: String,
    required: [true, 'Environment required'],
    minlength: 2,
    maxlength: 100
  },
  developed_by: {
    type: String,
    required: [true, 'developed_by required'],
    minlength: 2,
    maxlength: 100
  },
  implemented_by: {
    type: String,
    required: [true, 'implemented_by required'],
    minlength: 2,
    maxlength: 100
  },
  visibility: {
    type: String,
    required: [true, 'visibility required'],
    minlength: 2,
    maxlength: 100
  },
  status: {
    type: String,
  //  required: [true, 'Status required'],
   // default: 'active',
    minlength: 2,
    maxlength: 100
  },
  created_by: {
    type: String,
   // required: [true, "created_by required required"],
    minlength: 2,
    maxlength: 100
  },
  created_on: {
    type: String,
   // type:Date
   // "default": Date
  },
  updated_by: {
    type: String,
   // required: [true, "updated_by required required"],
    minlength: 2,
    maxlength: 100
  },
  updated_on: {
    type: String,
   // "default": Date
  },
  User_id: {
    type: String,
    unique: [true, 'User Id should be unique'],
    //required: [true, 'User Id required'],
  },
}, {
    collection: 'applications'
  });

module.exports = mongoose.model('Application', applicationSchema);
