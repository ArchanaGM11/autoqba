const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/*let userSchema = new Schema({

    user_id: {
        type: String,
        unique: [true, "User Id should be unique"],
        required: [true, "User Id required"]
    },
    user_name: {
        type: String,
        required: [true, "User Name required"],
        minlength: 4,
        maxlength: 20
    },
    role: {
        type: String,
        required: [true, "Role required"],
        minlength: 4,
        maxlength: 20
    },
});*/

let projectSchema = new Schema({
    project_id: {
        type: String,
        unique: [true, "Project Id should be unique"],
        required: [true, "Project Id required"]
    },
    project_name: {
        type: String,
        unique: [true, "Project Name should be unique"],
        //required: [true, "Project Name required"],
        minlength: 2,
        maxlength: 100
    },
    Project_Desc: {
        type: String,
        //required: [true, "Description required"],
        minlength: 2,
        maxlength: 1000
    },
    visibility: {
        type: String,
        default: 'private',
        //required: [true, "Visibility required"],
        minlength: 2,
        maxlength: 100
    },
    status: {
        type: String,
        //default: 'active',
        // required: [true, "Status required"],
        minlength: 2,
        maxlength: 100
    },
    project_admin: {
        type: String,
       // default: 'Azhagu',
        //  required: [true, "Project Admin required"],
        minlength: 2,
        maxlength: 100
    },
    user: {
        type: Array,
        required: [true, "User required"]
    },
    userscount: {
        type: Number
    },
    created_by: {
        type: String,
      //  required: [true, "created_by required required"],
        minlength: 2,
        maxlength: 100
    },
    created_on: {
        type: String,
       // type:Date
       // "default": Date
      },
      updated_by: {
        type: String,
       // required: [true, "updated_by required required"],
        minlength: 2,
        maxlength: 100
      },
      updated_on: {
        type: String,
       // "default": Date
      },
      User_id: {
        type: String,
        unique: [true, 'User Id should be unique'],
        //required: [true, 'User Id required'],
      },
}, {
    collection: 'projects'
});

module.exports = mongoose.model("Project", projectSchema);
