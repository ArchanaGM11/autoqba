const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let toolSchema = new Schema({
  tool_id: {
    type: String,
    unique: [true, 'Tool Id should be unique'],
    required: [true, 'Tool Id required']
  },
  tool_name: {
    type: String,
    required: [true, 'Tool Name required'],
    minlength: 2,
    maxlength: 100
  },
  tool_version: {
    type: String,
    required: [true, 'Tool Version required'],
    minlength: 1,
    maxlength: 100
  },
  technology: {
    type: String,
    required: [true, 'Technology required'],
    minlength: 2,
    maxlength: 100
  },
  window_objects: {
    type: String,
    required: [true, 'Windowobjects required'],
    minlength: 2,
    maxlength: 100
  },
  screen_objects: {
    type: String,
    required: [true, 'Screenobjects required'],
    minlength: 2,
    maxlength: 100
  },
  field_objects: {
    type: String,
    required: [true, 'Fieldobjects required'],
    minlength: 2,
    maxlength: 100
  },
  visibility: {
    type: String,
    required: [true, 'visibility required'],
    minlength: 2,
    maxlength: 100
  },
  status: {
    type: String,
   // required: [true, 'Status required'],
   // default: 'active',
    minlength: 2,
    maxlength: 100
  },
  created_by: {
    type: String,
  //  required: [true, "created_by required required"],
    minlength: 2,
    maxlength: 100
  },
  created_on: {
    type: String,
   // type:Date
   // "default": Date
  },
  updated_by: {
    type: String,
 //   required: [true, "updated_by required required"],
    minlength: 2,
    maxlength: 100
  },
  updated_on: {
    type: String,
   // "default": Date
  },
  User_id: {
    type: String,
    unique: [true, 'User Id should be unique'],
    //required: [true, 'User Id required'],
  },
}, {
    collection: 'tools'
  });

module.exports = mongoose.model('Tool', toolSchema);