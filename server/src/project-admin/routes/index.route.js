var express = require('express');
var router = express.Router();

var applicationController = require('../controllers/application.controller');
var toolController = require('../controllers/tool.controller');
var projectController = require('../controllers/project.controller');

// Application routes
router
  .route('/applications')
  .post(applicationController.addApplication)
  .get(applicationController.getAllApplication)

  router
.route('/recentapplications')
  .get(applicationController.recentApplication)

router
  .route('/applications/:applicationsId')
  .get(applicationController.applicationsGetOne)
  .put(applicationController.applicationsUpdateOne)
  .delete(applicationController.applicationsDeleteOne)

  router
  .route('/applications/user/:userId')
  .get(applicationController.filterApplication)

  router 
  .route('/applications/status/:applicationsId')
  .put(applicationController.applicationsUpdatebyStatus)

  // Application filter
  router
  .route('/applications/filter')
  .get(applicationController.applicationsByFilter)

// Tool routes
router
  .route('/tools')
  .post(toolController.addTool)
  .get(toolController.getAllTool)

  router
  .route('/recenttools')
  .get(toolController.recentTool)

router
  .route('/tools/:toolId')
  .get(toolController.toolGetOne)
  .put(toolController.toolsUpdateOne)
  .delete(toolController.toolDeleteOne);

  router
  .route('/tools/user/:userId')
  .get(toolController.filterTool)

  router 
  .route('/tools/status/:toolId')
  .put(toolController.toolsUpdatebyStatus)

// Project routes
router
  .route('/projects')
  .post(projectController.addProject)
  .get(projectController.getAllProject)

router
.route('/recentprojects')
  .get(projectController.recentProject)

router
  .route('/projects/:projectsId')
  .get(projectController.projectsGetOne)
  .put(projectController.projectsUpdateOne)
  .delete(projectController.projectDeleteOne)

   router
   .route('/projects/user/:userId')
   .get(projectController.filterProject)

   router 
  .route('/projects/status/:projectsId')
  .put(projectController.projectsUpdatebyStatus)

module.exports = router;