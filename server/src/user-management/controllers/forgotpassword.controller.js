var mongoose = require('mongoose');
require('../models/user.model');

const Pswd = require('../models/user.model')
const nodemailer = require("nodemailer");
var generator = require('generate-password');
const bcrypt = require('bcrypt');
const saltRounds = 8;

const resetEmailBody = 'Your password has been reset based on your request, Please use the following Credentials for logging in.';

/*Forgot Password and Email Triggering module*/
module.exports.resetPassword = (function (req, resp) {
    console.log(req.body.Email);
    console.log("Inside function");
    Pswd.findOne({
        Email: req.body.Email

    }).select()
        .exec(function (err, pass) {
            if (err) {
                resp.json({ success: false, message: err });
            }
            else {
                if (!req.body.Email) {
                    resp.json({ success: false, message: 'No Email was provided' });
                }
                else {
                    if (!pass) {
                        resp.json({
                                "status": "FAILED",
                                "messageCode": "ERR414",
                                "message": "Email was not found",
                                "userMessage": "Please enter your registered Email Id"
                            });

                    } else {
                        console.log('Email validation success');
                        /* Random-Password  Generation Module*/
                        var password = generator.generate({
                            length: 8,
                            numbers: true
                        });
                        console.log(password);
                        bcrypt.hash(password, saltRounds, function (err, hash) {
                            // Store hash in your password DB.
                            console.log("Encrypted Password " + hash);
                            pass.Password = hash;
                            Username = pass.UserName;
                            /* Password Updation in DB */
                            pass.save(function (err) {
                                if (err) {
                                    console.log('Server Error');
                                }
                                else {
                                    console.log('Updated Successfully!!!');
                                    /* Email Triggering module */
                                    var transporter = nodemailer.createTransport({
                                        host: "smtp.gmail.com",
                                        port: 587,
                                        secure: false,
                                        auth: {
                                            user: 'testmailAutoQBA@gmail.com',
                                            pass: '123@@456'
                                        }
                                    });
                                    console.log('SMTP Configured');
                                    var mailOptions = {
                                        from: 'testmailAutoQBA@gmail.com',
                                        to: req.body.Email,
                                        subject: 'Your Password has been changed',
                                        text: `Dear ` + Username + `,` +
                                        resetEmailBody + 
                                            `
                                                
                                                    UserName : `+ req.body.Email +
                                            `
                                                    Password  : ` + password +
                                                    
                                                    'Note: This is an auto-generated message, please do not reply. Please contact the site admin should you have any concerns.' 
                                    }
                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            return console.log(error);
                                        } else {
                                            console.log('Email Sent:  ' + info.response);
                                        }
                                    });
                                }
                            });
                        });

                        resp.json({
                                "status": "SUCCESS",
                                "messageCode": "MSG200",
                                "message": "Password changed Successfully",
                                "userMessage": "Your Password has been changed, Please check your Email."
                            });
                    }
                }
            }
        });
});
