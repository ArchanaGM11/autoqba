var mongoose = require('mongoose');
require('../models/privilege.model');
var Privilege = mongoose.model('privileges');
var autoIncrement = require('mongodb-autoincrement');
const db = mongoose.connection
var collectionName = "privileges";

module.exports.newPrivilege = async (req, resp) => {

    autoIncrement.getNextSequence(db, collectionName, async function (err, autoIndex) {

    let privilege = new Privilege({
        Privilege_id: 'Privilege ' +autoIndex,
        Privilege: req.body.Privilege,
        PrivilegeType: req.body.PrivilegeType,
        PrivilegeDescription: req.body.PrivilegeDescription,
        Status: req.body.Status,
        Created_on: req.body.Created_on,
        Updated_on: req.body.Updated_on,
        Created_by: req.body.Created_by,
        Updated_by: req.body.Updated_by
    })

    try {
        let newPrivilege = await privilege.save()
        resp.status(200).json(newPrivilege)
        console.log('save');
    }
    catch (err) {
        console.log(err);
        resp.status(400).json({ message: err.message })
    }
        });
};

module.exports.getOnePrivilege = (req, resp) => {
    console.log(req.params.privilegeid);
    var privilegeid=req.params.privilegeid;
    Privilege.findById(privilegeid)
    .exec(function (err, msg) {
     var response = {
       status: 200,
       message: msg
     };
     if (err) {
       response.status = 500;
       response.message = err;
     } else if (!msg) {
       response.status = 404;
       response.message = {
         "message": "privilegeid not found " + privilegeid
       };
     }
     resp
       .status(response.status)
       .json(response.message);
   });

};

module.exports.getAllPrivileges = (req, resp) => {
    console.log("PrivilegeList")
    Privilege.find()
        .then(privileges => resp.json(privileges))
        .catch(err => resp.status(400).json('Error:' + err));
};


module.exports.updatePrivilege = (req, resp) => {
    console.log("Update privilege");
    var privilegeid = req.params.privilegeid;

    console.log('GET privilegeid', privilegeid);

    Privilege
        .findById(privilegeid)
        .exec(function (err, privilege) {
            if (err) {
                console.log("Error finding privilege");
                resp
                    .status(500)
                    .json(err);
                return;
            }
            else if (!privilege) {
                console.log("privilege Id not found in database", privilegeid);
                resp
                    .status(404)
                    .json({
                        "message": "privilege Id not found " + privilegeid
                    });
                return;
            }
                console.log("In Update");
                console.log(privilege.PrivilegeName);
                    privilege.PrivilegeName= req.body.PrivilegeName,
                    privilege.PrivilegeType= req.body.PrivilegeType,
                    privilege.PrivilegeDescription= req.body.PrivilegeDescription,
                    privilege.Status= req.body.Status,
                    privilege.Created_by= req.body.Created_by,
                    privilege.Updated_by= req.body.Updated_by,
                    

                privilege.save(function (err, privilegeUpdated) {
                    if (err) {
                        resp
                            .status(500)
                            .json(err);
                    }
                    else {
                        resp
                            .status(200)
                            .json(privilegeUpdated);
                    }
                });
        });
};

module.exports.deletePrivilege = (req, resp) => {
    console.log("Delete Privilege");
    var privilegeid = req.params.privilegeid;

    Privilege
        .findByIdAndRemove(privilegeid)

        .exec(function (err, privilege) {
            if (err) {
                resp
                    .status(404)
                    .json(err);
            }
            else {
                console.log("Privilege deleted, id:", privilegeid);
                resp
                    .status(204)
                    .json();
            }
        })
};

module.exports.privilegesUpdateStatus = (req, res) => { 
    console.log('inside status update')
    var privilegeid = req.params.privilegeid;
    Privilege.findById(privilegeid).exec(function (err, doc) {
       if (doc.status === "new" || doc.status === "New") {
         doc.status = "active";
       }else if (doc.status === "active") {
         doc.status = "deactive";
       }else if (doc.status === "inactive" || doc.status === "inactive") {
         doc.status = "active";
       }
       doc.save(function (err, Privilegesupdated) {
         console.log(Privilegesupdated)
       });
       console.log('doc status after active', doc.status);
     });
   };

