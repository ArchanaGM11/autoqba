var mongoose = require('mongoose');
require('../models/role.model');
var Role = mongoose.model('roles');
var autoIncrement = require('mongodb-autoincrement');
const db = mongoose.connection
var collectionName = "roles";

module.exports.newRole = async (req, resp) => {
   // console.log("%o",req.body);
    autoIncrement.getNextSequence(db, collectionName, async function (err, autoIndex) {
    console.log(req.body.RoleName, autoIndex);
    console.log('role controller working')
    let role = new Role({
        Role_id: 'Role ' +autoIndex,
        Role: req.body.Role,
        RoleType: req.body.RoleType,
        Privilege: req.body.Privilege,
        RoleDescription: req.body.RoleDescription,
        Status: req.body.Status,
        Created_on: req.body.Created_on,
        Updated_on: req.body.Updated_on,
        Created_by: req.body.Created_by,
        Updated_by: req.body.Updated_by
    })

    try {
        let newRole = await role.save();

         resp.json({
            "status": "SUCCESS",
            "messageCode": "MSG200",
            "message": "Roles created",
            "userMessage": "Roles Added Successfully !!!."
            });
        console.log('save');
    }
    catch (err) {
        console.log(104,err);
        resp.json({
            "status": "Failure",
            "messageCode": "MSG415",
            "message": "Enter valid Roles details ",
            "userMessage": "Invalid Details."
            });
    }
        });
};
        

module.exports.getOneRole = (req, resp) => {
    console.log(req.params.roleid);
    var roleid=req.params.roleid;
    Role.findById(roleid)
    .exec(function (err, msg) {
     var response = {
       status: 200,
       message: msg
     };
     if (err) {
       response.status = 500;
       response.message = err;
     } else if (!msg) {
       response.status = 404;
       response.message = {
         "message": "roleid not found " + roleid
       };
     }
     resp
       .status(response.status)
       .json(response.message);
   });

};

module.exports.getAllRoles = (req, resp) => {
    let criteria = {};
    if(req.query.RoleType === undefined) {
        criteria = {}
    } else if (req.query.RoleType === 'System') {
        criteria = {
            RoleType: 'System'
        }
    } else if (req.query.RoleType === 'Project') {
        criteria = {
            RoleType: 'Project'
        }
    }
    Role.find(criteria)
        .then(roles => resp.json(roles))
        .catch(err => resp.status(400).json('Error:' + err));
};


module.exports.updateRole = (req, resp) => {
    console.log("Update Role");
    var roleid = req.params.roleid;

    console.log('GET RoleId', roleid);
    console.log('%o',req.body);
    console.log(req.body.RoleName);

    Role
        .findById(roleid)
        .exec(function (err, role) {
            if (err) {
                console.log("Error finding role");
                resp
                    .status(500)
                    .json(err);
                return;
            }
            else if (!role) {
                console.log("Role Id not found in database", roleid);
                resp
                    .status(404)
                    .json({
                        "message": "Role Id not found " + roleid
                    });
                return;
            }
                console.log("In Update");
                console.log('%o',req.body);
                console.log('rolename',req.body.RoleName);
                    role.RoleName= req.body.RoleName,
                    role.RoleType= req.body.RoleType,
                    role.PrivilegeType=req.body.PrivilegeType,
                    role.RoleDescription= req.body.RoleDescription,
                    role.Status= req.body.Status,
                    role.Created_by= req.body.Created_by,
                    role.Updated_by= req.body.Updated_by,
                    

                    role.save(function (err, roleUpdated) {
                    if (err) {
                        resp
                            .status(500)
                            .json(err);
                    }
                    else {
                        resp
                            .status(200)
                            .json(roleUpdated);
                    }
                });
        });
};

module.exports.deleteRole = (req, resp) => {
    console.log("Delete role");
    var roleid = req.params.roleid;

    Role
        .findByIdAndRemove(roleid)

        .exec(function (err, role) {
            if (err) {
                resp
                    .status(404)
                    .json(err);
            }
            else {
                console.log("Role deleted:", roleid);
                resp
                    .status(204)
                    .json({
                        "message":"Role deleted"
                    });
            }
        })
};

module.exports.rolesUpdateStatus = (req, res) => { 
    console.log('inside status update')
    var roleid = req.params.roleid;
    Role.findById(roleid).exec(function (err, doc) {
       if (doc.status === "new" || doc.status === "New") {
         doc.status = "active";
       }else if (doc.status === "active") {
         doc.status = "deactive";
       }else if (doc.status === "inactive" || doc.status === "inactive") {
         doc.status = "active";
       }
       doc.save(function (err, rolesupdated) {
         console.log(rolesupdated)
       });
       console.log('doc status after active', doc.status);
     });
   };

