var mongoose = require('mongoose');
var dateFormat = require("dateformat");
require('../models/user.model');
require('../models/role.model');
require('../../project-admin/models/projects.model');
require('../../common/models/userprojectrole.model')
const Pswd = require('../models/user.model')
var User = mongoose.model('users');
var UserProjectRole = mongoose.model('userprojectrole');
var Role = mongoose.model('roles');
const Project = mongoose.model('Project')
const bcrypt = require('bcrypt');
const saltRounds = 8;
var autoIncrement = require('mongodb-autoincrement');
const jwt = require("jsonwebtoken");
var generator = require('generate-password');
const nodemailer = require("nodemailer");

const db = mongoose.connection
var collectionName = "users";





/*Add New User */

module.exports.newUser = async (req, resp) => {

    // var token = req.headers.authorization.toString().split(" ")[1];
    // console.log("Token : " + token)
        var password = generator.generate({
                length: 8,
                numbers: true
            });
            console.log(password);
    bcrypt.hash(password, saltRounds, async function (err, hash) {
    autoIncrement.getNextSequence(db, collectionName, async function (err, autoIndex) {


        var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
        var created_on = myDate;
        var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
        var updated_on = myDate;
        


    let userId = 'User ' +autoIndex;
    let user = new User({
        User_id: userId,
        FirstName: req.body.FirstName,
        LastName: req.body.LastName,
        UserName: req.body.UserName,
        Email: req.body.Email,
        ConfirmEmail: req.body.ConfirmEmail,
        Role: req.body.Role,
        CompanyName: req.body.CompanyName,
        Status: req.body.Status,
        Visibility: req.body.Visibility,
        project: req.body.project,
      //  myDate : dateFormat(new Date(), "dd-mm-yy h:MM:ss TT"),
        Created_at: created_on,
      //  myDate : dateFormat(new Date(), "dd-mm-yy h:MM:ss TT"),
        Updated_at: updated_on,
        Password: hash
    })

    try {
        let newUser = await user.save();
        let projects = req.body.project;
        for(let i=0;i<projects.length;i++) {
            console.log(projects[i])
            let project = new UserProjectRole({
                User_id: userId,
                project_id: projects[i].projectname,
                Role_id: projects[i].Role,
                Status: req.body.Status,
                Visibility: req.body.Visibility,
                project: req.body.project,
                Created_at: created_on,
                Updated_at: updated_on,
                Password: hash
            })
            await project.save();
        }
        //resp.status(200).json(newUser)

        resp.json({
            "status": "SUCCESS",
            "messageCode": "MSG200",
            "message": "User created",
            "userMessage": "User Added Successfully !!!."
            });
        console.log('save');
        sendEmail(req,resp,password);
    }
    catch (err) {
        console.log(err);
        //resp.status(400).json({ message: err.message })
        resp.json({
            "status": "Failure",
            "messageCode": "MSG415",
            "message": "Enter valid User details ",
            "userMessage": "Invalid Data."
            });
    }
        });
            });
        
}


   /**Global Function */     
function sendEmail(req,resp,password)
{
     /* Email Triggering module */                                 
     var transporter = nodemailer.createTransport({
        host:"smtp.gmail.com",
        port: 587,
        secure: false, 
        auth: {
          user: 'testmailAutoQBA@gmail.com', 
          pass: '123@@456' 
        }
        });
    console.log('SMTP Configured');
    var mailOptions = {
          from:'testmailAutoQBA@gmail.com',
          to: req.body.Email,
          subject:'Your Password has been changed',
          text: `Dear `+req.body.UserName+`,`+
          `
This is a confirmation that the password for your account has just been changed.
                
                    UserName : `+req.body.Email+ 
                    `
                    Password  : ` + password
      }
     transporter.sendMail(mailOptions, function(error,info){
          if(error){
             return console.log(error);
          }else{
              console.log('Email Sent:  '+ info.response);
          }
      });
}


/**
 * Get User by ID
 */
module.exports.getOneUser = (req, resp) => {
       console.log(req.params.userid);
       var userid=req.params.userid;
       User.findById(userid)
       .exec(function (err, msg) {
        var response = {
          status: 200,
          message: msg
        };
        if (err) {
          response.status = 500;
          response.message = err;
        } else if (!msg) {
          response.status = 404;
          response.message = {
            "message": "User Id not found " + userid
          };
        }
        resp
          .status(response.status)
          .json(response.message);
      });

};





/**
 * Get users list
 */
module.exports.getAllUsers = (req, resp) => {
    console.log("UserList")
    User.find()
        .then(users => resp.json(users))
        .catch(err => resp.status(400).json('Error:' + err));
};

module.exports.getUserProjectRole = (req, resp) => {
    console.log("Update User");
    var userid = req.params.userid;

    console.log('GET userId', userid);
    console.log('UserName from req',req.body);

    UserProjectRole.find({
        User_id: userid
    }).exec(function (err, data) {
        if (err) {
            console.log("Error finding User");
            resp
                .status(500)
                .json(err);
            return;
        } else if(!data){
            resp.json({
                "status": "SUCCESS",
                "messageCode": "MSG200",
                "message": "User Project Role data fetched",
                data
                });
        } else {
            for(let i=0;i<data.length;i++) {
                console.log(100,data[0].Role_id);
                Role.findById(data[0].Role_id) .exec(function (err, msg) {
                    console.log(101,msg);
                   
                    
                })
                
                let projectDetails = Project.findById(data[0].project_id).exec(function (err, msgs) {
                    console.log(104,msgs)
                })
                console.log(103,projectDetails);
            }
            resp.json({
                "status": "SUCCESS",
                "messageCode": "MSG200",
                "message": "User Project Role data fetched",
                data
                });
        }
    })
}

/**
 * Individual User update
 */
module.exports.updateUser = (req, resp) => {
    console.log("Update User");
    var userid = req.params.userid;

    console.log('GET userId', userid);
    console.log('UserName from req',req.body);

    User
        .findById(userid)
        .exec(function (err, user) {
            if (err) {
                console.log("Error finding User");
                resp
                    .status(500)
                    .json(err);
                return;
            }
            else if (!user) {
                console.log("User Id not found in database", userid);
                resp
                    .status(404)
                    .json({
                        "message": "User Id not found " + userid
                    });
                return;
            }
                console.log("In Update");
                console.log("%o",req.body);
              //  let myresp = req.body;
               console.log('FirstName',req.body.FirstName);
               var myDate = dateFormat(new Date(), "dd-mm-yy h:MM:ss TT");
                    user.FirstName= req.body.FirstName,
                    user.LastName= req.body.LastName,
                    user.UserName= req.body.UserName,
                    user.Email= req.body.Email,
                    user.CompanyName= req.body.CompanyName,
                    user.Role= req.body.Role,
                    user.Status= req.body.Status,
                    user.Visibility= req.body.Visibility,
                    user.updated_on = myDate;
                    Updated_at = myDate,

                    
                console.log('user',user);
                user.save(function (err, userUpdated) {
                    if (err) {
                        resp.json({
                            "status": "Failure",
                            "messageCode": "MSG416",
                            "message": "Enter valid User details ",
                            "userMessage": "Invalid Data."
                            });
                    }
                    else {
                        resp.json({
                            "status": "SUCCESS",
                            "messageCode": "MSG200",
                            "message": "User updated",
                            "userMessage": "User updated successfully !!!."
                            });
                        
                    }
                });
        });
};

/**
 * Delete user
 */
module.exports.deleteUser = (req, resp) => {
    console.log("Delete User");
    var userid = req.params.userid;

    User
        .findByIdAndRemove(userid)

        .exec(function (err, user) {
            if (err) {
                resp
                    .status(404)
                    .json(err);
            }
            else {
                console.log("User deleted, id:", userid);
                resp
                    .status(204)
                    .json({
                        "message": "User Deleted"
                    });
            }
        })
};




/*Change Password */
// module.exports.changePassword = (req, resp) => {
//     console.log(2, "Inside the function ");
//     console.log(req.body.cEmail);
//     console.log(req.body.OldPassword);
//     console.log(req.body.ConfirmPassword);
//         let user = new User({
//             dEmail: req.body.cEmail,
//             OldPassword: req.body.OldPassword,
//             ConfirmPassword:req.body.ConfirmPassword

//         })
//         console.log('in backend');
//         console.log("test",User.OldPassword);
    
//         User.findOne({
           
//         }).select()
//             .exec(function (err, pass) {
               
//                 if (err) {
//                     resp.json({ success: false, message: err });
//                 }
//                 else {
//                     if (!req.body.cEmail) {
//                         resp.json({
//                             "status": "FAILED",
//                             "messageCode": "ERR411",
//                             "message": "No Email was Provided",
//                             "userMessage": "Invalid Credentials , Please check your Username and Password"
//                         });
//                     }
//                     else {
//                         if (pass === null) {
//                             resp.json({
//                                 "status": "FAILED",
//                                 "messageCode": "ERR412",
//                                 "message": "Email was not found",
//                                 "userMessage": "Invalid Credentials , Please check your Username and Password"
//                             });
    
//                         } else {
//                             console.log("User Value :", req.body.OldPassword)
//                             if(!(req.body.OldPassword)=== undefined){
//                                 console.log("Compared Success");
//                                 // if (bcrypt.compareSync(req.body.OldPassword, pass.Password)) {
//                                 //     console.log("Compared Success");
//                                 // }
//                             }else{
//                                 console.log("Oldpassword undefined");
//                             }
//                             // if (bcrypt.compareSync(user.OldPassword, pass.Password)) {
//                             //     console.log("Success");
                               
                               
//                             // } else {
//                             //     resp.json({
//                             //         "status": "FAILED",
//                             //         "messageCode": "ERR413",
//                             //         "message": "Incorrect Password",
//                             //         "userMessage": "Invalid Credentials , Please check your Username and Password"
//                             //     })
//                             // }
//                         }
//                     }
//                 }
//             });
// };

module.exports.changePassword = (req, resp) => {
    console.log(2, "Inside the function ");
    console.log(req.body.Email);
    console.log(req.body.OldPassword);
    console.log(req.body.ConfirmPassword);
   
        console.log("test",req.body.OldPassword);
    
        Pswd.findOne({
            Email: req.body.Email
           
        }).select()
            .exec(function (err, user) {
               
                if (err) {
                    resp.json({ success: false, message: err });
                }
                else {
                    if (!req.body.Email) {
                        resp.json({
                            "status": "FAILED",
                            "messageCode": "ERR411",
                            "message": "No Email was Provided",
                            "userMessage": "Invalid Credentials , Please check your Username and Password"
                        });
                    }
                    else {
                        if (user === null) {
                            resp.json({
                                "status": "FAILED",
                                "messageCode": "ERR412",
                                "message": "Email was not found",
                                "userMessage": "Invalid Credentials , Please check your Username and Password"
                            });
    
                        } else {
                            if (bcrypt.compareSync(req.body.OldPassword, user.Password)) {
                                  
                                    bcrypt.hash(req.body.ConfirmPassword, saltRounds, function (err, hash) {
                                        // Store hash in your password DB.
                                        console.log("Encrypted Password " + hash);
                                        user.Password = hash;
                                      
                                        user.save(function (err) {
                                            if (err) {
                                                resp.json({
                                                    "status": "FAILED",
                                                    "messageCode": "ERR500",
                                                    "message": "Internal server Error",
                                                    "userMessage": "Please try again later"
                                                });
                                            }
                                            else {
                                                resp.json({
                                                    "status": "SUCCESS",
                                                    "messageCode": "MSG200",
                                                    "message": "Password changed ",
                                                    "userMessage": "Password changed successfully !!!."
                                                    });
                                            }
                                        })
                                    });

                                }else{
                                    resp.json({
                                                "status": "FAILED",
                                                "messageCode": "ERR413",
                                                "message": "Incorrect old Password",
                                                "userMessage": "Please enter correct password"
                                            })
                                }


                                // user.Password=req.body.ConfirmPassword,

                                // user.save(function(err) {
                                //     if(err){
                                //         resp.json({
                                //             "status": "Failure",
                                //             "messageCode": "MSG416",
                                //             "message": "Enter valid Password",
                                //             "userMessage": "Invalid Data."
                                //             });
                                //     }
                                //     else {
                                //         resp.json({
                                //             "status": "SUCCESS",
                                //             "messageCode": "MSG200",
                                //             "message": "Password updated",
                                //             "userMessage": "Password updated successfully !!!."
                                //             });
                                        
                                //     }

                                // });
                                




                                // if (bcrypt.compareSync(req.body.OldPassword, pass.Password)) {
                                //     console.log("Compared Success");
                                // }
                           
                            // if (bcrypt.compareSync(user.OldPassword, pass.Password)) {
                            //     console.log("Success");
                            // } else {
                            //     resp.json({
                            //         "status": "FAILED",
                            //         "messageCode": "ERR413",
                            //         "message": "Incorrect Password",
                            //         "userMessage": "Invalid Credentials , Please check your Username and Password"
                            //     })
                            // }
                        }
                    }
                }
            });
};

module.exports.usersUpdateStatus = (req, res) => { 
    console.log('inside status update')
    var userid = req.params.userid;
    User.findById(userid).exec(function (err, doc) {
       if (doc.Status === "new" || doc.Status === "New") {
         doc.Status = "active";
       }else if (doc.Status === "active"||doc.Status === "Active") {
         doc.Status = "deactive";
       }else if (doc.Status === "inactive" || doc.Status === "Inactive"||doc.Status === "deactivate"||doc.Status === "Deactivate") {
         doc.status = "active";
       }
       doc.save(function (err, usersupdated) {
         console.log(usersupdated)
       });
       console.log('doc status after active', doc.Status);
     });
   };



