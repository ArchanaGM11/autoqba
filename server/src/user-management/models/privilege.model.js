const mongoose= require('mongoose')
const Schema = mongoose.Schema;

let privilegeSchema = new Schema({
    Privilege_id: {
        type: String,
        unique: [true, 'Privilege Id should be unique'],
        required: [true, 'Privilege Id required'],
      },
    Privilege: {
         type: String,
         unique: true,
         required:[true,'RoleName Field is required']
    },
    PrivilegeType: {
        type: String,
        required:[true,'LastName Field is required']
    },
    PrivilegeDescription: {
        type: String,
        required:[true,'ConfirmEmail is required']
    },
    Status: {
        type: String,
        default: 'Active',
        required:[true,'Status is required']
    },
    Created_on: {
        type: Date,
        default: Date.now,
        //required:[true,'Created_on is required']
    },
    Updated_on: {
        type: Date,
        default: Date.now,
        //required:[true,'Updated_on is required']
    },
    Created_by: {
        type: String,
         required:[true,'Updated_on is required']
    },
    Updated_by: {
        type: String,
        required:[true,'Updated_on is required']
    }
    });

module.exports = mongoose.model('privileges', privilegeSchema)