const mongoose= require('mongoose')

let roleSchema = new mongoose.Schema({
    Role_id: {
        type: String,
        unique: [true, 'Role Id should be unique'],
        //required: [true, 'Role Id required'],
      },
    Role: {
         type: String,
         unique:true,
         //default: 'adghgmin'
         required:[true,'RoleName Field is required']
    },
    RoleDescription: {
        type: String,
        //default: 'admghgin'
        required:[true,'RoleDescription Field is required']
    },
    RoleType: {
        type: String,
        //default: 'adggmin'
        required:[true,'RoleType is required']
    },
    Privilege:{
        type: Array,
        required:[true,'PrivilegeType is required']
    },
    Status: {
        type: String,
        //default: 'adhghmin'
        required:[true,'Status is required']
    },
    Created_on: {
        type: Date,
        default: Date.now,
        //required:[true,'Created_at is required']
    },
    Updated_on: {
        type: Date,
        default: Date.now,
    },
    Created_by: {
        type: String,
        default: Date.now,
        required:[true,'Created_at is required']
    },
    Updated_by: {
        type: String,
        required:[true,'Created_at is required']
    }
    });

module.exports = mongoose.model('roles', roleSchema)