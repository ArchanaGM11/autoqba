var express = require('express');
var router = express.Router();

var userController = require('../controllers/users.controller');
var forgotpasswordController = require('../controllers/forgotpassword.controller');
var authenticationController = require('../controllers/authentication.controller');
var roleController = require('../controllers/roles.controller');
var privilegeController = require('../controllers/privileges.controller');
//user routes

router
  .route('/authenticate')
  .post(authenticationController.jwtAuthentication);

//Authenticate Token

router
    .route('/verifytoken')
    .post(authenticationController.tokenVerification)


//New User

router  
   .route('/newuser')
   .post(userController.newUser);

//get all users
 
router
    .route('/userlist')
    .get(userController.getAllUsers)

//change password
   router 
   .route('/changepassword')
   .put(userController.changePassword);




//Reset Password

router
    .route('/resetpassword')
    .put(forgotpasswordController.resetPassword)

//UserProjectRole
router
    .route('/getUserProjectRole/:userid')
    .get(userController.getUserProjectRole)
    

//get,update,delete user

router
    .route('/:userid')
    .get(userController.getOneUser)
    .put(userController.updateUser)
    .delete(userController.deleteUser);

router 
    .route('/status/:userid')
    .put(userController.usersUpdateStatus)

//Router for Role

router
    .route('/role/rolelist')
    .get(roleController.getAllRoles)

router
   .route('/role/:roleid')
   .get(roleController.getOneRole)
   .put(roleController.updateRole)
   .delete(roleController.deleteRole);

router 
   .route('/role/status/:roleid')
   .put(roleController.rolesUpdateStatus)


router
    .route('/newrole')
    .post(roleController.newRole);

//Router for Privilege
router
    .route('/privilege/privilegelist')
    .get(privilegeController.getAllPrivileges)

router
    .route('/privilege/:privilegeid')
    .get(privilegeController.getOnePrivilege)
    .put(privilegeController.updatePrivilege)
    .delete(privilegeController.deletePrivilege);

router 
    .route('/privilege/status/:privilegeid')
    .put(privilegeController.privilegesUpdateStatus)

router
    .route('/newprivilege')
    .post(privilegeController.newPrivilege);


module.exports = router;
